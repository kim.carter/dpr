#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Config::IniFiles;

# Check for Cookie or err
my $cgi = new CGI;
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();
my $uname = $cgi->param("username");

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');

# print html header for any ajax'd pages
print $cgi->header( -cookie=>[$cookie] );


# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbuser');;
my $dbpass =  $cfg->val('DATABASE','dbpass');
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);


my ($ulevel) = $dbh->selectrow_array("select admin from users where username='$uname' && admin=1");

if (!$ulevel || $ulevel=="")
{
	err_login(); #sanity check - non admin users should not ever get to this page
}


print "<div><br>";
print "<table>\n";
my ($p) = $dbh->selectrow_array("select count(*) from projects");
print "<tr><td>Total number of projects: </td><td><b>$p</b></td></tr>\n";

#my ($pnonzero) = $dbh->selectrow_array("select count(distinct(project)) from subjects");
#print "<tr><td>Projects with subject data loaded: </td><td><b>".($p-$pnonzero)."</b></td></tr>\n";

my ($plocked) = $dbh->selectrow_array("select count(*) from projects where locked!=0");
print "<tr><td>Projects with subject data loaded and locked: </td><td><b>".($plocked)."</b></td></tr>\n";


my ($s) = $dbh->selectrow_array("select count(*) from subjects");
print "<br><tr><td>Total number of subjects loaded: </td><td><b>$s</b></td></tr>\n";

my ($s) = $dbh->selectrow_array("select count(*) from subjects where project in (select p_auto from projects where locked!=0)");
print "<br><tr><td>Total number of subjects loaded in locked projects: </td><td><b>$s</b></td></tr>\n";



print "</table>\n";

print "</div></div>";

#my $query = $dbh->prepare($stmt);
#$query->execute();
#my %projects = ();
#while (my @data = $query->fetchrow_array()){
 #$projects{$data[0]} = $data[1];
 #}
#$query->finish();
$dbh->disconnect();


#print $cgi->header( -cookie=>[$cookie] );

# my $ds = lc($project[0]);


sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
