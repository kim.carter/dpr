#!/usr/bin/perl


use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date Delta_Days Today);
use Statistics::Descriptive;
use Spreadsheet::ParseExcel;
use Spreadsheet::ParseXLSX;
use Config::IniFiles;


# Check for Cookie or err
my $cgi = new CGI;
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');


my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my $uid = $session->param("userid");
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');



print $cgi->header( -cookie=>[$cookie] );

# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbupdateuser'); # connect as privileged user to insert data
my $dbpass =  $cfg->val('DATABASE','dbupdatepass');
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);



if ($type eq "b"){

 print $cgi->start_multipart_form(
	-method=>'POST',
	-name=>'dcheck',
	-id=>'dcheck'
	);
 print "<center>";
 print $cgi->filefield(-name=>'dprfile', -id=>'dprfile', -default=>'Upload a file', -size=>60, -maxlength=>100);
print "&nbsp;&nbsp;&nbsp;";
print $cgi->hidden(-name=>'proj', -id=>'proj', -value=>$proj);
print $cgi->button( -name=>'up', -value=>'Check Data', -onclick=>"dprupdata();" );
# print "<button class=\"button\" type=\"submit\" name=\"up\" onclick=\"dprupdata();\" >Check Data</button>\n";
 print $cgi->end_multipart_form();
 print "</center>\n";
 #print "<form name=\"dcheck\" id=\"dcheck\" method=\"post\" enctype=\"multipart/form-data\" action=\"/dprs/cgi-bin/dpr_datacheck.cgi\">\n";
 #print "<input type=\"file\" name=\"dprfile\" id=\"dprfile\" default=\"Upload a file\" size=50 maxlength=80 />\n";
 #print "<input type=\"hidden\" name=\"proj\" id=\"proj\" value=\"$proj\" />\n";
 #print "<table><tr><td style=\"text-align:center\"><button class=\"button\" name=\"up\" onclick=\"dprupdata();\" >Check Data</button></td></tr></table>\n";
 #print "</form>\n";


 print "<img id=\"loading\" src=\"/dprs/images/loading.gif\" style=\"visibility:hidden;\" />\n";
 print "<div id=\"response\"></div>";

 }
elsif ($type eq "u"){
 # Connect to the database
 my $phash = $dbh->selectrow_array("select hash from projects where p_auto = $proj");

 # Bring in the file to be checked
 my $file = $cgi->param("dprfile");
 my $mimetype = $cgi->uploadInfo($file)->{'Content-Type'};

 if (($mimetype !~ /ms-excel/) && ($mimetype !~ /xls/ ) && ($mimetype !~ /openxmlformats-officedocument.spreadsheetml.sheet/) ){
  print "Sorry, your file does not appear to be a valid XLS (Excel 95-2003) or XLSX (Excel 2007) file - $mimetype<br>";
  $dbh->disconnect();
  exit();
  }

 my $outfile = "../projects/$phash/file$$\.xls";
 open(OUT,">$outfile") || die "Can't open output file for writing $outfile:$!\n\n";
 binmode OUT;
 print OUT $_ while(<$file>);
 close(OUT);

 # Read in the file and format in a hash of arrays
 my %HoA = ();
 my @headers = ();

 my $parser = Spreadsheet::ParseXLSX->new;
 if ($mimetype !~ /openxmlformats-officedocument.spreadsheetml.sheet/)
 {
	 $parser   = Spreadsheet::ParseExcel->new();
 }

 my $workbook = $parser->parse($outfile);

 if ( !defined $workbook ) {
        $dbh->disconnect();
  die $parser->error(), ". here\n";
  }

 my $worksheet = defined $workbook->Worksheet("DPR") ? $workbook->Worksheet("DPR") : $workbook->Worksheet(0);
 my ( $row_min, $row_max ) = $worksheet->row_range();
 my ( $col_min, $col_max ) = $worksheet->col_range();

 for my $row ( $row_min .. $row_max ) {
  for my $col ( $col_min .. $col_max ) {
   my $cell = $worksheet->get_cell( $row, $col );
   next unless $cell;
   if ($row == $row_min){
    my $val = $cell->value();
    @{ $HoA{ $val } } = ();
    push @headers, $val;
    }
   else {
    push @{ $HoA{$headers[$col]} }, $cell->value();
    }
   }
  }

 # Remove the file
 unlink($outfile);

 # set constants
 my $date_format = "YYYY-MM-DD";
 my $date_format_pat = '(\d\d\d\d)-(\d\d)-(\d\d)';
 my $date_year_format = "YYYY";
 my $date_year_format_pat = '(\d\d\d\d)';

 # get config variables from ini
 my $idcolname = $cfg->val('VARIABLES','id');
 my $na = $cfg->val('VARIABLES','missing');
 my $missing_cut = $cfg->val('VARIABLES','maxmissing_percent');
 my $sd_dev = $cfg->val('VARIABLES','sd');
 my $dp = $cfg->val('VARIABLES','dp');
 my $char_len = $cfg->val('VARIABLES','maxlen');


 # log all the errors
 my $allerrs = 0;
 my $allwarnings = 0;

 # open a handle to store the results
 my $results = undef;
 open(DAT,">",\$results) || die "Can't open results fh:$!\n\n";;

 # Get the valid parameters for each variable
 my $parms = &build_vars_db($dbh);




 # if there's no person_id defined then stop right here and error
 if (!defined $HoA{$idcolname}){
  print "No ID column <b>$idcolname</b> variable present in dataset. Please correct and retry\n";
  $dbh->disconnect();
  exit();
  }

 # Get all the person_ids in the database for this project
 # Go through the ids in the file and check we don't have data for this ID already
 my $sub_ids = $dbh->selectall_hashref("select id,s_auto from subjects where project = $proj", "id");
 my @person_ids_indb = ();
 foreach my $id (@{ $HoA{$idcolname} }){
  push @person_ids_indb, $id if defined $sub_ids->{$id};
  }

 # Go through the keys in $parms and check what data is present in the keys if the HoA
 	# if there are columns missing report them in @inparms_notinds
 my @inparms_notinds = ();
 foreach my $k (sort {$a cmp $b} keys %{$parms}){
  push @inparms_notinds, $k if !defined $HoA{$k};
  }

 # Go through the keys in HoA and see which do not match keys in $parms - these are likely typos
 	# if there are unidentified columns report them in @inds_notinparms
 my @inds_notinparms = ();
 foreach my $k (sort {$a cmp $b} keys %HoA){
  push @inds_notinparms, $k if !defined $parms->{$k};
  }






 # log errors
 $allerrs++ if scalar(@person_ids_indb) > 0;
 $allerrs++ if scalar(@inparms_notinds) > 0;
 $allerrs++ if scalar(@inds_notinparms) > 0;

 print DAT "<table class=\"varcheck\">\n";
 print DAT "<tbody>\n";

 if (scalar(@person_ids_indb) > 0){
  print DAT "<tr><td>Existing person_ids<br><div class=\"errdiv\" id=\"udb_var\">";
  print DAT "Person ID $_ already exists in the database. Cannot load data for this individual - either delete record or previous upload<br>\n" foreach @person_ids_indb;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('udb_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Existing person_ids";
  print DAT "</td><td class=\"viewerr\"><img src=\"/dprs/images/tick.png\"></td></tr>";
  }

 if (scalar(@inparms_notinds) > 0){
  print DAT "<tr><td>Undefined Variables (need to add column)<br><div class=\"errdiv\" id=\"ud_var\">";
  print DAT "$_<br>\n" foreach @inparms_notinds;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('ud_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Undefined Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/dprs/images/tick.png\"></td></tr>";
  }

 if (scalar(@inds_notinparms) > 0){
  print DAT "<tr><td>Unidentified Variables (need to delete column)<br><div class=\"errdiv\" id=\"ui_var\">";
  print DAT "$_<br>\n" foreach @inds_notinparms;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('ui_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Unidentified Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/dprs/images/tick.png\"></td></tr>";
  }




###############
# MISSING, MANDATORY, MEAN AND SD
###############

my @manderrs = ();
my @misserrs = ();
my $numidvals = scalar(@{ $HoA{$idcolname} });

foreach my $var (sort { $a cmp $b } keys %HoA){

 next unless defined($parms->{$var}); #make sure skip over variables not in the database, as this defines them otherwise and breaks other bits

 my $missing = 0;
 foreach my $d (@{ $HoA{$var} }){
  $missing++ if $d eq $na;
  }
 my $numvals = scalar(@{ $HoA{$var} });

 if ($parms->{$var}->{'mand'} == 1){
  my $percmiss = int( ($missing / $numvals) * 100);
  push @manderrs, "Mandatory variable $var has $percmiss% missing data" if (($percmiss >= $missing_cut) && ($parms->{$var}->{'mand'} == 1));
  }
 push @misserrs, "Variable $var has $numvals values but should have $numidvals. Make sure you have used \"$na\" as your missing value character" if $numvals != $numidvals;
 $allerrs++ if scalar(@manderrs) > 0;
 $allerrs++ if scalar(@misserrs) > 0;
 }



 if (scalar(@manderrs) > 0){
  print DAT "<tr><td>Mandatory Variables<br><div class=\"errdiv\" id=\"man_var\">";
  print DAT "$_<br>\n" foreach @manderrs;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('man_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Mandatory Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/dprs/images/tick.png\"></td></tr>";
  }
 if (scalar(@misserrs) > 0){
  print DAT "<tr><td>Missing data check<br><div class=\"errdiv\" id=\"miss_var\">";
  print DAT "$_<br>\n" foreach @misserrs;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('miss_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Missing data check";
  print DAT "</td><td class=\"viewerr\"><img src=\"/dprs/images/tick.png\"></td></tr>";
  }

 print DAT "</tbody>\n";
 print DAT "</table>\n";
 print DAT "<br><br>";

 print DAT "<table class=\"varcheck\">\n";
 print DAT "<tbody>\n";



VAR: foreach my $var (sort { $a cmp $b } keys %HoA){
  #print "check $var";
  next unless (exists($parms->{$var}));


  my $v_error = 0;
  my $v_mean = undef;
  my $v_sd = undef;
  my %errorsHoA = ();
  my %warningsHoA = ();
  my $v_warning=0;


###############
# MEAN AND SD
###############

  if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
   my $stat = Statistics::Descriptive::Full->new();
   # Get rid of $na and $nc
   my @data_no_na = ();
   foreach my $d (@{ $HoA{$var} }){
    push @data_no_na, $d eq $na ? 0 : $d;
    }
   $stat->add_data(@data_no_na);
   $v_mean = $stat->mean();
   $v_sd = $stat->standard_deviation();
   }

VAL:  for my $i (0 .. $#{ $HoA{$var} }){

   # get the data value
   my $d = $HoA{$var}[$i];

   # if there's an error for this data point
   # store the errors with the corresponding person_id

   # Error Checking begins

###############
# INTEGER
###############

   #if ($parms->{$var}->{'type'} eq "INTEGER"){
   if ($parms->{$var}->{'type'} == 1){
    if ($d ne $na){
     # Check max/min
     if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
      if ($d > $parms->{$var}->{'max'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is greater than the allowed maximum of $parms->{$var}->{'max'}";
       $v_error++;
       next VAL;
       }
      if ($d < $parms->{$var}->{'min'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is less than the allowed minimum of $parms->{$var}->{'min'}";
       $v_error++;
       next VAL;
       }
      }
     # Check categories
     elsif (defined $parms->{$var}->{'cats'}){
      unless ( grep { $_ eq $d } @{ $parms->{$var}->{'cats'} } ) {
       my $firstcat = ${ $parms->{$var}->{'cats'} }[0];
       my $lastind = $#{ $parms->{$var}->{'cats'} };
       my $lastcat = ${ $parms->{$var}->{'cats'} }[$lastind];
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not one of the expected categories ($firstcat .. $lastcat)";
       $v_error++;
       next VAL;
       }
      }
     }
    }

###############
# FLOAT
###############

   #elsif ($parms->{$var}->{'type'} =~ /FLOAT\((\d+)\)/){
   elsif ($parms->{$var}->{'type'} == 2){
    if ($d ne $na){
     # Check max/min
     if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
      if ($d > $parms->{$var}->{'max'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is greater than the allowed maximum of $parms->{$var}->{'max'}";
       $v_error++;
       next VAL;
       }
      if ($d < $parms->{$var}->{'min'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is less than the allowed minimum of $parms->{$var}->{'min'}";
       $v_error++;
       next VAL;
       }
      }

     # Check dp
     # Allow no more than $dp
     if ($d =~ /\d+.(\d*)/){
      my $dpval = length($1);
      if ($dpval > $dp){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d has more than $dp decimal places";
       $v_error++;
       next VAL;
       }
      }
     }
    }

###############
# CHARACTER
###############

   #elsif ($parms->{$var}->{'type'} eq "CHARACTER"){
   elsif ($parms->{$var}->{'type'} == 0){
    # length longer than $char_len
    if (length($d) > $char_len){
     push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "Value is greater than the allowed character limit ($char_len)";
     $v_error++;
     next VAL;
     }
    }

################
# DATE -YEAR ONLY
################

  elsif ($parms->{$var}->{'type'} eq "DATEYEAR"){
    if ($d ne $na){
     # check date format
     if ($d =~ m/$date_year_format_pat/){
      my $year = $1;
      # check validity
      if ($year<1900 || $year > 2020){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not a valid date";
       $v_error++;
       next VAL;
       }
      }
     else {
      # report bad format
      push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not in the right format ($date_year_format)";
      $v_error++;
      next VAL;
      }
     }
    }

###############
# DATE
###############

   #elsif ($parms->{$var}->{'type'} eq "DATE"){
   elsif ($parms->{$var}->{'type'} == 3){
    if ($d ne $na){
     # check date format
     if ($d =~ m/$date_format_pat/){
      my $year = $1;
      my $month = $2;
      my $day = $3;
      my ($y_2day,$m_2day,$d_2day) = Today();
      # check validity
      if (!check_date($year,$month,$day)){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not a valid date";
       $v_error++;
       next VAL;
       }
      # check if later than today
      elsif (Delta_Days($year,$month,$day,$y_2day,$m_2day,$d_2day) < 0 ){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is a date later than today";
       $v_error++;
       next VAL;
       }
      }
     else {
      # report bad format
      push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not in the right format ($date_format)";
      $v_error++;
      next VAL;
      }
     }
    }

   else {
    print DAT "Data type is not defined! Please contact your dpr administrators for more informtation at ".$cfg->val('SITE','email')."\n";
    $dbh->disconnect();
    exit();
    }

###############
# SD
###############


   # Now the value has passed all checks
   # Check it is not an outlier
   if ((defined $v_mean) && ($v_mean > 0)) {
    if ( $d ne $na ){
     if ( $d >=  $v_mean + ($v_sd * $sd_dev) ){

      #modded
      push @{ $warningsHoA{ $HoA{$idcolname}[$i] } }, "This value ($d) is >= 5 sd from the mean ($v_mean)";
      $v_warning++;
      next VAL;
      }
     if ( $d <=  $v_mean - ($v_sd * $sd_dev) ){
      push @{ $warningsHoA{ $HoA{$idcolname}[$i] } }, "This value ($d) is >= 5 sd from the mean ($v_mean)";
      $v_warning++;
      next VAL;
      }
     }
    }

   }




  if ($v_error){
   # if errors
   print DAT "<tr><td>$var - <span class=\"red\">$v_error Error(s)</span><br><div class=\"errdiv\" id=\"$var\"><br>";
   foreach my $k (sort {$a cmp $b} keys %errorsHoA){
    print DAT "Person ID $k: " . join(",", @{ $errorsHoA{$k} }) . "<br>";
    }
   print DAT "</div>";
   print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('$var');\">View Errors</span></td></tr>";
   $allerrs++;
   }
  else {

      #warnings
       if ($v_warning!=0){
          # if errors
          print DAT "<tr><td>$var - <span class=\"orange\">$v_warning Warning(s)</span><br><div class=\"errdiv\" id=\"$var\"><br>";
           foreach my $k (sort {$a cmp $b} keys %warningsHoA){
            print DAT "Person ID $k: " . join(",", @{ $warningsHoA{$k} }) . "<br>";
           }
        print DAT "</div>";
        print DAT "</td><td class=\"viewerr\"><span class=\"link orange\" onclick=\"revealdiv('$var');\">View Warnings</span></td></tr>";
        $allwarnings++;
      }
      else
      {

      # if no errors
      print DAT "<tr><td>$var - no error";
      print DAT "</td><td class=\"viewerr\"><img src=\"/dprs/images/tick.png\"></td></tr>";
      }
   }
  }

 print DAT "</tbody>\n";
 print DAT "</table><BR><BR>\n";

 # load data if there are no errors
 if ($allerrs == 0){




  # Get the study name for the project
  my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

  # Get all the dbids for the variables
  my $var_ids = $dbh->selectall_hashref("select variable,v_auto from variables where study = $study_id[0]", "variable");

  # Now all data is ready to load, make an entry in the load_data table

  $dbh->do("insert into load_data (project,user,filename) VALUES ($proj,$uid,\"$file\")");
  my $ld_id = $dbh->last_insert_id(undef, undef, undef, undef);

  # Now start loading the data
  # First load a person in to the subjects table and store the ID
  for my $id (0 .. $#{ $HoA{$idcolname} }){
   my $sub = ${ $HoA{$idcolname} }[$id];

   $dbh->do("insert into subjects (id,project,load_id) VALUES (\"$sub\",$proj,$ld_id)");
   my $s_id = $dbh->last_insert_id(undef, undef, undef, undef);

   # Now load all data for this individual
   #*** note the variables are sorted alpha increasing!!!!
   foreach my $var (sort { $a cmp $b } keys %HoA){
    #next unless (exists($parms->{$var})); # only insert data for columns that are valid FFS!
    my $v_id = $var_ids->{$var}->{'v_auto'};
    my $val = ${ $HoA{$var} }[$id];

    $dbh->do("insert into data_table (subject,variable,value) VALUES ($s_id,$v_id,\"$val\")");
   }






   }

   #commit all changes
   $dbh->commit or do { print $dbh->errstr; $dbh->rollback; $dbh->disconnect(); exit; }; # commit the changes or die and rollback

    if ($allwarnings !=0)
    {
          print "<BR><BR><b><img src=\"/dprs/images/tick.png\"> No errors found, but warning(s) (eg data variability) have been listed below - please check this is correct.  Your data file containing ".$row_max." records has been loaded. You can now view the data by selecting above.<br><br>$results</b><BR><BR>";
    }
    else
    {
          print "<BR><BR><b><img src=\"/dprs/images/tick.png\"> No errors found, your data file containing ".$row_max." records has been successfully loaded. You can now view the data by selecting above.</b><br/><br/>";
    }


  # Run db code to write CSV
  my @hashs = $dbh->selectrow_array("select hash from projects where p_auto = $proj");
  my $hash = $hashs[0];

  my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

  my @ds_ids = $dbh->selectrow_array("select p_auto from projects where hash='$hash'");
  my $ds_id = $ds_ids[0];

  my $var_id_ref = $dbh->selectall_arrayref("select v_auto,variable,type from variables where study = $study_id[0] order by variable asc");
  my @var_names;
  my @var_ids;
  my @var_types;
  foreach my $row (@$var_id_ref)
  {
	  my ( $v_auto, $v_name, $v_type ) = @$row;
	  push @var_ids,$v_auto;
	  push @var_names,$v_name;
	  push @var_types,$v_type;
  }
  my $varlist = join(",", @var_ids);

  my $p_id_ref = $dbh->selectall_arrayref("select s_auto,id from subjects where project = $ds_id");
  my @pids;
  my @origids;
  foreach my $row (@$p_id_ref)
  {
	  my ( $pid,$id) = @$row;
	  push @pids, $pid;
	  push @origids,$id;
  }
  my $pidlist = join(",", @pids);


  ###  sanity checks - copy R code into shiny for the first time
  my $dir = "/srv/shiny-server/dpr/$hash";
  unless(-d $dir)
  {
	 #make output dir
	 `mkdir -p $dir`;

	 #fix permissions
	 `chmod 777 $dir`;
  }

  # always copy server and ui components - in case changes have been made locally
  `cp ../shiny/*.R ../shiny/*.md  $dir`;



  if (! @pids)  # no subjects loaded, so delete data.csv
  {
	  if (-e "$dir/data.csv")
	  {
		  unlink("$dir/data.csv");
	  }
  }
  else # write data to data.csv
  {
	open(CSV,">$dir/data.csv");

	#header
	print CSV join(",", @var_names)."\n";

	#get variable types
	my @types;
	foreach my $v (@var_types)
	{
		if ($v eq "0") # string or date
		{
			push @types,"string";  # should be factored in R
		}
		elsif ($v eq "3") # date
		{
			push @types,"date";  # should be date in R
		}
		elsif ($v eq "2")
		{
			push @types,"number";  # should be numerical
		}
		elsif ($v eq "1")
		{
			push @types,"factor";  # should be factored in R
		}
	}

	print CSV join(",", @types)."\n";

	#get updated data
	for(my $i=0; $i<=$#pids; $i++)
	{
	  my $person = $pids[$i];
		  my $data_ref = $dbh->selectall_arrayref("select value from data_table where variable in ($varlist) and subject=$person");
		  my @data;
		  foreach my $row (@$data_ref)
		  {
			  my ($d) = @$row;
			  push @data,$d;
		  }
		  print CSV join(",", @data)."\n";
	}
	close(CSV);
   }

  }
 else {
  print "Errors were found trying to upload your file. Please fix as described, and reload<br><br>$results<BR><BR>";
  }

 }

$dbh->disconnect();

###############
# Subroutines
###############

sub build_vars_db {
my $dbh = shift;

my %allvars = ();

# Get all the variables from database including the con cat and dat types
# Get all the type 0 variables - character
my $key_field = "variable";
my $statement = "select v_auto,variable,type,mand from variables where type = 0";
my $charvars = $dbh->selectall_hashref($statement, $key_field);
# Get all the type 1 variables - categorical
$statement = "select v_auto,variables.variable,cat,type,mand from variables left join variables_cat on variables.v_auto = variables_cat.variable where type = 1";
my %catvars = ();
my $query = $dbh->prepare($statement);
$query->execute();
while (my @data = $query->fetchrow_array()){
 $catvars{$data[1]}{'v_auto'} = $data[0];
 $catvars{$data[1]}{'type'} = $data[3];
 next if !defined $data[2];
 push @{ $catvars{$data[1]}{'cats'} }, $data[2];
 }
$query->finish();
# Get all the type 2 variables - continuous
$statement = "select v_auto,variables.variable,description,units,min,max,prec,type,mand from variables left join variables_con on variables.v_auto = variables_con.variable where type = 2";
my $convars = $dbh->selectall_hashref($statement, $key_field);
# Get all the type 3 variables - date
$statement = "select v_auto,variables.variable,description,type,mand from variables left join variables_dat on variables.v_auto = variables_dat.variable where type = 3";
my $datvars = $dbh->selectall_hashref($statement, $key_field);

# combine the hashes into allvars
my %allvars = ();
@allvars{keys %{$charvars}} = values %{$charvars};
@allvars{keys %catvars} = values %catvars;
@allvars{keys %{$convars}} = values %{$convars};
@allvars{keys %{$datvars}} = values %{$datvars};

return(\%allvars);
}


sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
