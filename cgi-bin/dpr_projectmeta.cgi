#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use File::Basename;
use Config::IniFiles;

my $cgi = new CGI;
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();
my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

open(DUMP,">/tmp/testing");

print $cgi->header( -cookie=>[$cookie] );

print DUMP "project=$proj\n";



# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbupdateuser');;
my $dbpass =  $cfg->val('DATABASE','dbupdatepass');
my %attr = (
	RaiseError => 1,
	AutoCommit => 1
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);




# get all the fields and their IDs from project_meta
# This should strictly be study specific and should contain a reference to the study field however for ease and the fact we only have GLI on here at the moment I'm just getting all names
#my $statement = "select pm_auto,variable from project_meta where study = X";
my $statement = "select pm_auto,variable,type from project_meta";
my $loadfields = $dbh->selectall_hashref($statement, 'variable');
my $statement = "select variable from project_meta_oth";
my $loadfields_oth = $dbh->selectall_hashref($statement, 'variable');

# first delete any current entries, also allow the first initial entry
# this might be better as an update but this will require there to be an insert to this table when a project is created
my $delmeta = $dbh->do("delete from project_meta_data where project = $proj");
my $delmetaoth = $dbh->do("delete from project_meta_data_other where project = $proj");
# now load the data based on the keys of %loadfields
foreach my $f (keys %{$loadfields}){
 my $val = $cgi->param($f);
 print DUMP "val = $val\n";
 my $id = $loadfields->{$f}->{'pm_auto'};
 my $qval = $dbh->quote($val);
 # check to see if there is an "Other" associated with this variable
 if (defined $loadfields_oth->{$id}->{'variable'}){
  my $valoth = $cgi->param($f."_other");
  my $qvaloth = $dbh->quote($valoth);
  $dbh->do("insert into project_meta_data_other (project,variable,value) VALUES ($proj,$id,$qvaloth)") if $valoth ne "";
  }
 # if this is a file upload field and a file has been provided then need to upload this file to the right directory
 elsif ($loadfields->{$f}->{'type'} == 3){
  if ($val eq ""){
   # check if there is anything in the associate hidden field
   my $valhid = $cgi->param($f."_hidden");
   # if there is then use this value if not then the value is blank
   $qval = $valhid eq "" ? "" : $dbh->quote($valhid);
   }
  # otherwise new file to process
  else {

   # get the hash name from the db
   my $phash = $dbh->selectrow_array("select hash from projects where p_auto = $proj");

   # get the filename and extension of the provided file
   #my ($fname, $fdir, $fext) = fileparse($val,'\..*');
   #print DUMP "$fname :: $fdir :: $fext";
   # rebuild the uploaded filename
   #my $upfile = $fname . $fext;
   # build the variable filename
   #my $varfile = $f . $fext;
   #print DUMP "file=$varfile\n";

   my $outfile = "../projects/$phash/$val";
   print DUMP "file=$outfile\n";
   open(OUT,">$outfile") || die "Can't open output file for writing $outfile:$!\n\n";
   binmode OUT;
   print OUT $_ while(<$val>);
   close(OUT);

   $qval = $dbh->quote($val);
   }
  }
 #print "insert into project_meta_data (project,variable,value) VALUES ($proj,$id,$qval)<br>";
 #$qval="" if undef($qval);
 $dbh->do("insert into project_meta_data (project,variable,value) VALUES ($proj,$id,$qval)");
 }

$dbh->disconnect();
close(DUMP);
#print "Loading Completed<br>";
#my $referrer = $ENV{HTTP_REFERER};
#print $cgi->redirect($referrer);
