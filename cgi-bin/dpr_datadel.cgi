#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Config::IniFiles;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');
my $load = $cgi->param('load');

my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();
my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my $uid = $session->param("userid");
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');

# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");


#write cgi header
print $cgi->header( -cookie=>[$cookie] );

#sleep(2); #make the page wait to refresh

if ($type eq "r"){

  #set db host
  my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
  my $dbuser = $cfg->val('DATABASE','dbuser'); # connect as non privileged user to select
  my $dbpass =  $cfg->val('DATABASE','dbpass');
  my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

  # connect to the database
  my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);



 my ($ulevel) = $dbh->selectrow_array("select admin from users where u_auto = $uid");
 my ($locked) = $dbh->selectrow_array("select locked from projects where p_auto = $proj");


 # get all the load times for this project
 my $load_ids = $dbh->selectall_hashref("select ld_auto, load_date, username, filename from load_data as ld, users as u where u.u_auto = ld.user and ld.project = $proj", "ld_auto");


 if (!keys %{$load_ids})
 {
       print "<center>No data is loaded for this project currently. Visit the Upload page to add data.</center>"
 }
 else
 {
 print "<div class=\"fileman\" id=\"fileman\">\n";
 print "<table class=\"files\">";

 foreach my $l_id (sort keys %{ $load_ids }){
  if ($locked == 0 )#unlocked project
  {
	print "<tr><td>$load_ids->{$l_id}->{'load_date'}</td><td>$load_ids->{$l_id}->{'username'}</td><td>$load_ids->{$l_id}->{'filename'}</td><td><img class=\"link\" src=\"/dprs/images/erase_small.png\" onclick=\"delete_data($l_id,'rd',$proj);sleepStupidly(1);datadel($proj,'r');\"</td></tr>\n";

  }
  elsif ($locked==1 && $ulevel==0 )#locked dataset, normal user
  {
	print "<tr><td>$load_ids->{$l_id}->{'load_date'}</td><td>$load_ids->{$l_id}->{'username'}</td><td>$load_ids->{$l_id}->{'filename'} (locked)</td><td><img title=\"The data is locked - contact the GLI admin to unlock\" class=\"link\" src=\"/dprs/images/erase_small.png\" </td></tr>\n";
  }
  elsif ($locked==1 && $ulevel ==1 )#locked dataset, admin user
  {
	print "<tr><td>$load_ids->{$l_id}->{'load_date'}</td><td>$load_ids->{$l_id}->{'username'}</td><td>$load_ids->{$l_id}->{'filename'} (locked)</td><td><img title=\"The data is locked - please select above to unlock.\" class=\"link\" src=\"/dprs/images/erase_small.png\" </td></tr>\n";

  }

 } # end for
 print "</table>";
 print "</div>\n";
 }



 print "<div id=\"result\"></div>";

 $dbh->disconnect();
 }
elsif ($type eq "rd"){

  #set db host
  my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
  my $dbuser = $cfg->val('DATABASE','dbupdateuser'); # connect as privileged user to select
  my $dbpass =  $cfg->val('DATABASE','dbupdatepass');
  my %attr = (
      RaiseError => 1,
      AutoCommit => 0
      );

  # connect to the database
  my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr) or die $DBI::errstr;


 $dbh->do("delete from load_data where ld_auto = $load");
 $dbh->commit or do { print $dbh->errstr; $dbh->rollback; $dbh->disconnect(); exit; }; # commit the changes or die and rollback
 $dbh->disconnect();

 my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr) or die $DBI::errstr;
 # now check if we regenerate data.csv
 # Run db code to write CSV

 my @hashs = $dbh->selectrow_array("select hash from projects where p_auto = $proj");
 my $hash = $hashs[0];

 my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

 my @ds_ids = $dbh->selectrow_array("select p_auto from projects where hash='$hash'");
 my $ds_id = $ds_ids[0];

 my $var_id_ref = $dbh->selectall_arrayref("select v_auto,variable,type from variables where study = $study_id[0] order by variable asc");
 my @var_names;
 my @var_ids;
 my @var_types;
 foreach my $row (@$var_id_ref)
 {
         my ( $v_auto, $v_name, $v_type ) = @$row;
         push @var_ids,$v_auto;
         push @var_names,$v_name;
         push @var_types,$v_type;
 }
 my $varlist = join(",", @var_ids);

 my $p_id_ref = $dbh->selectall_arrayref("select s_auto,id from subjects where project = $ds_id");
 my @pids;
 my @origids;
 foreach my $row (@$p_id_ref)
 {
         my ( $pid,$id) = @$row;
         push @pids, $pid;
         push @origids,$id;
 }
 my $pidlist = join(",", @pids);

 my $dir = "/srv/shiny-server/dpr/$hash";




 if (! @pids)  # no subjects loaded, so delete data.csv
 {
         if (-e "$dir/data.csv")
         {
                 unlink("$dir/data.csv") or die "unable to remove file\n";
         }
 }
 else # write data to data.csv
 {
       open(CSV,">$dir/data.csv");

       #header
       print CSV join(",", @var_names)."\n";

       #get variable types
       my @types;
       foreach my $v (@var_types)
       {
               if ($v eq "0") # string or date
               {
                       push @types,"string";  # should be factored in R
               }
               elsif ($v eq "3") # date
               {
                       push @types,"date";  # should be date in R
               }
               elsif ($v eq "2")
               {
                       push @types,"number";  # should be numerical
               }
               elsif ($v eq "1")
               {
                       push @types,"factor";  # should be factored in R
               }
       }
       print CSV join(",", @types)."\n";


       #get updated data
       for(my $i=0; $i<=$#pids; $i++)
       {
         my $person = $pids[$i];
                 my $data_ref = $dbh->selectall_arrayref("select value from data_table where variable in ($varlist) and subject=$person");
                 my @data;
                 foreach my $row (@$data_ref)
                 {
                         my ($d) = @$row;
                         push @data,$d;
                 }
                 print CSV join(",", @data)."\n";
       }
       close(CSV);
  }


 $dbh->disconnect();

 }

sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
