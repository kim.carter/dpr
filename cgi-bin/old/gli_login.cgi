#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use DBI;

my $cgi = new CGI;

#&sitedown();
my $debug = 1;
if ($debug){ open(DB,">/tmp/glierr$$"); }

my $user = $cgi->param('user');
my $pass = $cgi->param('pass');

my $dsn = "dbi:mysql:gli";
my $dbuser = "root";
my $dbpass = "thunda32";
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

# Authenticate user
my $stmt = "select u_auto from users where username = '$user' and password = '$pass'";
print DB "$stmt\n" if $debug;

my $query = $dbh->prepare($stmt);
$query->execute();

my $uid = $query->fetchrow_array();
$query->finish();
$dbh->disconnect();

&err_login if (!$uid);

# now the person has been authenticated
# make a session cookie
my $session = new CGI::Session("driver:File", undef, {Directory=>'/tmp'});
$session->expire('+1h');
my $cookie = $cgi->cookie(
	-name=>'GLI_CGISESSID',
	-value=> $session->id(),
	-expires=> '+1h'
	);

# send the cookie to the browser
print $cgi->header( -cookie=>$cookie );

# put some info in to the session
$session->param("userid",$uid);

# redirect to the project management interface
print $cgi->start_html(
	-head => $cgi->meta({
		-http_equiv => 'Refresh',
		-content => '0;URL=/cgi-bin/gli_projman.cgi'
		})
	);
print $cgi->end_html();

if ($debug){ close(DB); }

sub err_login {
 my $url = '/';
 print $cgi->header( );
 print $cgi->start_html(
        -title=>'iCARE Web based Analysis Portal - Incorrect username / password',
        -style=>{'src'=>"/style/iwap.css"}
        );
print "<SCRIPT LANGUAGE='JavaScript'> window.alert('Username or password error, please try again or email bioinformatics\@ichr.uwa.edu.au for assistance');  window.location.href='/';  </SCRIPT>";



 #print $cgi->redirect(-uri => $url);
 #print $cgi->header();
 #print "window.location=\"$url\";\n\n";
 exit(1);

 }

sub sitedown {
 print $cgi->header();
 print $cgi->start_html(
	-title=>'iCARE Web based Analysis Portal - Site Down',
	-style=>{'src'=>"/style/iwap.css"}
		);
#print "SCRIPT LANGUAGE='JavaScript'> window.alert('Username or password error, please try again or email bioinformatics\@ichr.uwa.edu.au for assistance');  window.location.href='/';  </SCRIPT>";

 print "<p class='sitedown'>The site is down while we fix a problem.<br>Please check back later.<br><br>We apologise for the inconvenience.<br></p>";
 print "<p class='sitedown'>The site is down while we perform a few enhancements to the system.\nThis will be a regular occurrence over the next 4 weeks as we are due to have a final paper and software by the end of July.<br>Please check back later.<br><br>We apologise for the inconvenience.<br></p>";
 print $cgi->end_html();
 exit(1);
 }
