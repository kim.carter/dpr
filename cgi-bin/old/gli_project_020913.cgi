#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

print $cgi->header( -cookie=>[$cookie], -charset=>'utf-8' );

# Connect to the database
my $dbuser = "root";
my $dbpass = "thunda32";
my $dsn = "dbi:mysql:gli";
my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;

my @project = $dbh->selectrow_array("select project,title from projects where p_auto = $proj");
my @privs = $dbh->selectrow_array("select description from users_projects as up, project_privs as pp where up.user_level = pp.pp_auto and user = $uid and project = $proj");

# Need to set up the page
# Give the project name and description
print "<div style=\"float:left\"><img src=\"/images/project.png\"/></div>";
print "<div><br>";
print "<table><tr class=\"projhead\"><td><b>Project Name:</b></td><td>$project[0]</td></tr>";
print "<tr class=\"projhead\"><td><b>User level:</b></td><td>$privs[0]</td></tr>";
print "<tr class=\"projhead\"><td><b>Project Title:</b></td><td>$project[1]</td></tr></table><br>";

print "<img src=\"/images/upload_folder.png\" /><span class=\"link\" onclick=\"datacheck($proj,'b');\">Upload/Check Data</span>\n";
print "<img src=\"/images/file_delete.png\" /><span class=\"link\" onclick=\"datadel($proj,'r');\">Delete Data</span>\n";
print "<img src=\"/images/statistics.png\" /><span class=\"link\" onclick=\"dataview($proj,'v');\">View Site Statistics</span>\n";
print "</div><br><hr/>\n";

# make a div for the dates this project has a run for
# display the dates
print "<div class=\"maindisplay\" id=\"display\"><P>Please select from the above menu to Upload a new dataset, to delete an existing dataset, or to view summary stats on the uploaded data.</p></div>\n";

sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
