#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;

# Check for Cookie or err
my $cgi = new CGI;
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my $uid = $session->param("userid");
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');

# Connect to the database
my $dsn = "dbi:mysql:gli";
my $dbuser = "root";
my $dbpass = "thunda32";
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

my ($uname) = $dbh->selectrow_array("select username from users where u_auto = $uid");
my ($ulevel) = $dbh->selectrow_array("select admin from users where u_auto = $uid");

# get the projects this user has access to
my $stmt = "";
if ($ulevel==1) #admin - see all project
{
	$stmt = "select p_auto, project from projects order by project asc";
}
else #normal user
{
	$stmt = "select p.p_auto, p.project from projects as p, users_projects as up where p.p_auto = up.project and up.user = $uid order by p.project asc";
}
my $query = $dbh->prepare($stmt);
$query->execute();
my %projects = ();
while (my @data = $query->fetchrow_array()){
 $projects{$data[0]} = $data[1];
 }
$query->finish();
$dbh->disconnect();



print $cgi->header( -cookie=>[$cookie] );
print $cgi->start_html(
	-title=>'Global Lungs Initiative - Data Manager',
	-style=>[ {'src'=>"/style/gli.css"} ],
	-script=>[ {-type=>'text/javascript',-src=>'/js/jquery-1.10.1.min.js'}, 
                   {-type=>'text/javascript',-src=>'/js/gli.js'} ],
	-head => [ $cgi->meta({ -http_equiv => 'Pragma', -content => 'no-cache' }),
		$cgi->meta({ -http_equiv => 'Expires', -content => '-1' }) ]
	);

# Initially show all the projects
print "<div class=\"logout\">$uname<br>[ <a href=\"/cgi-bin/gli_logout.cgi\">Logout</a> ]</div> \n";
print "<div class=\"projmenu\"><h2>Welcome " . ucfirst($uname) . "!<br></h2>\n";
print "<a href=\"/cgi-bin/gli_projman.cgi\">Home</a><br>\n";

print "<br>Your projects<br><br>\n";
foreach (sort { $projects{$a} cmp $projects{$b} } keys %projects){
 print "<img src=\"/images/project_small1.png\" /><span class=\"link\" onclick=\"projheader($_,'h');\">". $projects{$_} . "</span><br>\n";
 }

if ($uname eq "gliadmin") #admin - see admin summary
{
 print "<hr><img src=\"/images/project_small1.png\" /><span class=\"link\" onclick=\"adminsummary();\">Admin Summary</span><br>\n";
}
#static for GLI ALL for all users
#print "<br><img src=\"/images/project_small1.png\" /><span class=\"link\" onclick=\"projheader(gli_all,'h');\">". "All GLI" . "</span><br>\n";
print "</div>";

print "<div id='runres' class='projdetails'>\n";
print "<BR><BR><BR><h2>Welcome to the GLI Gas Transfer Project portal.</h2><BR><BR><BR><BR>"; 

 print "<div class=\"frontfileman\" id=\"files\"><h4>Helpful Documents</h4><br><br>";
 print "<table><tr>\n";
 print "<td><img src=\"/images/pdf.jpg\" style=\"border-style:none\"/></td><td><a href=\"/docs/GLI_TLCO_Data_Instructions_V07.pdf\"><span class
=\"medtext\">GLI_TLCO_Data_Instructions_V07.pdf</span></a></td></tr>";
 print "<tr><td><img src=\"/images/msexcel32.jpg\" style=\"border-style:none\"/></td><td><a href=\"/docs/GLI_TLCO_data_template_Dec2013.xls\"><span class=\"medtext\">GLI_TLCO_data_template_Dec2013.xls</span></a></td>";

 print "</tr></table>"; 
 print "</div>";

print $cgi->end_html();

sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
