#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use Date::Calc qw(check_date);

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');

print $cgi->header( -cookie=>[$cookie], -charset=>'utf-8' );

# Report file
# Provide a summary per variable
# 	Include variable values that are out of range or in wrong format
# 	Include variables where range is outside 5sd of the mean

# date_format "YYYY-MM-DD";
my $date_format_pat = "(\d\d\d\d)-(\d\d)-(\d\d)";
my $year = 2012;
my $month = 01;
my $day = 21;

my @AoA = ();
while(my $line = <DATA>){
 $line =~ s/[\n\r]//g;
 my @array = split(",",$line);
 if ($. == 1){ $line =~ s/\"//g; $line =~ s/\s/./g; }
 push @AoA, [@array]; 
 }

#print "OK\n" if (check_date($year,$month,$day));

print $cgi->start_multipart_form(
	-method=>'POST',
	-name=>'datacheck',
	-id=>'datacheck',
	-action=>'/cgi-bin/gli_datacheck.cgi',
	);
print $cgi->hidden( -name=>'check', value=>1 );
print $cgi->filefield(-name=>'glifile', -default=>'upload a file', -size=>50, -maxlength=>80);
print $cgi->button( -name=>'up', -value=>'Check Data' );
print $cgi->end_multipart_form();

print $cgi->hr();

# http://www.finalwebsites.com/forums/topic/php-ajax-upload-example


# integer variables
# float variables
# cat variables
# date variables

sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header( -charset=>'utf-8' );
 print "window.location=\"$url\";\n\n";
 exit(1);
 }

sub build_vars {
my %ranges = (
 "study_id" => {
	
	},
 "dobirth" => {
	"type" => "date",
	"format" => $date_format_pat,
	"min" => 1900,
	"max" => 2012
	},
 "dotest" => {
	"type" => "date",
	"format" => $date_format_pat,
	"min" => 1900,
	"max" => 2012
	},
 "age" => {
	"type" => "int",
	"min" => 0,
	"max" => 150
	},
 "sex" => {
	"type" => "cat",
	"cats" => [1,2]
	},
 "weight" => {
	"type" => "float",
	"min" => 0,
	"max" => 150
	},
 "height" => {
	"type" => "float",
	"min" => 0,
	"max" => 150
	},
 "ethnic_group" => {
	"type" => "cat",
	"cats" => []
	},
 "gest_age" => {
	"type" => "int",
	"min" => 0,
	"max" => 150
	},
 "birth_weight" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "prev_smok_ex" => {
	"type" => "cat",
	"cats" => []
	},
 "cur_smok_ex" => {
	"type" => "cat",
	"cats" => []
	},
 "hist_whez_asth" => {
	"type" => "cat",
	"cats" => []
	},
 "hist_atopy" => {
	"type" => "cat",
	"cats" => []
	},
 "early_resp_inf" => {
	"type" => "cat",
	"cats" => []
	},
 "tlco" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "va" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "kco" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "insp_vol" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "hb" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "cohb" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "fev1" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "fvc" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "vc" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "breath_hold" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "insp_time" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "coll_time" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "ex_gas_vol" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "wash_vol" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "wash_time" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "altitude" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "pbar" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "d_spac" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "gas_x_mod" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "gas_x_mod_oth" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "year_manf" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "soft" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "soft_ver" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "vol_pneu" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "vol_pneu_oth" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "vol_disp" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "vol_disp_oth" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
# dup variable with pbar above
 "mean_bar" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
# dup variable with altitude above
 "alt_sea" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_co" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_he" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_ch4" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_c2h2" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_n" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_o" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_no" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_cor_haem" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_cor_co" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	},
 "tlco_cor_co_meth" => {
	"type" => "float",
	"dp" => 3,
	"min" => 0,
	"max" => 150
	}

 );

}

#id,"Date of Test","Age Years",Sex,Weight,Height,"Ethnic Group","Gestational Age","Birth Weight","Previous smoking exposure","Current smoking exposure","History of wheeze/asthma","History of Atopy","Early respiratory infection",TLCO,VA,KCO,"Inspiratory volume",Hb,COHb,FEV1,FVC,VC,"Breath hold time","Inspiratory time","Collection time","Exhaled gas sample volume","Washout volume","Washout time","Altitude of city",Pbar,deadspace

__DATA__
id,"Date of Test","Age Years",Sex,Weight,Height,"Ethnic Group","Gestational Age","Birth Weight","Previous smoking exposure","Current smoking exposure","History of wheeze/asthma","History of Atopy","Early respiratory infection",TLCO,VA,KCO,"Inspiratory volume",Hb,COHb,FEV1,FVC,VC,"Breath hold time","Inspiratory time","Collection time","Exhaled gas sample volume","Washout volume","Washout time","Altitude of city",Pbar,deadspace
1,2005/04/15,14.29,1,57.7,175,1,NA,NA,NA,NA,NA,NA,NA,24.3,4.34,5.599078341,3.4,NA,NA,1.81,1.95,NA,10,2.9,NA,0.7,1,NA,20,762,0.1154
2,2005/07/01,8.25,2,32.5,131,1,NA,NA,NA,NA,NA,NA,NA,14.9,2.42,6.1570247934,1.86,NA,NA,3.23,4.02,NA,10,2.9,NA,0.7,1,NA,20,762,0.065
3,2006/07/24,13.43,2,59.5,170,1,NA,NA,NA,NA,NA,NA,NA,20.4,3.89,5.2442159383,3.27,NA,NA,2.65,3.22,NA,10,2.9,NA,0.7,1,NA,20,762,0.119
4,2006/07/25,12.24,1,51.2,160,1,NA,NA,NA,NA,NA,NA,NA,19.7,4.43,4.4469525959,3.8,NA,NA,1.14,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.1024
5,2006/07/25,9.27,2,32.8,145,1,NA,NA,NA,NA,NA,NA,NA,14.5,2.73,5.3113553114,2.35,NA,NA,2.31,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0656
6,2007/06/19,12.51,1,49.7,162,1,NA,NA,NA,NA,NA,NA,NA,20.4,3.86,5.2849740933,3.09,NA,NA,1.5,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0994
7,2006/07/28,19.01,2,60,165,1,NA,NA,NA,NA,NA,NA,NA,22.4,4.68,4.7863247863,3.52,NA,NA,3,3.09,NA,10,2.9,NA,0.7,1,NA,20,762,0.12
8,2006/07/28,12.88,1,60.6,152,1,NA,NA,NA,NA,NA,NA,NA,20.2,3.2,6.3125,2.72,NA,NA,1.44,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.1212
9,2006/08/01,15.73,2,66.9,169,1,NA,NA,NA,NA,NA,NA,NA,24.9,4.86,5.1234567901,3.95,NA,NA,2.88,3.23,NA,10,2.9,NA,0.7,1,NA,20,762,0.1338
10,2006/08/01,13.76,2,69.6,168,1,NA,NA,NA,NA,NA,NA,NA,23.3,4.65,5.0107526882,3.95,NA,NA,2.17,2.38,NA,10,2.9,NA,0.7,1,NA,20,762,0.1392
11,2006/08/02,6.33,2,22.5,119,1,NA,NA,NA,NA,NA,NA,NA,10.7,1.72,6.2209302326,1.45,NA,NA,2.56,3,NA,10,2.9,NA,0.7,1,NA,20,762,0.045
12,2006/08/04,12.47,2,39.2,142,1,NA,NA,NA,NA,NA,NA,NA,17.2,2.91,5.910652921,2.38,NA,NA,2.58,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0784
13,2005/07/04,16.42,1,64.1,178,1,NA,NA,NA,NA,NA,NA,NA,32.3,6.85,4.7153284672,5.16,NA,NA,1.58,1.71,NA,10,2.9,NA,0.7,1,NA,20,762,0.1282
14,2006/10/13,11.14,2,28.7,141,1,NA,NA,NA,NA,NA,NA,NA,14.2,2.72,5.2205882353,2.16,NA,NA,NA,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0574
15,2006/11/03,15.47,2,61.6,155,1,NA,NA,NA,NA,NA,NA,NA,24.7,4.5,5.4888888889,4.61,NA,NA,2.61,2.97,NA,10,2.9,NA,0.7,1,NA,20,762,0.1232
16,2006/11/17,11.27,2,32.9,139,1,NA,NA,NA,NA,NA,NA,NA,16.6,2.76,6.0144927536,3.1,NA,NA,2.95,3.36,NA,10,2.9,NA,0.7,1,NA,20,762,0.0658
17,2006/11/24,18.91,1,74,186,1,NA,NA,NA,NA,NA,NA,NA,33.4,6.54,5.1070336391,2.61,NA,NA,1.6,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.148
18,2006/12/15,13.36,2,58.2,172,1,NA,NA,NA,NA,NA,NA,NA,21.9,4.15,5.2771084337,4.29,NA,NA,3.17,3.95,NA,10,2.9,NA,0.7,1,NA,20,762,0.1164
19,2006/12/15,10.42,1,38.1,151,1,NA,NA,NA,NA,NA,NA,NA,21.3,3.69,5.7723577236,3.46,NA,NA,1.61,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0762
20,2006/12/15,15.14,2,85.4,179,1,NA,NA,NA,NA,NA,NA,NA,25.4,5.34,4.7565543071,2.77,NA,NA,2.5,2.7,NA,10,2.9,NA,0.7,1,NA,20,762,0.1708
21,2007/04/19,14.79,2,61.7,165,1,NA,NA,NA,NA,NA,NA,NA,17.9,4.23,4.231678487,3.96,NA,NA,2.46,3.25,NA,10,2.9,NA,0.7,1,NA,20,762,0.1234
22,2005/07/05,11.33,1,40.2,147,1,NA,NA,NA,NA,NA,NA,NA,19.8,3.17,6.2460567823,1.41,NA,NA,1.59,1.92,NA,10,2.9,NA,0.7,1,NA,20,762,0.0804
23,2007/05/10,13.31,1,66.5,165,1,NA,NA,NA,NA,NA,NA,NA,26.3,4.83,5.4451345756,3.31,NA,NA,2.15,2.35,NA,10,2.9,NA,0.7,1,NA,20,762,0.133
24,2007/05/11,8.96,2,23.5,125,1,NA,NA,NA,NA,NA,NA,NA,9.8,1.67,5.8682634731,3.33,NA,NA,2.8,3.22,NA,10,2.9,NA,0.7,1,NA,20,762,0.047
25,2007/05/14,16.82,2,73.5,164,1,NA,NA,NA,NA,NA,NA,NA,19.3,4.05,4.7654320988,3.71,NA,NA,2.74,3.26,NA,10,2.9,NA,0.7,1,NA,20,762,0.147
26,2007/05/15,12.77,1,41,154,1,NA,NA,NA,NA,NA,NA,NA,19.7,4.12,4.7815533981,3.47,NA,NA,1.74,2.03,NA,10,2.9,NA,0.7,1,NA,20,762,0.082
27,2007/07/24,14.07,2,53.8,159,1,NA,NA,NA,NA,NA,NA,NA,21.5,5.07,4.2406311637,3.66,NA,NA,3.52,4.56,NA,10,2.9,NA,0.7,1,NA,20,762,0.1076
28,2007/07/24,11.08,2,47.6,152,1,NA,NA,NA,NA,NA,NA,NA,18.8,3.51,5.3561253561,3.05,NA,NA,3.49,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0952
29,2007/08/08,15.07,2,61.1,163,1,NA,NA,NA,NA,NA,NA,NA,21.6,4.19,5.1551312649,1.51,NA,NA,3.05,3.46,NA,10,2.9,NA,0.7,1,NA,20,762,0.1222
30,2007/08/09,18.21,2,51.5,168,1,NA,NA,NA,NA,NA,NA,NA,19.9,4.29,4.6386946387,4.36,NA,NA,NA,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.103
31,2007/08/27,8.41,2,27.1,127,1,NA,NA,NA,NA,NA,NA,NA,10.9,1.96,5.5612244898,3.53,NA,NA,3,3.63,NA,10,2.9,NA,0.7,1,NA,20,762,0.0542
32,2007/08/21,14.46,1,57.5,171,1,NA,NA,NA,NA,NA,NA,NA,24.1,5.16,4.6705426357,4.04,NA,NA,2.25,2.74,NA,10,2.9,NA,0.7,1,NA,20,762,0.115
33,2005/07/11,15.56,2,58.8,158,1,NA,NA,NA,NA,NA,NA,NA,20.9,4.29,4.8717948718,1.42,NA,NA,3.36,3.83,NA,10,2.9,NA,0.7,1,NA,20,762,0.1176
34,2007/12/20,13.12,1,64.3,164,1,NA,NA,NA,NA,NA,NA,NA,28.9,4.7,6.1489361702,1.8,NA,NA,2.21,2.62,NA,10,2.9,NA,0.7,1,NA,20,762,0.1286
35,2007/12/20,9.79,1,22.6,123,1,NA,NA,NA,NA,NA,NA,NA,11.9,1.83,6.5027322404,2.66,NA,NA,1.73,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0452
36,2008/01/22,10.75,1,33.6,143,1,NA,NA,NA,NA,NA,NA,NA,14,2.86,4.8951048951,2.49,NA,NA,2.21,2.83,NA,10,2.9,NA,0.7,1,NA,20,762,0.0672
37,2008/01/23,7.52,2,24.9,125,1,NA,NA,NA,NA,NA,NA,NA,37.9,4.13,9.1767554479,3.33,NA,NA,3.23,3.74,NA,10,2.9,NA,0.7,1,NA,20,762,0.0498
38,2008/03/04,11,2,39.6,151,1,NA,NA,NA,NA,NA,NA,NA,17.4,3.35,5.1940298507,4.57,NA,NA,4.62,5.86,NA,10,2.9,NA,0.7,1,NA,20,762,0.0792
39,2008/03/04,8.49,1,33.8,146,1,NA,NA,NA,NA,NA,NA,NA,16.2,2.94,5.5102040816,5.19,NA,NA,2.16,2.63,NA,10,2.9,NA,0.7,1,NA,20,762,0.0676
40,2005/07/11,12.67,2,70.6,156,1,NA,NA,NA,NA,NA,NA,NA,19.2,3.89,4.9357326478,1.77,NA,NA,3.61,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.1412
41,2008/04/02,15.8,1,66.1,177,1,NA,NA,NA,NA,NA,NA,NA,34.7,5.8,5.9827586207,3.25,NA,NA,2.39,2.56,NA,10,2.9,NA,0.7,1,NA,20,762,0.1322
42,2008/07/30,15.58,1,72.8,178,1,NA,NA,NA,NA,NA,NA,NA,35.2,6.16,5.7142857143,2.53,NA,NA,1.57,1.72,NA,10,2.9,NA,0.7,1,NA,20,762,0.1456
43,2005/07/11,7.67,1,21.9,122,1,NA,NA,NA,NA,NA,NA,NA,13.1,2.03,6.4532019704,1,NA,NA,1.9,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0438
44,2005/04/22,15.21,2,56.6,171,1,NA,NA,NA,NA,NA,NA,NA,22.9,4.28,5.3504672897,2.86,NA,NA,4,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.1132
45,2005/07/15,8.84,1,34.5,140,1,NA,NA,NA,NA,NA,NA,NA,17,3.11,5.4662379421,1.26,NA,NA,2.38,2.49,NA,10,2.9,NA,0.7,1,NA,20,762,0.069
46,2005/07/15,6.34,2,19.7,117,1,NA,NA,NA,NA,NA,NA,NA,7.9,1.29,6.1240310078,1.2,NA,NA,3.83,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0394
47,2005/07/15,11.08,1,36.5,151,1,NA,NA,NA,NA,NA,NA,NA,21.8,3.56,6.1235955056,2.39,NA,NA,1.78,2.14,NA,10,2.9,NA,0.7,1,NA,20,762,0.073
48,2005/07/18,7.22,2,24.6,124,1,NA,NA,NA,NA,NA,NA,NA,9.5,1.69,5.6213017751,1.99,NA,NA,3.17,3.74,NA,10,2.9,NA,0.7,1,NA,20,762,0.0492
49,2005/07/18,6.1,2,20,118,1,NA,NA,NA,NA,NA,NA,NA,10.8,1.7,6.3529411765,1.23,NA,NA,2.55,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.04
50,2005/08/08,10.91,1,36,151,1,NA,NA,NA,NA,NA,NA,NA,17.1,2.71,6.3099630996,1.52,NA,NA,2.12,2.28,NA,10,2.9,NA,0.7,1,NA,20,762,0.072
51,2005/08/23,13.12,2,58.6,166,1,NA,NA,NA,NA,NA,NA,NA,19.8,4.25,4.6588235294,2.03,NA,NA,3.58,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.1172
52,2005/08/29,10.36,1,36.4,147,1,NA,NA,NA,NA,NA,NA,NA,14.2,2.26,6.2831858407,3.2,NA,NA,2.19,NA,NA,10,2.9,NA,0.7,1,NA,20,762,0.0728
53,2005/09/20,11.08,1,40.3,150,1,NA,NA,NA,NA,NA,NA,NA,15.1,2.97,5.0841750842,2.01,NA,NA,2.01,2.27,NA,10,2.9,NA,0.7,1,NA,20,762,0.0806
54,2005/09/23,8.42,2,30.7,135,1,NA,NA,NA,NA,NA,NA,NA,12.5,2.29,5.4585152838,3.6,NA,NA,3.65,3.77,NA,10,2.9,NA,0.7,1,NA,20,762,0.0614
55,2005/09/23,12.28,1,39.9,151,1,NA,NA,NA,NA,NA,NA,NA,19.8,3.19,6.2068965517,2.4,NA,NA,1.91,2.56,NA,10,2.9,NA,0.7,1,NA,20,762,0.0798
