#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Spreadsheet::ParseExcel;
use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

print $cgi->header( -cookie=>[$cookie], -charset=>'utf-8' );

if ($type eq "b"){

 print $cgi->start_multipart_form(
	-method=>'POST',
	-name=>'dcheck',
	-id=>'dcheck'
	);
 print $cgi->filefield(-name=>'glifile', -id=>'glifile', -default=>'Upload a file', -size=>50, -maxlength=>80);
 print $cgi->hidden(-name=>'proj', -id=>'proj', -value=>$proj);
 print $cgi->button( -name=>'up', -value=>'Check Data', -onclick=>"gliupdata();" );
 print $cgi->end_multipart_form();

 print "<img id=\"loading\" src=\"/images/loading.gif\" style=\"visibility:hidden;\" />\n"; 
 print "<div id=\"response\"></div>";

 }
elsif ($type eq "u"){
 # Bring in the file to be checked
 my $file = $cgi->param("glifile");
 my $mimetype = $cgi->uploadInfo($file)->{'Content-Type'};
 if (($mimetype !~ /ms-excel/) && ($mimetype !~ /xls/ )){
  print "Your file is not an Excel 95-2003 file (sorry, no XLSX) - $mimetype<br>";
  exit();
  }

 my $outfile = "/var/www/upload/file$$\.xls";
 open(OUT,">$outfile") || die "Can't open output file for writing $outfile:$!\n\n";
 binmode OUT;
 print OUT $_ while(<$file>);
 close(OUT);

 # Read in the file and format in a hash of arrays
 my %HoA = ();
 my @headers = ();

 my $parser   = Spreadsheet::ParseExcel->new();
 my $workbook = $parser->parse($outfile);

 if ( !defined $workbook ) {
  die $parser->error(), ".\n";
  }

 my $worksheet = $workbook->Worksheet(0);
 my ( $row_min, $row_max ) = $worksheet->row_range();
 my ( $col_min, $col_max ) = $worksheet->col_range();

 for my $row ( $row_min .. $row_max ) {
  for my $col ( $col_min .. $col_max ) {
   my $cell = $worksheet->get_cell( $row, $col );
   next unless $cell;
   if ($row == $row_min){
    @{ $HoA{ $cell->value() } } = ();
    push @headers, $cell->value();
    }
   else {
    push @{ $HoA{$headers[$col]} }, $cell->value(); 
    }
   }
  }

 # Remove the file 
 unlink($outfile);
 
 # set constants
 my $date_format = "YYYY-MM-DD";
 my $date_format_pat = '(\d\d\d\d)-(\d\d)-(\d\d)';
 my $date_year_format = "YYYY";
 my $date_year_format_pat = '(\d\d\d\d)';


 # missing values
 my $na = 'NA';
 my $nc = 'NC';
 my $sd_dev = 5;
 # this value is the same as in the data_table.value field
 my $char_len = 100;

 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;

 # log all the errors
 my $allerrs = 0;
 # open a handle to store the results
 my $results = undef;
 open(DAT,">",\$results) || die "Can't open results fh:$!\n\n";;
 
 # Get the valid parameters for each variable
 my $parms = &build_vars();
# my $parms = &build_vars_db();
 
 # if there's no study_id defined then stop right here and error
 if (!defined $HoA{'study_id'}){
  print DAT "No study_id variable present in dataset, cannot proceed\n";
  exit();
  }

 # Get all the study_ids in the database
 # Go through the ids in the file and check we don't have data for this ID already
 my $sub_ids = $dbh->selectall_hashref("select id,s_auto from subjects where project = $proj", "id");
 my @study_ids_indb = ();
 foreach my $id (@{ $HoA{'study_id'} }){
  push @study_ids_indb, $id if defined $sub_ids->{$id};
  }

 # Go through the keys in $parms and check what data is present in the keys if the HoA
 	# if there are columns missing report them in @inparms_notinds
 my @inparms_notinds = ();
 foreach my $k (sort {$a cmp $b} keys %{$parms}){
  push @inparms_notinds, $k if !defined $HoA{$k};
  }
 
 # Go through the keys in HoA and see which do not match keys in $parms - these are likely typos
 	# if there are unidentified columns report them in @inds_notinparms
 my @inds_notinparms = ();
 foreach my $k (sort {$a cmp $b} keys %HoA){
  push @inds_notinparms, $k if !defined $parms->{$k};
  }

 # log errors
 $allerrs++ if scalar(@study_ids_indb) > 0;
 $allerrs++ if scalar(@inparms_notinds) > 0;
 $allerrs++ if scalar(@inds_notinparms) > 0;
 
 ### Might need a check here in case there are multiple headers the same, eg two "age" columns

 print DAT "<table class=\"varcheck\">\n";
 print DAT "<tbody>\n";

 if (scalar(@study_ids_indb) > 0){
  print DAT "<tr><td>Existing study_ids<br><div class=\"errdiv\" id=\"udb_var\">";
  print DAT "$_ already exists in the database cannot load data for this individual<br>\n" foreach @study_ids_indb;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('udb_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Existing study_ids";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }
 
 if (scalar(@inparms_notinds) > 0){
  print DAT "<tr><td>Undefined Variables<br><div class=\"errdiv\" id=\"ud_var\">";
  print DAT "$_<br>\n" foreach @inparms_notinds;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('ud_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Undefined Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }
 
 if (scalar(@inds_notinparms) > 0){
  print DAT "<tr><td>Unidentified Variables<br><div class=\"errdiv\" id=\"ui_var\">";
  print DAT "$_<br>\n" foreach @inds_notinparms;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('ui_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Unidentified Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }

 print DAT "</tbody>\n";
 print DAT "</table>\n";
 print DAT "<br><br>"; 

 print DAT "<table class=\"varcheck\">\n";
 print DAT "<tbody>\n";

VAR: foreach my $var (sort { $a cmp $b } keys %HoA){

  next unless (defined $parms->{$var});
  my $v_error = 0;
  my $v_mean = undef;
  my $v_sd = undef;
  my %errorsHoA = ();

###############
# MEAN AND SD 
###############

  if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
   my $stat = Statistics::Descriptive::Full->new();
   # Get rid of $na and $nc
   my @data_no_na = ();
   foreach my $d (@{ $HoA{$var} }){
    #if (($d eq $na) || ($d eq $nc)){ push @data_no_na, 0; }
    #else { push @data_no_na, $d; }
    #push @data_no_na, $d;
    push @data_no_na, ($d eq $na) || ($d eq $nc) ? 0 : $d;
    }
   $stat->add_data(@data_no_na);
   $v_mean = $stat->mean();
   $v_sd = $stat->standard_deviation();
   }
  
VAL:  for my $i (0 .. $#{ $HoA{$var} }){

   # get the data value
   my $d = $HoA{$var}[$i];
   my $nc_check = 0;

   # if there's an error for this data point
   # store the errors with the corresponding study_id

   # Error Checking begins
 
###############
# DEPENDANT
###############

   # check for dependent variables first
   if (defined $parms->{$var}->{'dep'}){
    # get the dependant variable name
    my $depvar = $parms->{$var}->{'dep'}->{'var'};
    # check it's present in the HoA
    if (!defined $HoA{$depvar}){
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "Dependant variable not defined";
     $v_error++;
     next VAL;
     }
    # if present check for the expected value(s)
    else {
     # if value matches
     if ( grep { $_ eq $HoA{$depvar}[$i] } @{ $parms->{$var}->{'dep'}->{'val'} } ) {
      # cannot be missing
      if ($d eq $na){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "This value ($d) cannot be $na, use $nc instead";
       $v_error++;
       next VAL;
       }
      # should be nc if data is not available
      # or be whatever the type is set to
      $nc_check++;
      }
     # if value doesn't match
     else {
      # this val should be missing
      if ($d ne $na){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "This value ($d) should be $na as $depvar is not (@{ $parms->{$var}->{'dep'}->{'val'} })";
       $v_error++;    
       next VAL;
       }
      }
     }
    } 
   
###############
# INTEGER
###############

   if ($parms->{$var}->{'type'} eq "INTEGER"){
    # OK to be $na unless nc_check
    if ( ($d eq $na) && ($nc_check) ){
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d cannot be $na use $nc instead";
     $v_error++;    
     next VAL;
     }
    elsif (($d ne $na) && ($d ne $nc)){
     # Check max/min
     if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
      if ($d > $parms->{$var}->{'max'}){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is greater than the allowed maximum of $parms->{$var}->{'max'}";
       $v_error++;    
       next VAL;
       }
      if ($d < $parms->{$var}->{'min'}){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is less than the allowed minimum of $parms->{$var}->{'min'}";
       $v_error++;    
       next VAL;
       }
      }
     # Check categories
     elsif (defined $parms->{$var}->{'cats'}){
      unless ( grep { $_ eq $d } @{ $parms->{$var}->{'cats'} } ) {
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is not one of the expected categories (" . join(",",@{ $parms->{$var}->{'cats'} }) . ")";
       $v_error++;    
       next VAL;
       }
      }
     }
    }

###############
# FLOAT
###############

   elsif ($parms->{$var}->{'type'} =~ /FLOAT\((\d+)\)/){
    my $dp = $1;
    # OK to be $na unless nc_check
    if ( ($d eq $na) && ($nc_check) ){
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d cannot be $na use $nc instead";
     $v_error++;    
     next VAL;
     }
    elsif (($d ne $na) && ($d ne $nc)){
     # Check max/min
     if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
      if ($d > $parms->{$var}->{'max'}){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is greater than the allowed maximum of $parms->{$var}->{'max'}";
       $v_error++;    
       next VAL;
       }
      if ($d < $parms->{$var}->{'min'}){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is less than the allowed minimum of $parms->{$var}->{'min'}";
       $v_error++;    
       next VAL;
       }
      }

     # Check dp
     # Either Enforce $dp
     #unless ($d =~ /\d+.\d{$dp}/){
     # push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d does not have $dp decimal places";
     # $v_error++;    
     # next VAL;
     # }
     # Or allow no more than $dp
     if ($d =~ /\d+.(\d*)/){
      my $dpval = length($1);
      if ($dpval > $dp){
       push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d has more than $dp decimal places";
       $v_error++;    
       next VAL;
       }
      }
     }
    
    }

###############
# CHARACTER
###############

   elsif ($parms->{$var}->{'type'} eq "CHARACTER"){
    # length longer than $char_len
    if (length($d) > $char_len){
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "Value is greater than the allowed character limit ($char_len)";
     $v_error++;    
     next VAL;
     }
    # OK to be $na unless nc_check
    if ( ($d eq $na) && ($nc_check) ){
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d cannot be $na use $nc instead";
     $v_error++;    
     next VAL;
     }
    }

###############
# DATE -YEAR ONLY
################

  elsif ($parms->{$var}->{'type'} eq "DATEYEAR"){
    # check date format
    if ($d =~ m/$date_year_format_pat/){
     my $year = $1;
     # check validity
     if ($year<1900 || $year > 2020){
      push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is not a valid date";
      $v_error++;
      next VAL;
      }
     }
    else {
     # report bad format
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is not in the right format ($date_year_format)";
     $v_error++;
     next VAL;
     }
    }

###############
# DATE
###############

   elsif ($parms->{$var}->{'type'} eq "DATE"){
    # check date format
    if ($d =~ m/$date_format_pat/){
     my $year = $1;
     my $month = $2;
     my $day = $3;
     # check validity
     if (!check_date($year,$month,$day)){
      push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is not a valid date";
      $v_error++;    
      next VAL;
      }
     }
    else {
     # report bad format
     push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "$d is not in the right format ($date_format)";
     $v_error++;    
     next VAL;
     }
    }
   else {
    print DAT "Data type not defined! Contact administrators";
    exit();
    }

###############
# SD
###############

   # Now the value has passed all checks
   # Check it is not an outlier
   if ((defined $v_mean) && ($v_mean > 0)) {
    if (( $d ne $na ) && ( $d ne $nc )){
     if ( $d >=  $v_mean + ($v_sd * $sd_dev) ){
      push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "This value ($d) is >= 5 sd from the mean ($v_mean)";
      $v_error++;    
      next VAL;
      }
     if ( $d <=  $v_mean - ($v_sd * $sd_dev) ){
      push @{ $errorsHoA{ $HoA{'study_id'}[$i] } }, "This value ($d) is >= 5 sd from the mean ($v_mean)";
      $v_error++;
      next VAL;
      }
     }
    }

   }

  if ($v_error){
   # if errors
   print DAT "<tr><td>$var - <span class=\"red\">$v_error Error(s)</span><br><div class=\"errdiv\" id=\"$var\"><br>";
   foreach my $k (sort {$a cmp $b} keys %errorsHoA){
    print DAT "$k => " . join(",", @{ $errorsHoA{$k} }) . "<br>";
    }
   print DAT "</div>";
   print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('$var');\">View Errors</span></td></tr>";
   $allerrs++;
   }
  else {
   # if no errors
   print DAT "<tr><td>$var";
   print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
   }  
  }

 print DAT "</tbody>\n";
 print DAT "</table>\n";

 # load data if there are no errors
 if ($allerrs == 0){
  print "<BR><BR><p>No errors found, your data has been successfully loaded. You can now view the data by selecting above.</p>";

  # Get the study name for the project
  my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

  my $outname = $dbh->selectrow_array("select project from projects where p_auto = $proj");
 
  # Get all the dbids for the variables
  # Once the dependent variables tables have been made then the retrieval of the data can come directly from the database
  # until then...........
  my $var_ids = $dbh->selectall_hashref("select variable,v_auto from variables where study = $study_id[0]", "variable");

  # Now all data is ready to load, make an entry in the load_data table
  $dbh->do("insert into load_data (project,user,filename) VALUES ($proj,$uid,\"$file\")");
  my $ld_id = $dbh->last_insert_id(undef, undef, undef, undef);

  # Now start loading the data
  # First load a person in to the subjects table and store the ID
  for my $id (0 .. $#{ $HoA{'study_id'} }){
   my $sub = ${ $HoA{'study_id'} }[$id];
   $dbh->do("insert into subjects (id,project,load_id) VALUES (\"$sub\",$proj,$ld_id)");
   my $s_id = $dbh->last_insert_id(undef, undef, undef, undef);

   # Now load all data for this individual
   foreach my $var (sort { $a cmp $b } keys %HoA){
    next if $var eq "study_id";
    my $v_id = $var_ids->{$var}->{'v_auto'};
    my $val = ${ $HoA{$var} }[$id];
    $dbh->do("insert into data_table (subject,variable,value) VALUES ($s_id,$v_id,\"$val\")");
    }
   }

  # Run the R command to generate the stats
  my $errorfile = "/tmp/Rerrors$$.txt";
  system("xvfb-run --auto-servernum --server-num=1 R CMD BATCH --no-save --slave --vanilla '--args $outname' /var/www/html/gli/makeHTML2.R $errorfile");
#system("xvfb-run --auto-servernum --server-num=1 R --no-save --slave --vanilla --args $outname < /var/www/html/gli/makeHTML2.R $errorfile");

  }
 else {
  print "Data errors were found as below. Please fix these and reload<br><br>$results";
  }

 } 


###############
# Subroutines
###############

sub build_vars {
my %ranges = (
 "study_id" => {
    "type" => "CHARACTER",
    "desc" => "Participants ID"
    },
 "dobirth" => {
    "type" => "DATE",
    "desc" => "Date of birth"
    },
 "dotest" => {
    "type" => "DATE",
    "desc" => "Date testing was carried out"
    },
 "age" => {
    "type" => "FLOAT(3)",
    "desc" => "Age at the time of testing",
    "units" => "years",
    "min" => 0,
    "max" => 100
    },
 "sex" => {
    "type" => "INTEGER",
    "desc" => "Participant\'s sex"
    "cats" => [1,2],
    },
 "weight" => {
    "type" => "FLOAT(3)",
    "desc" => "Weight",
    "units" => "kgs",
    "min" => 10, 
    "max" => 300
    },
 "height" => {
    "type" => "FLOAT(3)",
    "desc" => "Height",
    "units" => "cms",
    "min" => 50, 
    "max" => 230
    },
 "coun_birth" => {
    "type" => "INTEGER",
    "desc" => "Country of birth of subject",
    "cats" => [1..239]
    },
 "coun_birth_m" => {
    "type" => "CHARACTER",
    "desc" => "Country of birth of subject\'s mother",
    "cats" => [1..239]
    },
 "coun_birth_f" => {
    "type" => "CHARACTER",
    "desc" => "Country of birth of subject\'s father",
    "cats" => [1..239]
    },
 "gest_age" => {
    "type" => "FLOAT(3)",
    "desc" => "Gestational Age",
    "units" => "weeks",
    "min" => 20, 
    "max" => 45
    },
 "preterm" => {
    "type" => "INTEGER",
    "desc" => "Was the subject born at term (>36 completed weeks gestation)",
    "cats" => [1,2,3]
    },
 "birth_weight" => {
    "type" => "FLOAT(3)",
    "desc" => "Birth Weight",
    "units" => "kgs",
    "min" => 1.5, 
    "max" => 10
    },
 "ever_smok" => {
    "type" => "INTEGER",
    "desc" => "Have you smoked >100 cigarettes in your life time",
    "cats" => [1,2]
    },
 "prev_smok_ex" => {
    "type" => "INTEGER",
    "desc" => "Previous smoking exposure"
    "cats" => [1,2]
    },
 "cur_smok_ex" => {
    "type" => "INTEGER",
    "desc" => "Current smoking exposure"
    "cats" => [1,2]
    },
 "smok_py" => {
    "type" => "FLOAT(3)",
    "desc" => "Smoking pack years",
    "units" => "years",
    "min" => 1, 
    "max" => 100
    },
 "tlco_si" => {
    "type" => "FLOAT(3)",
    "desc" => "Transfer factor of the lung for carbon monoxide (uncorrected for Hb, CoHb or altitude) in SI units",
    "units" => "mmol.min-1.kPa-1",
    "min" => 0, 
    "max" => 20
    },
 "tlco_tr" => {
    "type" => "FLOAT(3)",
    "desc" => "Transfer factor of the lung for carbon monoxide (uncorrected for Hb, CoHb or altitude) in traditional units",
    "units" => "mL/min/mmHg",
    "min" => 0, 
    "max" => 50
    },
 "va" => {
    "type" => "FLOAT(3)",
    "desc" => "Alveolar Volume (BTPS corrected)",
    "units" => "L",
    "min" => 0, 
    "max" => 15
    },
 "hb" => {
    "type" => "FLOAT(3)",
    "desc" => "Hb",
    "units" => "g/L",
    "min" => 50,
    "max" => 250
    },
 "cohb" => {
    "type" => "FLOAT(3)",
    "desc" => "COHb",
    "units" => "%",
    "min" => 0, 
    "max" => 20
    },
 "fev1" => {
    "type" => "FLOAT(3)",
    "desc" => "Forced Expiratory Volume (1 sec)",
    "units" => "L",
    "min" => 0, 
    "max" => 10
    },
 "fvc" => {
    "type" => "FLOAT(3)",
    "desc" => "Forced Vital Capacity",
    "units" => "L",
    "min" => 0, 
    "max" => 10
    },
 "breath_hold" => {
    "type" => "FLOAT(3)",
    "desc" => "Breath hold time",
    "units" => "seconds",
    "min" => 5, 
    "max" => 20
    },
 "altitude" => {
    "type" => "FLOAT(3)",
    "desc" => "Altitude of city where test carried out",
    "units" => "m",
    "min" => 0, 
    "max" => 8935
    },
 "pbar" => {
    "type" => "FLOAT(3)",
    "desc" => "Barometric Pressure",
    "units" => "kPa",
    "min" => 98, 
    "max" => 105
    },
 );

return(\%ranges);
}

sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }


__DATA__

 "gas_x_mod" => {
    "type" => "INTEGER",
    "desc" => "Gas transfer equipment model",
    "cats" => [1,2,3,4,5,6,7,8,9,10,11,12,13],
    },
 "gas_x_mod_oth" => {
    "type" => "CHARACTER",
    "desc" => "Gas transfer equipment model - if other give details",
    "dep" => {
	"var" => "gas_x_mod",
	"val" => [13]
	}
    },
 "gas_x_year_manf" => {
    "type" => "DATEYEAR",
    "desc" => "Year manufactured"
    },
 "gas_x_soft" => {
    "type" => "CHARACTER",
    "desc" => "Software name"
    },
 "gas_x_soft_ver" => {
    "type" => "CHARACTER",
    "desc" => "Software version"
    },
 "same_soft" => {
    "type" => "INTEGER",
    "desc" => "Was the same software used for all subjects",
    "cats" => [1,2]
    },
 "same_equip" => {
    "type" => "INTEGER",
    "desc" => "Was the same equipment used for all subjects",
    "cats" => [1,2]
    },
# "flow_vol_pneu" => {
#    "type" => "INTEGER",
#    "cats" => [1,2,3,4,5,6,7],
#    },
# "flow_vol_pneu_oth" => {
#    "type" => "CHARACTER",
#    "dep" => {
#	"var" => "flow_vol_pneu",
#	"val" => [7]
#	}
#    },
# "flow_vol_disp" => {
#    "type" => "INTEGER",
#    "cats" => [1,2,3],
#    },
# "flow_vol_disp_oth" => {
#    "type" => "CHARACTER",
#    "dep" => {
#	"var" => "flow_vol_disp",
#	"val" => [3]
#	}
#    },
 "tlco_co" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Carbon Monoxide used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_he" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Helium used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_ch4" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Methane used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_c2h2" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Acetelyne used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_n" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Nitrogen used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_o" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Oxygen used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_no" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Nitrogen Monoxide used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
# "tlco_cor_haem" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "tlco_cor_co" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "tlco_cor_co_meth" => {
#    "type" => "CHARACTER",
#    "dep" => {
#	"var" => "tlco_cor_co",
#	"val" => [1]
#	}
#    },
 "vol_wo_meth" => {
    "type" => "INTEGER",
    "desc" => "What volume washout method was used?",
    "cats" => [1,2,3,4],
    },
 "vol_wo_fix_o2" => {
    "type" => "FLOAT(3)",
    "desc" => "If washout volume was fixed what volume was used if VC ≥ 2 litres?",
    "dep" => {
	"var" => "vol_wo_meth",
	"val" => [1]
	},
    "min" => 0, 
    "max" => 5
    },
 "vol_wo_fix_u2" => {
    "type" => "FLOAT(3)",
    "desc" => "If washout volume was fixed what volume was used if VC < 2 litres?",
    "dep" => {
	"var" => "vol_wo_meth",
	"val" => [1]
	},
    "min" => 0, 
    "max" => 2
    },
# "sam_vol_meth" => {
#    "type" => "INTEGER",
#    "cats" => [1,2,3,4],
#    },
# "sam_vol_fix_o2" => {
#    "type" => "FLOAT(3)",
#    "dep" => {
#	"var" => "sam_vol_meth",
#	"val" => [1]
#	},
#    "min" => 0, 
#    "max" => 2
#    },
# "sam_vol_fix_u2" => {
#    "type" => "FLOAT(3)",
#    "dep" => {
#	"var" => "sam_vol_meth",
#	"val" => [1]
#	},
#    "min" => 0, 
#    "max" => 2
#    },
 "ds_vol_meth" => {
    "type" => "INTEGER",
    "desc" => "What dead space volume method was used?",
    "cats" => [1,2,3,4],
    },
 "ds_vol_fix" => {
    "type" => "FLOAT(3)",
    "desc" => "If fixed what total dead space volume was used?",
    "dep" => {
	"var" => "ds_vol_meth",
	"val" => [1]
	},
    "min" => 0, 
    "max" => 5
    },
 "ds_vol_sub" => {
    "type" => "FLOAT(3)",
    "desc" => "If fixed what subject dead space volume was used?",
    "dep" => {
	"var" => "ds_vol_meth",
	"val" => [1]
	},
    "min" => 0, 
    "max" => 5
    },
 "ds_vol_eqp" => {
    "type" => "FLOAT(3)",
    "desc" => "If fixed what equipment dead space volume was used?",
    "dep" => {
	"var" => "ds_vol_meth",
	"val" => [1]
	},
    "min" => 0, 
    "max" => 1
    },
 "ds_vol_bs_for" => {
    "type" => "CHARACTER",
    "desc" => "If deadspace volume was calculated from body size, what formula was used?",
    "dep" => {
	"var" => "ds_vol_meth",
	"val" => [3]
	},
    },
# "mou_bac" => {
#    "type" => "INTEGER",
#    "cats" => [1,2]
#    },
 "mou_bac_brand" => {
    "type" => "CHARACTER",
    "desc" => "What mouthpiece or bacterial brand was used?"
    },
# "mou_bac_vol" => {
#    "type" => "FLOAT(3)",
#    "min" => 0, 
#    "max" => 0.5
#    },
 "mou_bac_filt" => {
    "type" => "INTEGER",
    "desc" => "Was deadspace of filter included in calculation?",
    "cats" => [1,2]
    },
 "meth_bh" => {
    "type" => "INTEGER",
    "desc" => "Method used for calculating breath hold time?",
    "cats" => [1,2,3,4],
    },
 "meth_bh_oth" => {
    "type" => "CHARACTER",
    "desc" => "Breath hold method - if other, please specify",
    },
# "equa_va" => {
#    "type" => "CHARACTER",
#    },
# "tlco_corr_equa" => {
#    "type" => "INTEGER",
#    "cats" => [1,2,3,4,5,6],
#    },
# "kco_equa" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "tlco_col" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "vaeff_col" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
 "bre_ht_col" => {
    "type" => "FLOAT(3)",
    "desc" => "What target breath hold time was used?",
    },
# "kco_col" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "vinsp_col" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "tlco_p_col" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
# "no_manu" => {
#    "type" => "INTEGER",
#    "min" => 0, 
#    "max" => 0
#    },
# "no_acpt_manu" => {
#    "type" => "INTEGER",
#    "min" => 0, 
#    "max" => 0
#    },
 "bes_manu" => {
    "type" => "INTEGER",
    "desc" => "How was the reported TLCO value selected?",
    "cats" => [1,2,3,4],
    },
 "vol_calib" => {
    "type" => "INTEGER",
    "desc" => "Flow/Volume equipment calibration frequency",
    "cats" => [1,2,3,4,5,6],
    },
 "ga_calib" => {
    "type" => "INTEGER",
    "desc" => "Gas analysers calibration frequency",
    "cats" => [1,2,3,4,5,6],
    },
 "vol_qc" => {
    "type" => "CHARACTER",
    "desc" => "Flow/Volume equipment quality control method and frequency (select all that apply)",
    },
 "ga_qc" => {
    "type" => "INTEGER",
    "desc" => "Gas analysers quality control frequency see above",
    "cats" => [1,2,3,4,5,6],
    },
 "adh_spec" => {
    "type" => "INTEGER",
    "desc" => "Adherence to published TLCO/DLCO guidelines",
    "cats" => [1,2,3],
    },
 "adh_guid" => {
    "type" => "CHARACTER",
    "desc" => "If adherence, which guidelines were used?",
    "dep" => {
	"var" => "adh_spec",
	"val" => [1,2]
	},
    },
# "dat_2005_comp" => {
#    "type" => "INTEGER",
#    "cats" => [1,2,3],
#    },
# "ychild_mod" => {
#    "type" => "INTEGER",
#    "cats" => [1,2,3],
#    },
# "ychild_guid" => {
#    "type" => "CHARACTER",
#    "dep" => {
#	"var" => "ychild_mod",
#	"val" => [1,2]
#	},
#    },
# "post" => {
#    "type" => "INTEGER",
#    "cats" => [1,2],
#    },
 "nose_clip" => {
    "type" => "INTEGER",
    "desc" => "Nose clip used during testing?",
    "cats" => [1,2,3],
    },
 "rest_mes" => {
    "type" => "FLOAT(3)",
    "desc" => "How long did the subject rest between measurements?",
    "min" => 0, 
    "max" => 1000
    },
# "max_manu" => {
#    "type" => "INTEGER",
#    "min" => 0, 
#    "max" => 0
#    }
 "pub" => {
    "type" => "FILE",
    "desc" => "Has your data been published. If yes please upload PDF of publiction",
    }
