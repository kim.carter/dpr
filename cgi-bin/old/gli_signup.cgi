#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use MIME::Lite;
use utf8;
use Encode;

# Check for Cookie or err
my $cgi = new CGI;

#get details to add by key
my $key = $cgi->param("newid");

print $cgi->header();

# Connect to the database
my $dsn = "dbi:mysql:gli";
my $dbuser = "root";
my $dbpass = "thunda32";
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

#check file exists
if (!-e "/home/gli/requests/$key/details.txt")
{
	print "Unable to find the project request - The project may have already been certified by the other GLI admins, or an error has occurred. please contact the site admins - bioinformatics\@ichr.uwa.edu.au for assistance\n";
	exit(0);
}




open(IN,"</home/gli/requests/$key/details.txt") or die "Unable to open the project request file $_.<BR>Please contact the site admins - bioinformatics\@ichr.uwa.edu.au for assistance\n";
my @lines=<IN>;
my @details;
my %details = ();
for (my $i=0; $i<=11; $i++)
{
	my $line = $lines[$i];
	chomp($line);
	my @bits = split("\t",$line);
	$details[$i] = $bits[1];
	$details[$i] =~s/["']+//g; #strip quotes
}

#help fix name stuff
use utf8;
use Text::Unidecode;
#my $name = unidecode("\x{5317}\x{4EB0}  \n\n");
 
 #$name=~s/[\x00-\x1F\n]+//g; 
 #$name=~s/ //g;
 #print "Clean = **$name**\n";

  # That prints: Bei Jing 

$details[0] =~ s/ ;+-*\?#&%//g; #strip spaces 
$details[1] =~ s/ ;+-*\?#&%//g; #strip spaces 

$details[0]  =~ s/ +//g; 
$details[1]  =~ s/ +//g;
$details[2] =~ s/ +//g; #strip spaces and other randoms from name and email


use Unicode::String qw(utf8 latin1 utf16);

$details[0] = utf8($details[0]);
#$details[0] =  quotemeta($details[0]->utf7);      # 7-bit clean format
$details[1] = utf8($details[1]);
#$details[1] =  quotemeta($details[1]->utf7);      # 7-bit clean format

$details[0] =~ s/[^[:ascii:]]//g;
$details[1] =~ s/[^[:ascii:]]//g;
$details[1] =~ s/[^!-~\s]//g;
$details[1] = encode("ascii", decode("latin1", $details[1]));
$details[0] = encode("ascii", decode("latin1", $details[0]));

$details[0]=~s/[\x00-\x1F\n]+//g; 
$details[1]=~s/[\x00-\x1F\n]+//g; 

$details[6] =~ s/ //g; #strip spaces from country


#print "$details[0]_$details[1]<BR>";

#potential multi-line description - no tabs
my $desc = $lines[12];
for(my $i=13;$i<=$#lines;$i++)
{
	$desc=$desc." $lines[$i]";
}
$desc =~s/[\n\r]//g; #strip new lines

#[0] fwrite($handle, "contact_fname\t".$_POST["contact_fname"]."\n");
#[1] fwrite($handle, "contact_sname\t".$_POST["contact_sname"]."\n");
#[2] fwrite($handle, "contact_email\t".$_POST["contact_email"]."\n");
#[3] fwrite($handle, "contact_phone\t".$_POST["contact_phone"]."\n");
#[4] fwrite($handle, "contact_org\t".$_POST["contact_org"]."\n");
#[5] fwrite($handle, "contact_address\t".$_POST["contact_address"]."\n");
#[6] fwrite($handle, "country\t".$_POST["country"]."\n");
#[7] fwrite($handle, "custodian_name\t".$_POST["custodian_name"]."\n");
#[8] fwrite($handle, "custodian_email\t".$_POST["custodian_email"]."\n");
#[9] fwrite($handle, "custodian_phone\t".$_POST["custodian_phone"]."\n");
#[10] fwrite($handle, "data_name\t".$_POST["data_name"]."\n");contact_org
#[11] fwrite($handle, "data_size\t".$_POST["data_size"]."\n");
#[12-] fwrite($handle, $_POST["description"]."\n");


#get next available project name
my $num=1;  #Country_Surname_number
my ($yup) = $dbh->selectrow_array("select project from projects where project = \"$details[6]_$details[1]_$num\"");
while ($yup ne "")
{
	$num++;
	($yup) = $dbh->selectrow_array("select project from projects where project = \"$details[6]_$details[1]_$num\"");
}





my $username="$details[0]_$details[1]";
$username=~s/[^\w\s]//g;
$username=~s/[0-9\?\&\#9\;]//g;

$dbh->do("insert into projects (project,hash,title,description,display,study,contact_fname,contact_sname,contact_email,contact_phone,contact_org,contact_address,country,custodian_name,custodian_email,custodian_phone) values ('$details[6]_$details[1]_$num','$key','$details[10]',".$dbh->quote($desc).",0,1,'$details[0]','$details[1]','$details[2]','$details[3]','$details[4]','$details[5]','$details[6]','$details[7]','$details[8]','$details[9]') ");

my ($p_auto) = $dbh->selectrow_array("select p_auto from projects where project='$details[6]_$details[1]_$num' and title='$details[10]'");



#generate username and password, or find existing
my ($un,$password) = $dbh->selectrow_array("select username,password from users where username='$username' and email='$details[2]'");

my $existing=1;

if ($un eq "" || $password eq "") #can't find existing user, so generate new
{
	$existing=0; #new user
	use Crypt::GeneratePassword qw(word chars);
	my $maxlen=8;
	my $minlen=8;
	my $signs=0; #max signs
	my $caps=3; #max caps
	my $minfreq=0.1;
	my $avfreq=0.2;

	$un = $username;
	$password = &word($minlen,$maxlen,"en",$signs,$caps,$minfreq,$avfreq);

	$dbh->do("insert into users (username, password, email) values ('$username','$password','$details[2]')");
}
else
{
	$un=$username;
}

#now get u_auto
#print "select u_auto from users where username='$un' and email='$details[2]'";
my ($u_auto) = $dbh->selectrow_array("select u_auto from users where username='$un' and email='$details[2]'");


#update user_projects
$dbh->do("insert into users_projects (user, project, user_level) values ($u_auto,$p_auto,2)");

$dbh->disconnect();


my $fail="";


#move uploaded details into project directory
#mkdir("/home/gli/projects/$key/") or die "unable to make directory /home/gli/projects/$key/\n";

#No need to do a mkdir, as this makes the key directory, and moves the contents into it
system("mv /home/gli/requests/$key /home/gli/projects/");
#system("mv /home/gli/requests/$key/*.pdf /home/gli/projects/$p_auto/");

#system("\\rmdir /var/www/html/requests/$key") or die "unable to remove temp directory\n";

my $certname = $un;
$certname =~ s/ ;+-*\?#//g; #strip spaces 


my $certpath= "/home/gli/certs/gli-$certname.p12";

#generate cert if NEW user or cert missing
if (-e $certpath && -r $certpath && $existing==1)
{}
else
{
	##print "gencert \"$details[0] $details[1]\" $details[2] \"$certname\"<BR>";
	my $str = "/usr/bin/generateclientcert.pl \"$details[0] $details[1]\" \"$details[2]\" $certname";
	system($str);
	#system("gencert \"$details[0] $details[1]\" $details[2] \"$certname\" ");
	$existing=0;
}


# send email to person
my $con_email = $details[2];
my $con_name = $details[0]." ".$details[1];


my $msgcert = "";
if ($existing==0)
{
	$msgcert=qq(<p>We've created a client certificate for you to use to access the GLI portal. Your certificate is
                the .p12 file inside the zip file attached to this email. In the upload instructions document are instructions how to install the certificate
                into your browser (you need to install into each browser you use). You can install this in your work 
                computer/laptop or home computer (or where ever you normally work from). Please keep this certificate file
                and the attached document in a secure location.</p>);
}
else
{
	$msgcert=qq(<p>As you already have a login to the GLI portal for another data set, you don't need a new certificate or password - please use the existing ones you have. The new project will appear on the left-hand-side when you login. Your username and password and re-printed below.</p>);
}

my $msg = MIME::Lite->new(
         To      => $con_email,
         Subject =>'GLI Gas Transfer Project: Access details and instructions',
         From    =>'gliportal@ichr.uwa.edu.au',
         Type    =>'multipart/related'
    );
    $msg->attach(
        Type => 'text/html',
        Data => qq{
            <body>
                <p>Dear $con_name,<br>
		Thankyou for signing up to the GLI Gas Transfer project</p>
		<p>As part of the data loading process we have added a number of security measures to lock down
		the site. One of the measures we have taken is to have client SSL certificates in addition to
		traditional server SSL certificates (which your bank etc normally use). The client certificate
		system stops anyone without a valid certificate in their web browser loading the website at all 
		(they don't even get to the password login screen).</p>
		$msgcert
		<p>If at any stage you feel like someone has had access to your machine, or someone may have used your login,
		please let us know and we can revoke this certificate and regenerate a new one for you.</p>
		<p>Once you have installed the certificate, you will be able to get to the login page for the GLI portal,
		located at <a href="https://www.gligastransfer.org.au">https://www.gligastransfer.org.au</a>.</p>
		<p>When you do get to the login page, please use the following details:<br>
		Username: $username<br>
		Password: $password<br>
		</p>
		<p>
		Attached to this email is a zip file containing a PDF document describing the data harmonisation and upload process within the portal
		itself, plus the XLS data upload template. The documents are also available to you when you login to the portal.</p>
		<p>For assistance with this process, please contact <a href="mailto:bioinformatics\@ichr.uwa.edu.au">bioinformatics\@ichr.uwa.edu.au</a>.</p>
                
            </body>
        },
    );
    #if ($existing==0)
#{
$msg->attach(
       Type     =>'application/zip',
        Path     => "zip -  -j /var/www/docs/GLI_TLCO_Data_Instructions_V07.pdf /var/www/docs/GLI_TLCO_data_template_Dec2013.xls /home/gli/certs/gli-$certname.p12  |",
	Filename =>'GLI_documents.zip',
        Disposition => 'attachment'
    );



#  	$msg->attach(Type => 'application/pdf',Id  => 'HowToLoadClientCertificate_v1.pdf', Path => '/var/www/docs/HowToLoadClientCertificate_v1.pdf');
#	$msg->attach(Type => 'application/x-pkcs12',Id => "gli-$certname.p12",  Path => "/home/GLIcerts/gli-$certname.p12" );
#}


    $msg->send('smtp','10.0.1.31',Timeout=>60);


#send email to GLI admin

my $gliadmin2  = 'grahamh@ichr.uwa.edu.au';
my $gliadmin1 = 'sanja.stanojevic@sickkids.ca';
my $gliadmin3 = 'B.Thompson@alfred.org.au';
my $gliadmin4 = 'Brian.Graham@sk.lung.ca';


print "<p>Successfully added a new project - $details[10] - for $con_name ($con_email). They have been emailed certificate and instructions, and an email has been sent to you (and other GLI admins) too.</p>";


my $msg2 = MIME::Lite->new(
         To      => $gliadmin1,
	 CC      => $gliadmin2,
	 CC      => $gliadmin3,
	 CC      => $gliadmin4,
	 CC      => 'bioinformatics@ichr.uwa.edu.au',

	# CC	 => 'sanja.stanojevic@sickkids.ca',
	# CC       => 'B.Thompson@alfred.org.au',

         Subject => "GLI Gas Transfer Project: Authorised project for $con_name",
         From    =>'gligastransfer@ichr.uwa.edu.au',
         Type    =>'multipart/related'
    );
    $msg2->attach(
        Type => 'text/html',
        Data => qq{
            <body>
                <p>Dear GLI Admin,<br>
		This is a notification to indicate that a new project - $details[10] - has been authorised for $con_name ($con_email).
		</p>
               
            </body>
        },
    );
$msg2->send('smtp','10.0.1.31',Timeout=>60);


