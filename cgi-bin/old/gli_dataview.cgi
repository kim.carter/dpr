#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

print $cgi->header( -cookie=>[$cookie] );

if ($type eq "v"){

 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;

 if ($proj == 9999) # all sites
 {
	print "Some final modifications are being made to this feature. Cross-site summary statistics for locked dataset will be available here<br>";

 }
 else
{
  my @project = $dbh->selectrow_array("select hash from projects where p_auto = $proj");

# my $ds = lc($project[0]);
 my $hash = $project[0];
 if (-e "/home/gli/projects/$hash/$hash.html"){  #added to ensure no cache
  
  print "<iframe src=\"/projects/$hash/$hash.html?random=".rand()."\" class=\"resframe\"></iframe>";
  }
 else{
  print "No statistics have been generated for this site yet<br>";
  }

 }
 }

sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }

