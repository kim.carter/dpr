#!/usr/bin/perl

use CGI;
use CGI::Carp qw(fatalsToBrowser);
use util;
use Env qw(GLI_HOME);

# Load config form gli.conf
my $cgi = new CGI;
print $cgi->header();
print $cgi->start_html(
	-title=>'Global Lungs Initiative - Data Manager'
	);


print "E=$GLI_HOME";
util::LoadConfig($GLI_HOME);


print $cgi->end_html();

