#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

# Connect to the database
my $dsn = "dbi:mysql:gli";
my $dbuser = "root";
my $dbpass = "thunda32";
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

print $cgi->header( -cookie=>[$cookie] );

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

my ($uname) = $dbh->selectrow_array("select username from users where u_auto = $uid");
my ($ulevel) = $dbh->selectrow_array("select admin from users where u_auto = $uid");

if ($uname ne "gliadmin")
{
	err_login();
}


print "<div><br>";
print "<table>\n";
my ($p) = $dbh->selectrow_array("select count(*) from projects");
print "<tr><td>Total number of projects: </td><td><b>$p</b></td></tr>\n";

#my ($pnonzero) = $dbh->selectrow_array("select count(distinct(project)) from subjects");
#print "<tr><td>Projects with subject data loaded: </td><td><b>".($p-$pnonzero)."</b></td></tr>\n";

my ($plocked) = $dbh->selectrow_array("select count(*) from projects where locked!=0");
print "<tr><td>Projects with loaded and locked: </td><td><b>".($plocked)."</b></td></tr>\n";


my ($s) = $dbh->selectrow_array("select count(*) from subjects");
print "<br><tr><td>Total number of subjects loaded: </td><td><b>$s</b></td></tr>\n";

my ($males) = $dbh->selectrow_array("select count(subject) from data_table where variable=21 and value=1");
print "<br><tr><td>Number of males: </td><td><b>$males</b></td></tr>\n";

my ($females) = $dbh->selectrow_array("select count(subject) from data_table where variable=21 and value=2");
print "<br><tr><td>Number of females: </td><td><b>$females</b></td></tr>\n";


my $sql = "select value from data_table where variable=1 and value!='.'";
my $sth = $dbh->prepare($sql);
$sth->execute;
my $agetotal=0;
my $a=0;
while ( my ($r) = $sth->fetchrow_array() ) {
	$agetotal = $agetotal + $r;
	$a++;
}
print "<br><tr><td>Mean age (N=".$a."): </td><td><b>".$agetotal/$a."</b></td></tr>\n";


print "</table>\n";

print "</div></div>";

#my $query = $dbh->prepare($stmt);
#$query->execute();
#my %projects = ();
#while (my @data = $query->fetchrow_array()){
 #$projects{$data[0]} = $data[1];
 #}
#$query->finish();
$dbh->disconnect();


#print $cgi->header( -cookie=>[$cookie] );

# my $ds = lc($project[0]);


sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }

