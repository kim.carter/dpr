#!/usr/bin/perl

# Modified 09/06/14 to alter mandatory field checking following feedback:
# - In some cases we only have age, not date of test, date of birth and the portal will not accept the dataset unless you have both. These should be an either/or type rule. 
#
# Mand = 1, mandatory
# Mand = 2, mandatory with a separate check (ago or (dot & dob) must be complete)



use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date Delta_Days Today);
use Statistics::Descriptive;
use Spreadsheet::ParseExcel;
use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

print $cgi->header( -cookie=>[$cookie] );

if ($type eq "b"){

 print $cgi->start_multipart_form(
	-method=>'POST',
	-name=>'dcheck',
	-id=>'dcheck'
	);
 print $cgi->filefield(-name=>'glifile', -id=>'glifile', -default=>'Upload a file', -size=>50, -maxlength=>80);
 print $cgi->hidden(-name=>'proj', -id=>'proj', -value=>$proj);
 print $cgi->button( -name=>'up', -value=>'Check Data', -onclick=>"gliupdata();" );
 print $cgi->end_multipart_form();

 print "<img id=\"loading\" src=\"/images/loading.gif\" style=\"visibility:hidden;\" />\n"; 
 print "<div id=\"response\"></div>";

 }
elsif ($type eq "u"){
 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;

 my $phash = $dbh->selectrow_array("select hash from projects where p_auto = $proj");
 
 # Bring in the file to be checked
 my $file = $cgi->param("glifile");
 my $mimetype = $cgi->uploadInfo($file)->{'Content-Type'};
 if (($mimetype !~ /ms-excel/) && ($mimetype !~ /xls/ )){
  print "Your file is not an Excel 95-2003 file (sorry, no XLSX) - $mimetype<br>";
  exit();
  }

 my $outfile = "/home/gli/projects/$phash/file$$\.xls";
 open(OUT,">$outfile") || die "Can't open output file for writing $outfile:$!\n\n";
 binmode OUT;
 print OUT $_ while(<$file>);
 close(OUT);

 # Read in the file and format in a hash of arrays
 my %HoA = ();
 my @headers = ();

 my $parser   = Spreadsheet::ParseExcel->new();
 my $workbook = $parser->parse($outfile);

 if ( !defined $workbook ) {
  die $parser->error(), ".\n";
  }

 my $worksheet = defined $workbook->Worksheet("GLIData") ? $workbook->Worksheet("GLIData") : $workbook->Worksheet(0);
 my ( $row_min, $row_max ) = $worksheet->row_range();
 my ( $col_min, $col_max ) = $worksheet->col_range();

 for my $row ( $row_min .. $row_max ) {
  for my $col ( $col_min .. $col_max ) {
   my $cell = $worksheet->get_cell( $row, $col );
   next unless $cell;
   if ($row == $row_min){
    my $val = $cell->value();
    @{ $HoA{ $val } } = ();
    push @headers, $val;
    }
   else {
    push @{ $HoA{$headers[$col]} }, $cell->value();
    }
   }
  }

 # Remove the file 
 unlink($outfile);

 # set constants
 my $date_format = "YYYY-MM-DD";
 my $date_format_pat = '(\d\d\d\d)-(\d\d)-(\d\d)';
 my $date_year_format = "YYYY";
 my $date_year_format_pat = '(\d\d\d\d)';
 my $idcolname = "study_id";

 # missing values
 my $na = '.';
 my $missing_cut = 5;
 my $sd_dev = 5;
 my $dp = 3;
 # this value is the same as in the data_table.value field
 my $char_len = 100;

 # log all the errors
 my $allerrs = 0;
 my $allwarnings = 0;

 # open a handle to store the results
 my $results = undef;
 open(DAT,">",\$results) || die "Can't open results fh:$!\n\n";;
 
 # Get the valid parameters for each variable
 my $parms = &build_vars_db($dbh);
 
#print "<PRE>";
#print Dumper($parms);
#print "</PRE>";

 # if there's no study_id defined then stop right here and error
 if (!defined $HoA{$idcolname}){
  print "No $idcolname variable present in dataset, cannot proceed\n";
  exit();
  }

 # Get all the study_ids in the database
 # Go through the ids in the file and check we don't have data for this ID already
 my $sub_ids = $dbh->selectall_hashref("select id,s_auto from subjects where project = $proj", "id");
 my @study_ids_indb = ();
 foreach my $id (@{ $HoA{$idcolname} }){
  push @study_ids_indb, $id if defined $sub_ids->{$id};
  }

 # Go through the keys in $parms and check what data is present in the keys if the HoA
 	# if there are columns missing report them in @inparms_notinds
 my @inparms_notinds = ();
 foreach my $k (sort {$a cmp $b} keys %{$parms}){
  push @inparms_notinds, $k if !defined $HoA{$k};
  }
 
 # Go through the keys in HoA and see which do not match keys in $parms - these are likely typos
 	# if there are unidentified columns report them in @inds_notinparms
 my @inds_notinparms = ();
 foreach my $k (sort {$a cmp $b} keys %HoA){
  push @inds_notinparms, $k if !defined $parms->{$k};
  }

 # log errors
 $allerrs++ if scalar(@study_ids_indb) > 0;
 $allerrs++ if scalar(@inparms_notinds) > 0;
 $allerrs++ if scalar(@inds_notinparms) > 0;
 
 ### Might need a check here in case there are multiple headers the same, eg two "age" columns

 print DAT "<table class=\"varcheck\">\n";
 print DAT "<tbody>\n";

 if (scalar(@study_ids_indb) > 0){
  print DAT "<tr><td>Existing study_ids<br><div class=\"errdiv\" id=\"udb_var\">";
  print DAT "$_ already exists in the database cannot load data for this individual<br>\n" foreach @study_ids_indb;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('udb_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Existing study_ids";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }
 
 if (scalar(@inparms_notinds) > 0){
  print DAT "<tr><td>Undefined Variables<br><div class=\"errdiv\" id=\"ud_var\">";
  print DAT "$_<br>\n" foreach @inparms_notinds;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('ud_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Undefined Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }
 
 if (scalar(@inds_notinparms) > 0){
  print DAT "<tr><td>Unidentified Variables<br><div class=\"errdiv\" id=\"ui_var\">";
  print DAT "$_<br>\n" foreach @inds_notinparms;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('ui_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Unidentified Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }

###############
# MISSING, MANDATORY, MEAN AND SD 
###############

my @manderrs = ();
my @misserrs = ();
my $numidvals = scalar(@{ $HoA{$idcolname} });

foreach my $var (sort { $a cmp $b } keys %HoA){
 my $missing = 0;
 foreach my $d (@{ $HoA{$var} }){
  $missing++ if $d eq $na;
  }
 my $numvals = scalar(@{ $HoA{$var} });
 
 if ($parms->{$var}->{'mand'} == 1){
  my $percmiss = int( ($missing / $numvals) * 100);
  push @manderrs, "Mandatory variable $var has $percmiss% missing data" if (($percmiss >= $missing_cut) && ($parms->{$var}->{'mand'} == 1));
  }
 push @misserrs, "Variable $var has $numvals values but should have $numidvals. Make sure you have used \"$na\" as your missing value character" if $numvals != $numidvals;
 $allerrs++ if scalar(@manderrs) > 0;
 $allerrs++ if scalar(@misserrs) > 0;
 }

 ##
 ##specific test for mandatory age ||  DOT && DOB 
 ##

 #AGE
 my $age_missing = 0;
 foreach my $d (@{ $HoA{"age"} })
 {
 	$age_missing++ if $d eq $na;
 }
 my $age_numvals = scalar(@{ $HoA{"age"} });
my  $age_percmiss;
if ($age_numvals ==0)
{
	$age_percmiss=100;
}
else
{
	$age_percmiss = int( ($age_missing / $age_numvals) * 100);
}

 #DOT
 my $dot_missing = 0;
 foreach my $d (@{ $HoA{"dotest"} })
 {
        $dot_missing++ if $d eq $na;
 }
 my $dot_numvals = scalar(@{ $HoA{"dotest"} });
  my $dot_percmiss;
if ($dot_numvals ==0)
{
	$dot_percmiss=100;
}
else
{
	$dot_percmiss = int( ($dot_missing / $dot_numvals) * 100);
}

 #DOB
 my $dob_missing = 0;
 foreach my $d (@{ $HoA{"dobirth"} })
 {
        $dob_missing++ if $d eq $na;
 }
 my $dob_numvals = scalar(@{ $HoA{"dobirth"} });
 my $dob_percmiss;
 if ($dob_percmiss==0)
 {
	$dob_percmiss=100;
}
else
{
	 $dob_percmiss = int( ($dob_missing / $dob_numvals) * 100);
}

 
 if ($age_percmiss < $missing_cut || ($age_percmiss < $missing_cut && $age_percmiss < $missing_cut) )
 {} # all good
 else
 {
	push @manderrs, "Mandatory variable age has $age_percmiss% missing data. Either age or [Date of Birth and Date of Test] should be as complete as possible." if ($age_percmiss >= $missing_cut);
	push @manderrs, "Mandatory variable dobirth has $dob_percmiss% missing data. Either age or [Date of Birth and Date of Test] should be as complete as possible. " if ($dob_percmiss >= $missing_cut);
	push @manderrs, "Mandatory variable dotest has  $dot_percmiss% missing data. Either age or [Date of Birth and Date of Test] should be as complete as possible." if ($dot_percmiss >= $missing_cut);
	$allerrs++ if scalar(@manderrs) > 0;
 
}



 if (scalar(@manderrs) > 0){
  print DAT "<tr><td>Mandatory Variables<br><div class=\"errdiv\" id=\"man_var\">";
  print DAT "$_<br>\n" foreach @manderrs;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('man_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Mandatory Variables";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }
 if (scalar(@misserrs) > 0){
  print DAT "<tr><td>Missing data check<br><div class=\"errdiv\" id=\"miss_var\">";
  print DAT "$_<br>\n" foreach @misserrs;
  print DAT "</div>";
  print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('miss_var');\">View Errors</span></td></tr>";
  }
 else {
  print DAT "<tr><td>Missing data check";
  print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
  }

 print DAT "</tbody>\n";
 print DAT "</table>\n";
 print DAT "<br><br>"; 

 print DAT "<table class=\"varcheck\">\n";
 print DAT "<tbody>\n";

VAR: foreach my $var (sort { $a cmp $b } keys %HoA){

  next unless (defined $parms->{$var});
  my $v_error = 0;
  my $v_mean = undef;
  my $v_sd = undef;
  my %errorsHoA = ();
  my %warningsHoA = ();
  my $v_warning=0;


###############
# MEAN AND SD 
###############

  if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
   my $stat = Statistics::Descriptive::Full->new();
   # Get rid of $na and $nc
   my @data_no_na = ();
   foreach my $d (@{ $HoA{$var} }){
    push @data_no_na, $d eq $na ? 0 : $d;
    }
   $stat->add_data(@data_no_na);
   $v_mean = $stat->mean();
   $v_sd = $stat->standard_deviation();
   }
  
VAL:  for my $i (0 .. $#{ $HoA{$var} }){

   # get the data value
   my $d = $HoA{$var}[$i];

   # if there's an error for this data point
   # store the errors with the corresponding study_id

   # Error Checking begins
 
###############
# INTEGER
###############

   #if ($parms->{$var}->{'type'} eq "INTEGER"){
   if ($parms->{$var}->{'type'} == 1){
    if ($d ne $na){
     # Check max/min
     if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
      if ($d > $parms->{$var}->{'max'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is greater than the allowed maximum of $parms->{$var}->{'max'}";
       $v_error++;    
       next VAL;
       }
      if ($d < $parms->{$var}->{'min'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is less than the allowed minimum of $parms->{$var}->{'min'}";
       $v_error++;    
       next VAL;
       }
      }
     # Check categories
     elsif (defined $parms->{$var}->{'cats'}){
      unless ( grep { $_ eq $d } @{ $parms->{$var}->{'cats'} } ) {
       my $firstcat = ${ $parms->{$var}->{'cats'} }[0];
       my $lastind = $#{ $parms->{$var}->{'cats'} };
       my $lastcat = ${ $parms->{$var}->{'cats'} }[$lastind];
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not one of the expected categories ($firstcat .. $lastcat)";
       $v_error++;    
       next VAL;
       }
      }
     }
    }

###############
# FLOAT
###############

   #elsif ($parms->{$var}->{'type'} =~ /FLOAT\((\d+)\)/){
   elsif ($parms->{$var}->{'type'} == 2){
    if ($d ne $na){
     # Check max/min
     if ((defined $parms->{$var}->{'min'}) && (defined $parms->{$var}->{'max'})){
      if ($d > $parms->{$var}->{'max'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is greater than the allowed maximum of $parms->{$var}->{'max'}";
       $v_error++;    
       next VAL;
       }
      if ($d < $parms->{$var}->{'min'}){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is less than the allowed minimum of $parms->{$var}->{'min'}";
       $v_error++;    
       next VAL;
       }
      }

     # Check dp
     # Allow no more than $dp
     if ($d =~ /\d+.(\d*)/){
      my $dpval = length($1);
      if ($dpval > $dp){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d has more than $dp decimal places";
       $v_error++;    
       next VAL;
       }
      }
     }
    }

###############
# CHARACTER
###############

   #elsif ($parms->{$var}->{'type'} eq "CHARACTER"){
   elsif ($parms->{$var}->{'type'} == 0){
    # length longer than $char_len
    if (length($d) > $char_len){
     push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "Value is greater than the allowed character limit ($char_len)";
     $v_error++;    
     next VAL;
     }
    }

################
# DATE -YEAR ONLY
################

  elsif ($parms->{$var}->{'type'} eq "DATEYEAR"){
    if ($d ne $na){
     # check date format
     if ($d =~ m/$date_year_format_pat/){
      my $year = $1;
      # check validity
      if ($year<1900 || $year > 2020){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not a valid date";
       $v_error++;
       next VAL;
       }
      }
     else {
      # report bad format
      push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not in the right format ($date_year_format)";
      $v_error++;
      next VAL;
      }
     }
    }

###############
# DATE
###############

   #elsif ($parms->{$var}->{'type'} eq "DATE"){
   elsif ($parms->{$var}->{'type'} == 3){
    if ($d ne $na){
     # check date format
     if ($d =~ m/$date_format_pat/){
      my $year = $1;
      my $month = $2;
      my $day = $3;
      my ($y_2day,$m_2day,$d_2day) = Today();
      # check validity
      if (!check_date($year,$month,$day)){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not a valid date";
       $v_error++;    
       next VAL;
       }
      # check if later than today
      elsif (Delta_Days($year,$month,$day,$y_2day,$m_2day,$d_2day) < 0 ){
       push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is a date later than today";
       $v_error++;    
       next VAL;
       }
      }
     else {
      # report bad format
      push @{ $errorsHoA{ $HoA{$idcolname}[$i] } }, "$d is not in the right format ($date_format)";
      $v_error++;    
      next VAL;
      }
     }
    }

   else {
    print DAT "Data type not defined! Contact administrators at bioinformatics\@ichr.uwa.edu.au";
    exit();
    }

###############
# SD
###############


   # Now the value has passed all checks
   # Check it is not an outlier
   if ((defined $v_mean) && ($v_mean > 0)) {
    if ( $d ne $na ){
     if ( $d >=  $v_mean + ($v_sd * $sd_dev) ){

      #modded
      push @{ $warningsHoA{ $HoA{$idcolname}[$i] } }, "This value ($d) is >= 5 sd from the mean ($v_mean)";
      $v_warning++;    
      next VAL;
      }
     if ( $d <=  $v_mean - ($v_sd * $sd_dev) ){
      push @{ $warningsHoA{ $HoA{$idcolname}[$i] } }, "This value ($d) is >= 5 sd from the mean ($v_mean)";
      $v_warning++;
      next VAL;
      }
     }
    }

   }




  if ($v_error){
   # if errors
   print DAT "<tr><td>$var - <span class=\"red\">$v_error Error(s)</span><br><div class=\"errdiv\" id=\"$var\"><br>";
   foreach my $k (sort {$a cmp $b} keys %errorsHoA){
    print DAT "$k => " . join(",", @{ $errorsHoA{$k} }) . "<br>";
    }
   print DAT "</div>";
   print DAT "</td><td class=\"viewerr\"><span class=\"link red\" onclick=\"revealdiv('$var');\">View Errors</span></td></tr>";
   $allerrs++;
   }
  else {

      #warnings
       if ($v_warning!=0){
          # if errors
          print DAT "<tr><td>$var - <span class=\"orange\">$v_warning Warning(s)</span><br><div class=\"errdiv\" id=\"$var\"><br>";
           foreach my $k (sort {$a cmp $b} keys %warningsHoA){
            print DAT "$k => " . join(",", @{ $warningsHoA{$k} }) . "<br>";
           }
        print DAT "</div>";
        print DAT "</td><td class=\"viewerr\"><span class=\"link orange\" onclick=\"revealdiv('$var');\">View Warnings</span></td></tr>";
        $allwarnings++;
      }
      else
      { 

      # if no errors
      print DAT "<tr><td>$var";
      print DAT "</td><td class=\"viewerr\"><img src=\"/images/tick.png\"></td></tr>";
      } 
   }  
  }

 print DAT "</tbody>\n";
 print DAT "</table>\n";

 # load data if there are no errors
 if ($allerrs == 0){

  if ($allwarnings !=0)
  {
	print "<BR><BR><p>No errors found, but warning(s) (eg data variability) have been listed below. Please check to ensure this is correct. You can now view the data by selecting above.<br><br>$results</p>";
  }
  else
  {
  	print "<BR><BR><p>No errors found, your data has been successfully loaded. You can now view the data by selecting above.</p>";
  }

  # Get the study name for the project
  my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

  # Get all the dbids for the variables
  my $var_ids = $dbh->selectall_hashref("select variable,v_auto from variables where study = $study_id[0]", "variable");

  # Now all data is ready to load, make an entry in the load_data table
  $dbh->do("insert into load_data (project,user,filename) VALUES ($proj,$uid,\"$file\")");
  my $ld_id = $dbh->last_insert_id(undef, undef, undef, undef);

  # Now start loading the data
  # First load a person in to the subjects table and store the ID
  for my $id (0 .. $#{ $HoA{$idcolname} }){
   my $sub = ${ $HoA{$idcolname} }[$id];
   $dbh->do("insert into subjects (id,project,load_id) VALUES (\"$sub\",$proj,$ld_id)");
   my $s_id = $dbh->last_insert_id(undef, undef, undef, undef);

   # Now load all data for this individual
   foreach my $var (sort { $a cmp $b } keys %HoA){
    my $v_id = $var_ids->{$var}->{'v_auto'};
    my $val = ${ $HoA{$var} }[$id];
    $dbh->do("insert into data_table (subject,variable,value) VALUES ($s_id,$v_id,\"$val\")");
    }
   }

  # Run the R command to generate the stats
  my $errorfile = "/tmp/Rerrors$$.txt";
  
  system("xvfb-run --auto-servernum --server-num=1 R CMD BATCH --no-save --slave --vanilla '--args $phash' /var/www/html/gli/makeHTML2.R $errorfile");
#system("xvfb-run --auto-servernum --server-num=1 R --no-save --slave --vanilla --args $outname < /var/www/html/gli/makeHTML2.R $errorfile");

  }
 else {
  print "Data errors were found as below. Please fix these and reload<br><br>$results";
  }

 } 


###############
# Subroutines
###############

sub build_vars_db {
my $dbh = shift;

my %allvars = ();

# Get all the variables from project_meta including the con cat and dat types
# Get all the type 0 variables - character
my $key_field = "variable";
my $statement = "select v_auto,variable,type,mand from variables where type = 0";
my $charvars = $dbh->selectall_hashref($statement, $key_field);
# Get all the type 1 variables - categorical
$statement = "select v_auto,variables.variable,cat,type,mand from variables left join variables_cat on variables.v_auto = variables_cat.variable where type = 1";
my %catvars = ();
my $query = $dbh->prepare($statement);
$query->execute();
while (my @data = $query->fetchrow_array()){
 $catvars{$data[1]}{'v_auto'} = $data[0];
 $catvars{$data[1]}{'type'} = $data[3];
 next if !defined $data[2];
 push @{ $catvars{$data[1]}{'cats'} }, $data[2];
 }
$query->finish();
# Get all the type 2 variables - continuous
$statement = "select v_auto,variables.variable,description,units,min,max,prec,type,mand from variables left join variables_con on variables.v_auto = variables_con.variable where type = 2";
my $convars = $dbh->selectall_hashref($statement, $key_field);
# Get all the type 3 variables - date
$statement = "select v_auto,variables.variable,description,type,mand from variables left join variables_dat on variables.v_auto = variables_dat.variable where type = 3";
my $datvars = $dbh->selectall_hashref($statement, $key_field);

# combine the hashes into allvars
my %allvars = ();
@allvars{keys %{$charvars}} = values %{$charvars};
@allvars{keys %catvars} = values %catvars;
@allvars{keys %{$convars}} = values %{$convars};
@allvars{keys %{$datvars}} = values %{$datvars};

return(\%allvars);
}


sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }


__DATA__

 "gas_x_mod" => {
    "type" => "INTEGER",
    "desc" => "Gas transfer equipment model",
    "cats" => {
1 => "Collins PFT System (USA)",
2 => "CosMed (Italy)",
3 => "Jaeger MasterScreen (Germany)",
4 => "Ndd Easy Pro One (Switzerland)",
5 => "nSpireHDpft (USA)",
6 => "Medisoft BodyBox (Belgium)",
7 => "Medisoft HypAir (Belgium)",
8 => "Morgan BodyBox (USA)",
9 => "Morgan Other (USA)",
10 => "SensorMedics Spectra Autobox (USA)",
11 => "SensorMedics Vmax Encore (USA)",
12 => "Ultima PF(MedGraphics, USA)",
13 => "Elite (Medgraphics, USA)",
14 => "Other"
	},
    },
 "gas_x_mod_oth" => {
    "type" => "CHARACTER",
    "desc" => "Gas transfer equipment model - if other give details",
    },
 "gas_x_year_manf" => {
    "type" => "DATEYEAR",
    "desc" => "Year manufactured"
    },
 "gas_x_soft" => {
    "type" => "CHARACTER",
    "desc" => "Software name"
    },
 "gas_x_soft_ver" => {
    "type" => "CHARACTER",
    "desc" => "Software version"
    },
 "same_soft" => {
    "type" => "INTEGER",
    "desc" => "Was the same software used for all subjects",
    "cats" => {
1 => "Yes",
2 => "No"
},
    },
 "same_equip" => {
    "type" => "INTEGER",
    "desc" => "Was the same equipment used for all subjects",
    "cats" => {
1 => "Yes",
2 => "No"
},
    },
 "tlco_co" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Carbon Monoxide used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_he" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Helium used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_ch4" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Methane used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_c2h2" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Acetelyne used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_n" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Nitrogen used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_o" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Oxygen used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_no" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Nitrogen Monoxide used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "tlco_ne" => {
    "type" => "FLOAT(3)",
    "desc" => "Concentration of Neon used for TLCO test gas",
    "units" => "perc",
    "min" => 0, 
    "max" => 100
    },
 "vol_wo_meth" => {
    "type" => "INTEGER",
    "desc" => "What volume washout method was used?",
    "cats" => {
1 => "Fixed",
2 => "Computer selected from tracer gas washout",
3 => "Computer selected from tracer gas washout and manually verified",
4 => "Other"
},
    },
 "vol_wo_fix_o2" => {
    "type" => "FLOAT(3)",
    "desc" => "If washout volume was fixed what volume was used if VC ≥ 2 litres?",
    "min" => 0, 
    "max" => 5
    },
 "vol_wo_fix_u2" => {
    "type" => "FLOAT(3)",
    "desc" => "If washout volume was fixed what volume was used if VC < 2 litres?",
    "min" => 0, 
    "max" => 2
    },
 "ds_vol_meth" => {
    "type" => "INTEGER",
    "desc" => "What dead space volume method was used?",
    "cats" => {
1 => "Fixed",
2 => "Measured from tracer gas washout",
3 => "Calculated from body size",
4 => "Other"
},
    },
 "ds_vol_fix" => {
    "type" => "FLOAT(3)",
    "desc" => "If fixed what total dead space volume was used?",
    "min" => 0, 
    "max" => 5
    },
 "ds_vol_sub" => {
    "type" => "FLOAT(3)",
    "desc" => "If fixed what subject dead space volume was used?",
    "min" => 0, 
    "max" => 5
    },
 "ds_vol_eqp" => {
    "type" => "FLOAT(3)",
    "desc" => "If fixed what equipment dead space volume was used?",
    "min" => 0, 
    "max" => 1
    },
 "ds_vol_bs_for" => {
    "type" => "CHARACTER",
    "desc" => "If deadspace volume was calculated from body size, what formula was used?",
    },
 "mou_bac_brand" => {
    "type" => "CHARACTER",
    "desc" => "What mouthpiece or bacterial brand was used?"
    },
 "mou_bac_filt" => {
    "type" => "INTEGER",
    "desc" => "Was deadspace of filter included in calculation?",
    "cats" => {
1 => "Yes",
2 => "No"
},
    },
 "meth_bh" => {
    "type" => "INTEGER",
    "desc" => "Method used for calculating breath hold time?",
    "cats" => {
1 => "Jones-Meade",
2 => "Ogilvie",
3 => "Epidemiological Standardisation Project",
4 => "Other"
},
    },
 "meth_bh_oth" => {
    "type" => "CHARACTER",
    "desc" => "Breath hold method - if other, please specify",
    },
 "bre_ht_col" => {
    "type" => "FLOAT(3)",
    "desc" => "What target breath hold time was used?",
    },
 "bes_manu" => {
    "type" => "INTEGER",
    "desc" => "How was the reported TLCO value selected?",
    "cats" => {
1 => "Automatically selected by software",
2 => "Manually selected largest value",
3 => "Average of at least 2 acceptable tests",
4 => "Other"
     },
    },
 "vol_calib" => {
    "type" => "INTEGER",
    "desc" => "Flow/Volume equipment calibration frequency",
    "cats" => {
1 => "Prior to every test",
2 => "Daily",
3 => "Weekly",
4 => "Monthly",
5 => "As per manufacturer",
6 => "Other"
     },
    },
 "ga_calib" => {
    "type" => "INTEGER",
    "desc" => "Gas analysers calibration frequency",
    "cats" => {
1 => "Prior to every test",
2 => "Daily",
3 => "Weekly",
4 => "Monthly",
5 => "As per manufacturer",
6 => "Other"
     },
    },
 "vol_qc" => {
    "type" => "CHARACTER",
    "desc" => "Flow/Volume equipment quality control method and frequency (select all that apply)",
    "cats" => {
1 => "Weekly",
2 => "Monthly",
3 => "Biological control",
4 => "TLCO validator",
5 => "Syringe dilution",
6 => "Other"
     },
    },
 "ga_qc" => {
    "type" => "INTEGER",
    "desc" => "Gas analysers quality control frequency see above",
    "cats" => {
1 => "Weekly",
2 => "Monthly",
3 => "Biological control",
4 => "TLCO validator",
5 => "Syringe dilution",
6 => "Other"
     },
    },
 "adh_spec" => {
    "type" => "INTEGER",
    "desc" => "Adherence to published TLCO/DLCO guidelines",
    "cats" => {
1 => "Fully",
2 => "Partially",
3 => "No"
     },
    },
 "adh_guid" => {
    "type" => "CHARACTER",
    "desc" => "If adherence, which guidelines were used?",
    "cats" => {
1 => "ATS/ERS 2005",
2 => "ATS 1994",
3 => "ers 1993",
4 => "ers 1983"
     },
    },
 "nose_clip" => {
    "type" => "INTEGER",
    "desc" => "Nose clip used during testing?",
    "cats" => {
1 => "Not used",
2 => "Nose clip",
3 => "Fingers"
     },
    },
 "rest_mes" => {
    "type" => "FLOAT(3)",
    "desc" => "How long did the subject rest between measurements?",
    "min" => 0, 
    "max" => 1000
    },
 "pub" => {
    "type" => "FILE",
    "desc" => "Has your data been published. If yes please upload PDF of publiction",
    }
