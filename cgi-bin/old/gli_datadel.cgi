#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Date::Calc qw(check_date);
use Statistics::Descriptive;
use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');
my $load = $cgi->param('load');;

# Check for Cookie or err
my $sid = $cgi->cookie("GLI_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'GLI_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

print $cgi->header( -cookie=>[$cookie] );

sleep(2); #make the page wait to refresh

if ($type eq "r"){

 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;


 my ($ulevel) = $dbh->selectrow_array("select admin from users where u_auto = $uid");
 my ($locked) = $dbh->selectrow_array("select locked from projects where p_auto = $proj");


 # get all the load times for this project
 my $load_ids = $dbh->selectall_hashref("select ld_auto, load_date, username, filename from load_data as ld, users as u where u.u_auto = ld.user and ld.project = $proj", "ld_auto");
 print "<div class=\"fileman\" id=\"fileman\">\n";
 print "<table class=\"files\">";
 foreach my $l_id (sort keys %{ $load_ids }){
  
  if ($locked == 0 )#unlocked project
  {
	print "<tr><td>$load_ids->{$l_id}->{'load_date'}</td><td>$load_ids->{$l_id}->{'username'}</td><td>$load_ids->{$l_id}->{'filename'}</td><td><img class=\"link\" src=\"/images/erase_small.png\" onclick=\"delete_data($l_id,'rd');sleepStupidly(1);datadel($proj,'r');\"</td></tr>\n";
  }
  elsif ($locked==1 && $ulevel==0 )#locked dataset, normal user
  {
	print "<tr><td>$load_ids->{$l_id}->{'load_date'}</td><td>$load_ids->{$l_id}->{'username'}</td><td>$load_ids->{$l_id}->{'filename'} (locked)</td><td><img title=\"The data is locked - contact the GLI admin to unlock\" class=\"link\" src=\"/images/erase_small.png\" </td></tr>\n";
  }
  elsif ($locked==1 && $ulevel ==1 )#locked dataset, admin user
  {
	print "<tr><td>$load_ids->{$l_id}->{'load_date'}</td><td>$load_ids->{$l_id}->{'username'}</td><td>$load_ids->{$l_id}->{'filename'} (locked)</td><td><img title=\"The data is locked - please select above to unlock.\" class=\"link\" src=\"/images/erase_small.png\" </td></tr>\n";
  
  }


  
 } # end for

 print "</table>";
 print "</div>\n";
 print "<div id=\"result\"></div>";

 $dbh->disconnect();
 }
elsif ($type eq "rd"){
 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;

 $dbh->do("delete from load_data where ld_auto = $load");
 $dbh->disconnect();
 }
elsif ($type eq "ld"){
 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;
 $dbh->do("update load_data set locked=1 where ld_auto = $load");
 $dbh->disconnect();
 }
 elsif ($type eq "ud"){
 # Connect to the database
 my $dbuser = "root";
 my $dbpass = "thunda32";
 my $dsn = "dbi:mysql:gli";
 my $dbh = DBI->connect($dsn, $dbuser, $dbpass) or die $DBI::errstr;
 $dbh->do("update load_data set locked=0 where ld_auto = $load");
 $dbh->disconnect();
 }

sub err_login {
 my $url = '/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }

