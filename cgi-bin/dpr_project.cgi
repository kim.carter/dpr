#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Data::Dumper;
use MIME::Lite;
use Config::IniFiles;

# Check for Cookie or err
my $cgi = new CGI;
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');


my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my $uid = $session->param("userid");
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');

# print html header for any ajax'd pages
print $cgi->header( -cookie=>[$cookie] );


# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbuser');;
my $dbpass =  $cfg->val('DATABASE','dbpass');
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

my ($uname) = $dbh->selectrow_array("select username from users where u_auto = $uid");
my ($ulevel) = $dbh->selectrow_array("select admin from users where u_auto = $uid");

my @project = $dbh->selectrow_array("select project,title,hash from projects where p_auto = $proj");
# override project if 9999 (all sites)
if ($proj==9999)
{
	@project = ("All sites", "All sites with locked data", "");
}


my ($privs) = $dbh->selectrow_array("select description from users_projects as up, project_privs as pp where up.user_level = pp.pp_auto and user = $uid and project = $proj");



#check if data loaded for this project
my ($loaded) = $dbh->selectrow_array("select ld_auto from load_data where project=$proj");
if  (!defined $loaded || $loaded <1)
{
	$loaded=-1;
}


if ($ulevel==1) #admin
{
	$privs = "Admin";
}

#check for lock updates
if ($type eq "ud" && $ulevel==1)
{
	$dbh->disconnect();
	my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
	my $dbuser = $cfg->val('DATABASE','dbupdateuser');;
	my $dbpass =  $cfg->val('DATABASE','dbupdatepass');
	my %attr = (
		RaiseError => 1,
		AutoCommit => 0
		);

	# connect to the database
	$dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

	#unlock project
	$dbh->do("update projects set locked=0 where p_auto = $proj");

	$dbh->commit or do { print $dbh->errstr; $dbh->rollback; $dbh->disconnect(); exit; }; # commit the changes or die and rollback

	#if project is unlocked, we need to update the global data.csv

	#get the study
	my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

	# get variable details
	my $var_id_ref = $dbh->selectall_arrayref("select v_auto,variable,type from variables where study = $study_id[0] order by variable asc");
        my @var_names;
        my @var_ids;
        my @var_types;
        foreach my $row (@$var_id_ref)
        {
      	  my ( $v_auto, $v_name, $v_type ) = @$row;
      	  push @var_ids,$v_auto;
      	  push @var_names,$v_name;
      	  push @var_types,$v_type;
        }
        my $varlist = join(",", @var_ids);

	my $ds_id_ref = $dbh->selectall_arrayref("select p_auto,title from projects where locked=1");
        my @projects;
	my @titles;
	foreach my $row (@$ds_id_ref)
        {
		my ($p,$t) = @$row;
		push @projects, $p;
		push @titles, $t;
	}

	###  sanity checks - copy R code into shiny for the first time
        my $dir = "/srv/shiny-server/dpr/global";
        unless(-d $dir)
        {
      	 #make output dir
      	 `mkdir -p $dir`;

      	 #fix permissions
      	 `chmod 777 $dir`;
        }


      	# always copy server and ui components - in case changes made
      	`cp ../shiny/*.R ../shiny/*.md $dir`;



	if (! @projects)  # no locked project, so delete global data.csv
	{
		if (-e "$dir/data.csv")
             	{
      	       		unlink("$dir/data.csv");
             	}
	}
	else
	{
		open(CSV,">$dir/data.csv");
		#header
		print CSV "project,".join(",", @var_names)."\n";

		#get variable types
	      	my @types;
	      	foreach my $v (@var_types)
	      	{
			if ($v eq "0") # string or date
			{
				push @types,"string";  # should be factored in R
			}
			elsif ($v eq "3") # date
			{
				push @types,"date";  # should be date in R
			}
			elsif ($v eq "2")
			{
				push @types,"number";  # should be numerical
			}
			elsif ($v eq "1")
			{
				push @types,"factor";  # should be factored in R
			}
	      	}
		print CSV ",".join(",", @types)."\n"; #need to add extra comma at start for project column

		# now get the data for each locked project
		for (my $p=0; $p<=$#projects; $p++)
		{
			my $p_id_ref = $dbh->selectall_arrayref("select s_auto,id from subjects where project = ".$projects[$p]);

			my @pids;
		        my @origids;
		        foreach my $row (@$p_id_ref)
		        {
		      	  my ( $pid,$id) = @$row;
		      	  push @pids, $pid;
		      	  push @origids,$id;
		        }
		        my $pidlist = join(",", @pids);

		      	#get the data
		      	for(my $i=0; $i<=$#pids; $i++)
		      	{
		      	 	my $person = $pids[$i];
				my $data_ref = $dbh->selectall_arrayref("select value from data_table where variable in ($varlist) and subject=$person");
				my @data;
				foreach my $row (@$data_ref)
				{
					my ($d) = @$row;
					push @data,$d;
				}
				$titles[$p] =~ s/,//g; # sanity check for commas
				print CSV "$titles[$p],".join(",", @data)."\n";
		      	}
		}

		close(CSV);
	}




}
elsif ($type eq "cld")  #check lock project
{
	if (1==1)
	{
		print "here<script>alert(\"test\");</script>";
	}
	else
	{

	}

}
elsif ($type eq "ld")  #lock project
{



	$dbh->disconnect();
	my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
	my $dbuser = $cfg->val('DATABASE','dbupdateuser');
	my $dbpass =  $cfg->val('DATABASE','dbupdatepass');
	my %attr = (
		RaiseError => 1,
		AutoCommit => 0
		);

	# connect to the database
	$dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

	$dbh->do("update projects set locked=1 where p_auto = $proj");

	$dbh->commit or do { print $dbh->errstr; $dbh->rollback; $dbh->disconnect(); exit; }; # commit the changes or die and rollback

	#send email to DPR admin
	my $dpradmin  = $cfg->val('SITE','email');

	my $msg2 = MIME::Lite->new(
	         To      => $dpradmin,
	 Subject => $cfg->val('SITE','subtitle').": Project $project[0] has been Locked by $uname",
         From    =>'DPR Portal',
         Type    =>'multipart/related'
    	);
	$msg2->attach(
		Type => 'text/html',
	        Data => qq{
        	<body>
                <p>Dear DPR Admin,<br>
		This is a notification to indicate that the project $project[0] has been locked for access (and inclusion to global data) by $uname.
		</p>

            </body>
        },
	);

	eval
	{
		$msg2->send('smtp',$cfg->val('EMAIL','server'),Timeout=>10);
	};

	#die "MIME::Lite->send failed: $@\n" if $@;





	#if project is unlocked, we need to update the global data.csv

	#get the study
	my @study_id = $dbh->selectrow_array("select study from projects where p_auto = $proj");

	# get variable details
	my $var_id_ref = $dbh->selectall_arrayref("select v_auto,variable,type from variables where study = $study_id[0] order by variable asc");
        my @var_names;
        my @var_ids;
        my @var_types;
        foreach my $row (@$var_id_ref)
        {
      	  my ( $v_auto, $v_name, $v_type ) = @$row;
      	  push @var_ids,$v_auto;
      	  push @var_names,$v_name;
      	  push @var_types,$v_type;
        }
        my $varlist = join(",", @var_ids);

	my $ds_id_ref = $dbh->selectall_arrayref("select p_auto,title from projects where locked=1");
        my @projects;
	my @titles;
	foreach my $row (@$ds_id_ref)
        {
		my ($p,$t) = @$row;
		push @projects, $p;
		push @titles, $t;
	}

	###  sanity checks - copy R code into shiny for the first time
        my $dir = "/srv/shiny-server/dpr/global";
        unless(-d $dir)
        {
      	 #make output dir
      	 `mkdir -p $dir`;

      	 #fix permissions
      	 `chmod 777 $dir`;
        }


	# always copy server and ui components
      	`cp ../shiny/*.R ../shiny/*.md $dir`;



	if (! @projects)  # no locked project, so delete global data.csv
	{
		if (-e "$dir/data.csv")
             	{
      	       		unlink("$dir/data.csv");
             	}
	}
	else
	{
		open(CSV,">$dir/data.csv");
		#header
		print CSV "project,".join(",", @var_names)."\n";

		#get variable types
	      	my @types;
	      	foreach my $v (@var_types)
	      	{
			if ($v eq "0") # string or date
			{
				push @types,"string";  # should be factored in R
			}
			elsif ($v eq "3") # date
			{
				push @types,"date";  # should be date in R
			}
			elsif ($v eq "2")
			{
				push @types,"number";  # should be numerical
			}
			elsif ($v eq "1")
			{
				push @types,"factor";  # should be factored in R
			}
	      	}
		print CSV ",".join(",", @types)."\n"; #need to add extra comma at start for project column

		# now get the data for each locked project
		for (my $p=0; $p<=$#projects; $p++)
		{
			my $p_id_ref = $dbh->selectall_arrayref("select s_auto,id from subjects where project = ".$projects[$p]);

			my @pids;
		        my @origids;
		        foreach my $row (@$p_id_ref)
		        {
		      	  my ( $pid,$id) = @$row;
		      	  push @pids, $pid;
		      	  push @origids,$id;
		        }
		        my $pidlist = join(",", @pids);

		      	#get the data
		      	for(my $i=0; $i<=$#pids; $i++)
		      	{
		      	 	my $person = $pids[$i];
				my $data_ref = $dbh->selectall_arrayref("select value from data_table where variable in ($varlist) and subject=$person");
				my @data;
				foreach my $row (@$data_ref)
				{
					my ($d) = @$row;
					push @data,$d;
				}
				$titles[$p] =~ s/,//g; # sanity check for commas
				print CSV "$titles[$p],".join(",", @data)."\n";
		      	}
		}

		close(CSV);
	}



}


my ($locked) = $dbh->selectrow_array("select locked from projects where p_auto = $proj");


# Top of the page project details
# Give the project name and description
print "<div style=\"float:left\"><img src=\"/dprs/images/project.png\"/></div>";
print "<div><br>";
print "<table>";
if ($proj==9999)
{
	print "<tr class=\"projhead\"><td><b>Project Name:</b></td><td>$project[0] </td></tr>";
}
else
{
	print "<tr class=\"projhead\"><td><b>Project Name:</b></td><td>$project[0] (unlocked)</td></tr>" if $locked==0;
	print "<tr class=\"projhead\"><td><b>Project Name:</b></td><td>$project[0] (locked)</td></tr>" if $locked==1;
}
print "<tr class=\"projhead\"><td><b>User level:</b></td><td>$privs</td></tr>";
print "<tr class=\"projhead\"><td><b>Project Title:</b></td><td>$project[1]</td></tr></table><br>";

#Top menus
#uploads
print "<center><img src=\"/dprs/images/upload_folder.png\" /><span class=\"link\" onclick=\"datacheck($proj,'b');\">Upload / Check Data</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" if $locked==0;
print "<center><img src=\"/dprs/images/upload_folder.png\" /><span class=\"link\" onclick=\"alert('The project data is locked - please contact the DPR admin to unlock');\">Upload / Check Data</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" if ($locked==1 && $ulevel==0);
print "<center><img src=\"/dprs/images/upload_folder.png\" /><span class=\"link\" onclick=\"alert('The project data is locked - please unlock first');\">Upload / Check Data</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" if ($locked==1 && $ulevel==1);

#deletes
print "<img src=\"/dprs/images/file_delete.png\" /><span class=\"link\" onclick=\"datadel($proj,'r');\">Delete Data</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" if $locked==0;
print "<img src=\"/dprs/images/file_delete.png\" /><span class=\"link\" onclick=\"alert('The project data is locked - please contact the DPR admin to unlock');\">Delete  Data</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" if ($locked==1 && $ulevel==0);
print "<img src=\"/dprs/images/file_delete.png\" /><span class=\"link\" onclick=\"alert('The project data is locked - please unlock first');\">Delete Data</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n" if ($locked==1 && $ulevel==1);

print "<img src=\"/dprs/images/statistics.png\" /><span class=\"link\" onclick=\"dataview($proj,'v');\">View Site Stats</span>\n";

if ($ulevel==1) #admin
{

	print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"/dprs/images/lock.png\"/><span class=\"link\" onclick=\"unlock_data($proj,'ud');projheader($proj,'h');\">Unlock</span>\n" if $locked==1;
	print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"/dprs/images/unlock.png\"/><span class=\"link\" onclick=\"lock_data($proj,'ld');projheader($proj,'h');\">Lock</span>\n" if $locked==0;

}
else  #normal user
{
	print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"/dprs/images/lock.png\"/><span class=\"link\" onclick=\"alert('The project data is locked - please contact the DPR admin to unlock');\">Unlock</span>\n" if $locked==1;
	print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"/dprs/images/unlock.png\"/><span class=\"link\" onclick=\"lock_data($proj,'ld');projheader($proj,'h');\">Lock</span>\n" if $locked==0;

}

#Removed
#print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"/dprs/images/statistics.png\" /><span class=\"link\" onclick=\"dataview('9999','v');\">View Global Stats</span>\n";
print "</center>\n";

print "</div><br><hr/>\n";

# make a div for the dates this project has a run for
# display the dates

if ($proj==9999)
{
	print "<div class=\"maindisplay\" id=\"display\">";

	if (-e "/srv/shiny-server/dpr/global/data.csv")
	{
		print "<iframe src=\"http://localhost:3838/dpr/global/\" class=\"resframe\"></iframe>";
	}
	else {
	 print "<center>No dataset are locked (for global access) currently. Please re-visit the page once datasets are available.</center>"
	}

	print "</center></div></div>";

}
else
{

	print "<div class=\"maindisplay\" id=\"display\"><center>Please select from the above menu to Upload a new dataset, to delete an existing UNLOCKED dataset, or to view summary stats on the uploaded data.</center><br/>";

	my ($cfname) = $dbh->selectrow_array("select contact_fname from projects where p_auto = $proj");
	my ($csname) = $dbh->selectrow_array("select contact_sname from projects where p_auto = $proj");
	my ($cemail) = $dbh->selectrow_array("select contact_email from projects where p_auto = $proj");
	my ($cphone) = $dbh->selectrow_array("select contact_phone from projects where p_auto = $proj");
	my ($org) = $dbh->selectrow_array("select contact_org from projects where p_auto = $proj");
	my ($address) = $dbh->selectrow_array("select contact_address from projects where p_auto = $proj");
	my ($country) = $dbh->selectrow_array("select country from projects where p_auto = $proj");

	my ($custname) = $dbh->selectrow_array("select custodian_name from projects where p_auto = $proj");
	my ($custemail) = $dbh->selectrow_array("select custodian_email from projects where p_auto = $proj");
	my ($custphone) = $dbh->selectrow_array("select custodian_phone from projects where p_auto = $proj");

	my ($desc) = $dbh->selectrow_array("select description from projects where p_auto = $proj");



	print "<div id=\"cols\">";
	print "<div id=\"leftcol\">";

	print "<center><h4>Administrative Details</h4>\n";
	print "<table border=\"0\">\n";

	if ($csname eq "")
	{
		print "<tr><td><b>Contact name</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Contact email</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Contact phone</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Organisation</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Address</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Country</b></td><td>not supplied</td></tr>";
		print "<tr/>";
		print "<tr><td><b>Custodian name</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Custodian email</b></td><td>not supplied</td></tr>";
		print "<tr><td><b>Custodian phone</b></td><td>not supplied</td></tr>";
		print "<tr/>";
		print "<tr><td><b>Description</b></td><td>not supplied</td></tr>";

	}
	else
	{
		print "<tr><td><b>Contact name</b></td><td>$cfname $csname</td></tr>";
		print "<tr><td><b>Contact email</b></td><td>$cemail</td></tr>";
		print "<tr><td><b>Contact phone</b></td><td>$cphone</td></tr>";
		print "<tr><td><b>Organisation</b></td><td>$org</td></tr>";
		print "<tr><td><b>Address</b></td><td>$address</td></tr>";
		print "<tr><td><b>Country</b></td><td>$country</td></tr>";
		print "<tr/>";
		print "<tr><td><b>Custodian name</b></td><td>$custname</td></tr>";
		print "<tr><td><b>Custodian email</b></td><td>$custemail</td></tr>";
		print "<tr><td><b>Custodian phone</b></td><td>$custphone</td></tr>";
		print "<tr/>";
		print "<tr><td><b>Description</b></td><td>$desc</td></tr>";
	        print "<tr><td><b>MOU Document</b></td><td><a target=\"_blank\" href=\"/projects/$project[2]/MOU.pdf\">MOU.pdf</a></td></tr>";

	}
	print "</table></center>";

	print "</div>\n";
	print "<div id=\"rightcol\">";

	print "<center><h4>Metadata</h4>\n";

	#sleep(1);

	# Get all the variables from project_meta including the con cat and dat types
	# Get all the type 0 variables - character
	my $key_field = "variable";
	my $statement = "select pm_auto,project_meta.variable,description,type,qorder from project_meta where type = 0";
	my $charvars = $dbh->selectall_hashref($statement, $key_field);

	# Get all the type 1 variables - categorical
	#$statement = "select pm_auto,project_meta.variable,description,cat,code,type,qorder from project_meta left join project_meta_cat on project_meta.pm_auto = project_meta_cat.variable where type = 1";
	$statement = "select pm_auto,project_meta.variable,description,cat,code,type,qorder,oth from project_meta left join project_meta_cat on project_meta.pm_auto = project_meta_cat.variable left join project_meta_oth on project_meta.pm_auto = project_meta_oth.variable where type = 1";
	my %catvars = ();
	my $query = $dbh->prepare($statement);
	$query->execute();
	while (my @data = $query->fetchrow_array()){
	 $catvars{$data[1]}{'pm_auto'} = $data[0];
	 $catvars{$data[1]}{'description'} = $data[2];
	 $catvars{$data[1]}{'type'} = $data[5];
	 $catvars{$data[1]}{'qorder'} = $data[6];
	 #next if !defined $data[4];
	 $catvars{$data[1]}{'cats'}{0} = "-- Select --";
	 $catvars{$data[1]}{'cats'}{$data[3]} = $data[4];
	 $catvars{$data[1]}{'oth'} = $data[7];
	 }
	$query->finish();

	# Get all the type 2 variables - continuous
	$statement = "select pm_auto,project_meta.variable,description,units,min,max,prec,type,qorder from project_meta left join project_meta_con on project_meta.pm_auto = project_meta_con.variable where type = 2";
	my $convars = $dbh->selectall_hashref($statement, $key_field);

	# Get all the type 3 variables - file
	my $statement = "select pm_auto,project_meta.variable,description,type,qorder from project_meta where type = 3";
	my $filevars = $dbh->selectall_hashref($statement, $key_field);

	# combine the hashes into allvars
	my %allvars = ();
	@allvars{keys %{$charvars}} = values %{$charvars};
	@allvars{keys %catvars} = values %catvars;
	@allvars{keys %{$convars}} = values %{$convars};
	@allvars{keys %{$filevars}} = values %{$filevars};

	#print "<pre>";
	#print Dumper(\%allvars);
	#print "</pre>";

	# Need to find a better way of doing this.
	# Currently the submission of the form and subsequent loading in to the database
	# takes longer than it does to refresh the page displaying the form
	# need to play with sync a bit more

	# Get all the values for this project
	# 	Set the previously set values in the display to SELECTED
	# Need to create a hash that for text boxes looks like:
	# variablename => value
	my $query = "select variable, value from project_meta_data where project = $proj";
	my %projdata_text = map { $_->[0], $_->[1]} @{$dbh->selectall_arrayref($query)};
	# and for dropdowns looks like
	# variablename => { value => "SELECTED" }
	my $query = "select variable, value from project_meta_data where project = $proj";
	my %projdata_drop = map { $_->[0] => { $_->[1] => { 'selected' => 'yes' } } } @{$dbh->selectall_arrayref($query)};

	# Print form
	print $cgi->start_multipart_form(
	 -method=>'POST',
	 -action=>'/dprs/cgi-bin/dpr_projectmeta.cgi',
	  -name=>'projectmeta',
	 -id=>'projectmeta',
	 );

	print $cgi->hidden(-name=>'proj',-id=>"proj",-default=>$proj);
	print $cgi->hidden(-name=>'loaded',-id=>"loaded",-default=>$loaded);

	print "<table>\n";
	foreach my $var (sort { $allvars{$a}{'qorder'} <=> $allvars{$b}{'qorder'} } keys %allvars){
	 my $numcats = scalar(keys %{ $allvars{$var}{'cats'} });
	 print "<tr><td>$allvars{$var}{'description'}</td><td>";

	 # if type 0 (character) or 2 (continuous) - print a text box
	 if ( ($allvars{$var}{'type'} == 0) || ($allvars{$var}{'type'} == 2) ){
	  my $val = $projdata_text{ $allvars{$var}{'pm_auto'} };
	  print $cgi->textfield(-name=>$var, -size=>30, -value=>$val) . "</td></tr>\n";
	  }
	 # if type 3 (file) - print a file upload with filename if one already uploaded
	 elsif ($allvars{$var}{'type'} == 3){
	  my $val = $projdata_text{ $allvars{$var}{'pm_auto'} };
	  my $val_full = $val;
	  if (length($val)>40){ $val = substr($val,0,15) . "..." . substr($val,-15,15); }
	  print $cgi->filefield(-name=>$var,-size=>30,-maxlength=>100) . "<br><span class=\"upfile\"><a target=\"_blank\" href=\"../projects/$project[2]/$val\">$val</a></span><br>";
	  # print $cgi->filefield(-name=>$var,-size=>30,-maxlength=>100) . "<br>";
	  print $cgi->hidden(-name=>"$var\_hidden",-default=>$val_full)."</td></tr>\n";
	  }
	 elsif (($allvars{$var}{'type'} == 1) && $numcats > 0){
	  my %attr = ();
	  if (defined $projdata_drop{ $allvars{$var}{'pm_auto'} } ){ #only pre-fill if we have data to prefill
	   %attr = %{ $projdata_drop{ $allvars{$var}{'pm_auto'} } };
	   }
	  # now check if there is an other in this drop down
	  #  if there is then need to check the current value of this drop down
	  #  if this value is "Other" then need to pull the value for "Other" in to a text box that is enabled
	  #  otherwise create a text box that is disabled
	  #  either way there needs to be some js attached to the drop down to enable/disable the box if Other is selected/deselected
	  if ($allvars{$var}{'oth'} > 0){
	   # get the id of the "Other" option in the dropdown for this variable
	   my $vothid = $allvars{$var}{'oth'};

	   # print the dropdown with the javascript attached to the form object
	   print $cgi->popup_menu(-name=>$var, -id=>$var, -values=>[sort { $a <=> $b } keys %{ $allvars{$var}{'cats'} }], -labels=> \%{ $allvars{$var}{'cats'} }, -attributes=>\%attr, -onchange=>"showoth(\'$var\',$vothid);") . "</td></tr>\n";

	   # print the text box with its value and whether the box is disabled or not
	   # get the id for this variable
	   my $vid = $allvars{$var}{'pm_auto'};
	   # check to see if "Other" is selected
	   my $ro = $attr{$vothid}{'selected'} eq "yes" ? "" : "readonly";
	   # make the name/id for the other field to display with the text box
	   my $vothname = $var."_other";
	   # get the current value for "Other" from the database - this can be blank if "Other" is not selected OR if "Other" is selected and nothing entered
	   my $vothval = $dbh->selectrow_array("select value from project_meta_data_other where project = $proj and variable = $vid");
	   # Can't get CGI perl to print a text box that can be changed with javascript so have to do manually
	#   print "<tr><td></td><td>" . $cgi->textfield(-name=>$vothname, -id=>$vothname, -size=>30, -value=>$vothval, -readonly=>$ro) . "</td></tr>\n";
	   print "<tr><td></td><td><input type=\"text\" name=\"$vothname\" size=\"30\" $ro id=\"$vothname\" value=\"$vothval\"></td></tr>\n";
	   }
	  # if no "Other" option exists, just draw the standard drop down
	  else {
	   print $cgi->popup_menu(-name=>$var, -id=>$var, -values=>[sort { $a <=> $b } keys %{ $allvars{$var}{'cats'} }], -labels=> \%{ $allvars{$var}{'cats'} }, -attributes=>\%attr) . "</td></tr>\n";
	   }
	  }

	 } #end for
	print "</table>\n";
	print "<button name='pm_submit' class='button' type=\"submit\" onclick=\"save_meta('projectmeta',$proj);\">Submit</button>";
	#print "<button name='pm_submit' class='button' type=\"submit\">Submit</button>";
	print "<button name='resetform' class='button' onclick=\"projheader($proj,'h');\">Reset</button";

	print $cgi->end_multipart_form();

	print "</center></div></div>";
}

$dbh->disconnect();

sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
