#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use Config::IniFiles;

# Check for Cookie or err
my $cgi = new CGI;
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my $uid = $session->param("userid");
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');

# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbuser');;
my $dbpass =  $cfg->val('DATABASE','dbpass');
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);


my ($uname) = $dbh->selectrow_array("select username from users where u_auto = $uid");
my ($ulevel) = $dbh->selectrow_array("select admin from users where u_auto = $uid");

# get the projects this user has access to
my $stmt = "";
if ($ulevel==1) #admin - see all project
{
	$stmt = "select p_auto, project from projects order by project asc";
}
else #normal user
{
	$stmt = "select p.p_auto, p.project from projects as p, users_projects as up where p.p_auto = up.project and up.user = $uid order by p.project asc";
}
my $query = $dbh->prepare($stmt);
$query->execute();
my %projects = ();
while (my @data = $query->fetchrow_array()){
 $projects{$data[0]} = $data[1];
 }
$query->finish();
$dbh->disconnect();



print $cgi->header( -cookie=>[$cookie] );
print $cgi->start_html(
	-title=> $cfg->val('SITE','title'),
	-style=>[ {'src'=>"/dprs/css/dpr.css"} ],
	-script=>[ {-type=>'text/javascript',-src=>'/dprs/js/jquery-1.10.1.min.js'},
                   {-type=>'text/javascript',-src=>'/dprs/js/dpr.js'} ],
	-head => [ $cgi->meta({ -http_equiv => 'Pragma', -content => 'no-cache' }),
		$cgi->meta({ -http_equiv => 'Expires', -content => '-1' }) ]
	);

# Initially show all the projects
print "<div class=\"logout\">$uname<br>[ <a href=\"/dprs/cgi-bin/dpr_logout.cgi\">Logout</a> ]</div> \n";
print "<div class=\"projmenu\"><h3>Welcome " . ucfirst($uname) . "!<br></h3>\n";
print "<a href=\"/dprs/cgi-bin/dpr_projman.cgi\">Home</a><br>\n";

print "<h4>Your projects</h4><br>\n";
foreach (sort { $projects{$a} cmp $projects{$b} } keys %projects){
 print "<img src=\"/dprs/images/project_small1.png\" /><span class=\"link\" onclick=\"projheader($_,'h');\">". $projects{$_} . "</span><br>\n";
 }


#static project for ALL locked site data (special)
print "<br><img src=\"/dprs/images/project_small1.png\" /><span class=\"link\" onclick=\"projheader(9999,'h');\">". "All locked sites" . "</span><br>\n";

#admin summary starts
if ($ulevel==1) #admin - see admin summary
{
	print "<hr><img src=\"/dprs/images/project_small1.png\" /><span class=\"link\" onclick=\"adminsummary('$uname');\">Admin Summary</span><br>\n";
}

print "</div>\n";

print "<div id='runres' class='projdetails'>\n";
print "<h1>".$cfg->val('SITE','subtitle')."</h1>";

 print "<div class=\"frontdocs\" id=\"files\"><h3>Helpful Documents</h3><br><br>";
 print "<table>\n";
 print "<th>Document name</th>";
 print "<tr><td>Files / Documents should be linked into here</td></tr>";
 print "<tr><td>Files / Documents should be linked into here</td>";

 print "</tr></table>";
 print "</div>";

print $cgi->end_html();

sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
