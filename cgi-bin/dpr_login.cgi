#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use DBI;
use Config::IniFiles;


# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

# New SGI
my $cgi = new CGI;

# read user details from CGI
my $user = $cgi->param('user');
my $pass = $cgi->param('pass');

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbuser');;
my $dbpass =  $cfg->val('DATABASE','dbpass');
my %attr = (
	RaiseError => 1,
	AutoCommit => 0
	);

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);

# Authenticate user
my $stmt = "select u_auto from users where username = '$user' and password = '$pass'";

my $query = $dbh->prepare($stmt);
$query->execute();

my $uid = $query->fetchrow_array();
$query->finish();
$dbh->disconnect();

&err_login if (!$uid);

# now the person has been authenticated
# make a session cookie
my $session = new CGI::Session("driver:File", undef, {Directory=>'/tmp'});
$session->expire('+1h');
my $cookie = $cgi->cookie(
	-name=>'DPR_CGISESSID',
	-value=> $session->id(),
	-expires=> '+1h'
	);

# send the cookie to the browser
print $cgi->header( -cookie=>$cookie );

# put some info in to the session
$session->param("userid",$uid);

# redirect to the project management interface
print $cgi->start_html(
	-head => $cgi->meta({
		-http_equiv => 'Refresh',
		-content => '0;URL=/dprs/cgi-bin/dpr_projman.cgi'
		})
	);
print $cgi->end_html();



sub err_login {
 my $url = '/dprs';
 print $cgi->header( );
 print $cgi->start_html(
        -title=> $cfg->val('SITE','title')
        -style=>{'src'=>"/css/dpr.css"}
        );
print "<SCRIPT LANGUAGE='JavaScript'> window.alert('Username or password error, please try again or email the site admin ".$cfg->val('SITE','email')." for assistance');  window.location.href='/dprs';  </SCRIPT>";



 #print $cgi->redirect(-uri => $url);
 #print $cgi->header();
 #print "window.location=\"$url\";\n\n";
 exit(1);

 }
