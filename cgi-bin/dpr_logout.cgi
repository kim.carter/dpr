#!/usr/bin/perl

use strict;
use DBI;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;

# Check for Cookie or err
my $cgi = new CGI;
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('-1s');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('-1s');
# send the browser cookie back to the browser
print $cgi->header( -cookie=>[$cookie] );
print $cgi->start_html(
        -head => $cgi->meta({
                -http_equiv => 'Refresh',
                -content => '0;URL=/dprs/'
                })
        );
print $cgi->end_html();

sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
