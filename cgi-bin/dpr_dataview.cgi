#!/usr/bin/perl

use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::Session;
use CGI::Cookie;
use DBI;
use Config::IniFiles;

#use Date::Calc qw(check_date);
#use Statistics::Descriptive;
#use Data::Dumper;

my $cgi = new CGI;
my $type = $cgi->param('type');
my $proj = $cgi->param('proj');

# Check for Cookie or err
my $sid = $cgi->cookie("DPR_CGISESSID") || &err_login();

my $session = CGI::Session->load($sid);
&err_login() if $session->is_expired();
$session->expire('+1h');
my %cookies = fetch CGI::Cookie;
my $cookie = $cookies{'DPR_CGISESSID'};
$cookie->expires('+1h');
my $uid = $session->param("userid");

print $cgi->header( -cookie=>[$cookie] );

if ($type eq "v"){


# Load dpr config file
my $cfg = Config::IniFiles->new( -file => "../config/dpr.ini");

#set db host
my $dsn = "dbi:mysql:database=".$cfg->val('DATABASE','database').";host=".$cfg->val('DATABASE','host');
my $dbuser = $cfg->val('DATABASE','dbuser');;
my $dbpass =  $cfg->val('DATABASE','dbpass');
my %attr = (
        RaiseError => 1,
        AutoCommit => 0
        );

# connect to the database
my $dbh = DBI->connect($dsn, $dbuser, $dbpass, \%attr);


 if ($proj == 9999) # all sites
 {
	
        if (-e "/srv/shiny-server/dpr/global/data.csv")
        {
                print "<iframe src=\"http://localhost:3838/dpr/global/\" class=\"resframe\"></iframe>";
        }
        else {
         print "<center>No dataset are locked (for global access) currently. Please re-visit the page once datasets are available.</center>"
        }

 }
 else
{
  my @project = $dbh->selectrow_array("select hash from projects where p_auto = $proj");

 my $ds = lc($project[0]);
 my $hash = $project[0];
 if (-e "/srv/shiny-server/dpr/$hash/data.csv")
 {
         print "<iframe src=\"http://localhost:3838/dpr/$hash/\" class=\"resframe\"></iframe>";
 }
 else {
  print "<center>No data is loaded for this project currently. Visit the Upload page to add data.</center>"
 }




 }
 }

sub err_login {
 my $url = '/dprs/';
 print $cgi->redirect(-uri => $url);
 print $cgi->header();
 print "window.location=\"$url\";\n\n";
 exit(1);
 }
