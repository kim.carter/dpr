FROM ubuntu:14.04
LABEL Version="1.0" Description="dPortal data capture portal"
MAINTAINER Bioinformatics <bioinformatics@telethonkids.org.au>

#Update apt repository to a fast WA mirror
RUN echo "deb http://mirror.internode.on.net/pub/ubuntu/ubuntu/ trusty main restricted universe multiverse" > /etc/apt/sources.list
RUN echo "deb http://mirror.internode.on.net/pub/ubuntu/ubuntu trusty-updates main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb http://security.ubuntu.com/ubuntu trusty-security main restricted universe multiverse" >> /etc/apt/sources.list
RUN echo "deb http://cran.ms.unimelb.edu.au/bin/linux/ubuntu/ trusty/" >> /etc/apt/sources.list

# Install necessary system packages and dependencies
RUN apt-get update && apt-get install -y --force-yes apache2 mysql-server php5-dev php5-cli php5-mysql build-essential libncurses5-dev bzip2 wget ncurses-base flex bison zlib1g-dev git unzip r-base libcurl4-gnutls-dev libxml2-dev libcgi-session-perl libconfig-inifiles-perl  libmime-lite-perl libmime-types-perl libdate-calc-perl libspreadsheet-parseexcel-perl libstatistics-descriptive-perl libxml-twig-perl

# perl modules - need to perl build from CPAN

#cpan -MCPAN -e 'install Spreadsheet::ParseExcel'
#cpan -MCPAN -e 'install Spreadsheet::ParseXLSX'
#cpan -MCPAN -e 'install Digest::Perl::MD5'
#cpan -MCPAN -e 'install Graphics::ColorUtils'
#cpan -MCPAN -e 'install Crypt::Mode::ECB'

# shinyserver
#
# R - DT, markdown, plotly
