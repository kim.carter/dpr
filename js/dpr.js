//Browser Support Code
function ajaxFunction(div,size){
var ajaxRequest;  // The variable that makes Ajax possible!
try{
// Opera 8.0+, Firefox, Safari
 ajaxRequest = new XMLHttpRequest();
 } catch (e){
 // Internet Explorer Browsers
 try{
  ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e){
  try{
   ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
   } catch (e){
   // Something went wrong
   alert("You're using an unsupported browser!");
   return false;
   }
  }
 }

 // Create a function that will receive data sent from the server
 ajaxRequest.onreadystatechange = function(){
  // readyState can be processing downloading or complete
  // When readyState changes onreadystatechange executes
  //var ajaxDisplay = document.getElementById(div);
  if(ajaxRequest.readyState == 4){
   // Get the data from the server's response stored in responseText
   $("#" + div).html(ajaxRequest.responseText)
   //ajaxDisplay.innerHTML = ajaxRequest.responseText;
   }
  else{
   if (size){
    $("#" + div).html('<img src="/dprs/images/loading_small.gif">');
    //ajaxDisplay.innerHTML = '<img src="/images/loading_small.gif">';
    }
   else {
    $("#" + div).html('<img src="/dprs/images/loading.gif">');
    //ajaxDisplay.innerHTML = '<img src="/images/loading.gif">';
    }
   }
  }
 return(ajaxRequest);
 }

// Function when a project is clicked
// Displays header line with icons
function projheader(proj,type){
 var ajaxRequest = ajaxFunction('runres');
 var date = new Date();
 var timestamp = date.getTime();
 // use the ajaxRequest object to actually post something to the server
 var query = "?proj="+proj+"&type="+type+"&time="+timestamp;
 ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_project.cgi'+query, true);
 ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
 ajaxRequest.send(null);
 }


// Function when a project is clicked
// Displays header line with icons
function adminsummary(username){
 var ajaxRequest = ajaxFunction('runres');
 var date = new Date();
 var timestamp = date.getTime();
 // use the ajaxRequest object to actually post something to the server
 var query = "?"+"&username="+username;
 ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_adminsummary.cgi'+query, true);
 ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
 ajaxRequest.send(null);
 }

// Function when the check data icon is clicked
// Allows user to upload and check data
function datacheck(proj,type){
 var ajaxRequest = ajaxFunction('display');
 var date = new Date();
 var timestamp = date.getTime();
 // use the ajaxRequest object to actually post something to the server
 var query = "?type="+type+"&proj="+proj+"&time="+timestamp;
 ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_datacheck.cgi'+query, true);
 ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
 ajaxRequest.send(null);
 }

// Function when the remove data icon is clicked
// Allows user to delete data
function datadel(proj,type){
 var ajaxRequest = ajaxFunction('display');
 var date = new Date();
 var timestamp = date.getTime();
 // use the ajaxRequest object to actually post something to the server
 var query = "?type="+type+"&proj="+proj+"&time="+timestamp;
 ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_datadel.cgi'+query, true);
 ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
 ajaxRequest.send(null);
 }

//Function to delete data
function delete_data(load,type,proj){
 var r=confirm("You are about to remove data from the database\n. This cannot be undone. Are you sure?");
 if (r==true) {
  var ajaxRequest = ajaxFunction('result');
  var date = new Date();
  var timestamp = date.getTime();
  // use the ajaxRequest object to actually post something to the server
  var query = "?type="+type+"&load="+load+"&time="+timestamp+"&proj="+proj;
  ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_datadel.cgi'+query, false);
  ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
  ajaxRequest.send(null);
  }
 else {
  return false;
  }
 }



function lock_data(proj,type)
{
	// reset the projectmeta form, to make sure its saved when we lock
	// prevents from changing but not submitting
	//document.getElementById("projectmeta").reset();

	var form = document.getElementById('projectmeta');
	if (form != null)
	{
                $("#projectmeta").get(0).reset();

                //now confirm with user
                var r=confirm("You are about to lock the data to certify it is suitable for inclusion into the global summary statistics. This can only be unlocked by the DPR site admin.\n\nDo you wish to proceed?");
                if (r==true) {
                var ajaxRequest = ajaxFunction('result');
                var date = new Date();
                var timestamp = date.getTime();
                // use the ajaxRequest object to actually post something to the server
                var query = "?type="+type+"&proj="+proj+"&time="+timestamp;
                ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_project.cgi'+query, false);
                ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
                ajaxRequest.send(null);
                }
                else {
                        return false;
                }

	}
	else
	{
		 alert("We are returning you to the project metadata page to confirm details are correct before locking the dataset.");
                return false;
	}
}


function unlock_data(proj,type){
 var r=confirm("You are about to unlock the data to remove it from inclusion in the global summary statistics (updated nightly).\n\nDo you wish to proceed?");
 if (r==true) {
  var ajaxRequest = ajaxFunction('result');
  var date = new Date();
  var timestamp = date.getTime();
  // use the ajaxRequest object to actually post something to the server
  var query = "?type="+type+"&proj="+proj+"&time="+timestamp;
  ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_project.cgi'+query, false);
  ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
  ajaxRequest.send(null);
  }
 else {
  return false;
  }
 }


// Function when the view site dataicon is clicked
// Allows user to view data
function dataview(proj,type){
 var ajaxRequest = ajaxFunction('display');
 var date = new Date();
 var timestamp = date.getTime();
 // use the ajaxRequest object to actually post something to the server
 var query = "?type="+type+"&proj="+proj+"&time="+timestamp;
 ajaxRequest.open('GET', '/dprs/cgi-bin/dpr_dataview.cgi'+query, true);
 ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
 ajaxRequest.send(null);
 }

// Function to use ajax to upload data and send for processing
function dprupdata() {
 formdata = new FormData();
 document.getElementById("response").innerHTML = "";
 var f = document.getElementById("dprfile");
 if (!f.files.length){
  document.getElementById("response").innerHTML = "Please select a file<br>";
  return false;
  }
 var i = 0, file;
 document.getElementById("response").innerHTML = "Uploading . . . (note this may take up to a minute or so)";

 document.getElementById("loading").style.visibility = "visible";
 var proj = document.getElementById("proj").value;
 file = f.files[i];
 formdata.append("dprfile", file);
 formdata.append("type", "u");
 formdata.append("proj", proj);
 $.ajax({
  url: "/dprs/cgi-bin/dpr_datacheck.cgi",
  type: "POST",
  data: formdata,
  processData: false,
  contentType: false,
  success: function (res) {
   document.getElementById("loading").style.visibility = "hidden";
   document.getElementById("response").innerHTML = res;
   }
  });
 }


function save_meta(val,proj) {


         $('#projectmeta').submit(function(e){
             e.preventDefault();
             $.ajax({
                 url:'/dprs/cgi-bin/dpr_projectmeta.cgi',
                 type:'post',
                 contentType: false,
                 cache: false,
                 processData:false,
                 data:new FormData(document.getElementById('projectmeta')),
                 //data:$('#projectmeta').serialize(),
                 success:function(){
                    alert("Saved changes to metadata\nYou may have to refresh to see new files");
                 }
             });
         });

return false;
}



function send_data(val,proj) {



 // all fields must have something in them
 var warning = "";

 if (warning == ""){

  //var form = document.getElementById(val);
  //wopen('newwindow',200,100);
  //form.target = 'newwindow';
  //form.submit();
  //sleep(2);

  //document.getElementById('projectmeta').submit(function() {
//          event.preventDefault();
//});
          /*

          $.ajax({
           url: "/dprs/cgi-bin/dpr_projectmeta.cgi",
           type: "POST",
           processData: false,
           data: {form,proj:proj}


    });
    return false;
    });
    */
  return false;
 // 'newwindow'.onbeforeunload = window.location.reload();
 // 'newwindow'.close();
 // form.submit();
  //sleep(2);


 //var query = "?proj="+proj;
 //ajaxRequest.open('GET', '/cgi-bin/gli_project.cgi'+query, false);
 //ajaxRequest.setRequestHeader("Cache-Control","no-cache, private, max-age=0");
 //ajaxRequest.send(null);

  //return true;
  }
 else {
  alert(warning);
  return false;
  }

 }



function revealdiv(div){
 var divobj = document.getElementById(div);
 if (divobj.style.display == "inline"){
  divobj.style.display = "none";
  }
 else {
  divobj.style.display = "inline";
  }
 }

function wopen(name, w, h){
 w += 32;
 h += 96;
 var wleft = (screen.width - w) / 2;
 var wtop = (screen.height - h) / 2;
 if (wleft < 0) {
  w = screen.width;
  wleft = 0;
  }
 if (wtop < 0) {
  h = screen.height;
  wtop = 0;
  }
 var win = window.open('',name,'width='+w+',height='+h+','+'left='+wleft+',top='+wtop+','+'location=no,menubar=no,status=no,toolbar=no,scrollbars=yes');
 win.resizeTo(w, h);
 win.moveTo(wleft, wtop);
 win.focus();
 //win.onbeforeunload = window.location.reload();
 //win.close();

 }

function showoth(v,val){
 var obj=document.getElementById(v);
 var objhid=document.getElementById(v+"_other");
 if(obj.value==val){
  objhid.readOnly=false;
  }
 else {
  objhid.value = "";
  objhid.readOnly=true;
  }
 }


function sleepStupidly(usec)
    {
	var endtime= new Date().getTime() + usec;
        while (new Date().getTime() < endtime)
	    ;
    }
