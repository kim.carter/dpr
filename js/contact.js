$(function() {
	// Validate the contact form
  $('#contactform').validate({
  	// Specify what the errors should look like
  	// when they are dynamically added to the form
  	errorElement: "label",
  	wrapper: "td",
  	errorPlacement: function(error, element) {
  		error.insertBefore( element.parent().parent() );
  		error.wrap("<tr class='error'></tr>");
  		$("<td></td>").insertBefore(error);
  	},
  	
  	// Add requirements to each of the fields
  	rules: {
  		data_name: {
  			required: true,
  			minlength: 5
  		},
  		contact_sname: {
  			required: true,
  			minlength: 2
  		},
  		contact_fname: {
  			required: true,
  			minlength: 2
  		},
  		contact_email: {
  			required: true,
  			email: true
  		},
  		contact_phone: {
  			required: true,
  			minlength: 8
  		},
		custodian_name: {
  			required: true,
  			minlength: 5
  		},
  		custodian_email: {
  			required: true,
  			email: true
  		},
  		custodian_phone: {
  			required: true,
  			minlength: 8
  		},
  		message: {
  			required: true,
  			minlength: 10
  		},
  		captcha: {
  			required: true,
  			captcha: true
  		},
		data_size: {
  			required: true,
  			minlength: 1
  		}

  	},
  	
  	// Specify what error messages to display
  	// when the user does something horrid
  	messages: {
  		name: {
  			required: "Please enter your name.",
  			minlength: jQuery.format("At least {0} characters required.")
  		},
  		email: {
  			required: "Please enter your email.",
  			email: "Please enter a valid email."
  		},
  		message: {
  			required: "Please enter a message.",
  			minlength: jQuery.format("At least {0} characters required.")
  		},
		captcha: {
  			required: "Please enter the security code.",
  			captcha: "Please enter the correct security code."
  		}
  	},
  	
  	// Use Ajax to send everything to processForm.php
  	submitHandler: function(form) {
  		$("#send").attr("value", "Sending...");
  		$(form).ajaxSubmit({
  			success: function(responseText, statusText, xhr, $form) {
  				$(form).slideUp("fast");
  				$("#response").html(responseText).hide().slideDown("fast");
  			}
  		});
  		return false;
  	}
  });
});
