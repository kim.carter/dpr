<?php

	//read ini file
	$ini = parse_ini_file("../config/dpr.ini",true,INI_SCANNER_RAW);


	# note this requires php5-mysql ubuntu package
	$conn = new mysqli($ini['DATABASE']['host'], $ini['DATABASE']['dbuser'], $ini['DATABASE']['dbpass'], $ini['DATABASE']['database']);

	// Check connection
	if ($conn->connect_error)
	{
    		die("<html><title>DPR</title><body>Unable to connect to DPR database - Connection failed: " . $conn->connect_error.". <BR>Please contact your administrator</body></html>");
	}

	// get details to display on public site
	$sql = "SELECT title, body FROM publicsite";
	$result = $conn->query($sql);

	if ($result->num_rows > 0)
	{
		$row = $result->fetch_assoc();
		$title  = $row['title'];
		$body = $row['body'];
	}
	else
	{
	    die("<html><title>DPR</title><body>No site data has been loaded for this DPR installation.<BR>Please read the project documentation for further information</body></html>") ;
	}

	$conn->close();
?>

<html>

<head>
	<title><?=$title?></title>
<!--	<link href='http://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Allerta' rel='stylesheet' type='text/css'> -->

	<link rel="stylesheet" href="/dpr/css/dpr.css" type="text/css" />
	<script type="text/javascript" src="/dpr/js/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="/dpr/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/dpr/js/jquery.form.js"></script>
	<script type="text/javascript" src="/dpr/js/contact.js"></script>

	<script language="JavaScript">
		function CopyContact(f)
		{
			if(f.copycontact.checked == true)
			{
				f.custodian_name.value = f.contact_fname.value+" "+f.contact_sname.value;
				f.custodian_email.value = f.contact_email.value;
				f.custodian_phone.value = f.contact_phone.value;
			}
		}

		function checkFile()
		{
	      		var fileElement = document.getElementById("moufile");
        		var fileExtension = "";
        		if (fileElement.value.lastIndexOf(".") > 0)
			{
            			fileExtension = fileElement.value.substring(fileElement.value.lastIndexOf(".") + 1, fileElement.value.length);
        		}

			if (fileExtension == "PDF" || fileExtension == "pdf" )
			{
            			return true;
        		}
        		else
			{
            			alert("You must select a PDF file for upload");
            			return false;
        		}
    		}
	</script>
</head>

<body>
<div id="wrap">
	<?=$body?>
asasa
</div>

<BR>

<div id="wrap">

	<form id="contactform" action="processForm.php" method="post" enctype="multipart/form-data" onsubmit="return checkFile();">

	<table>
		<tr>
			<td><label for="name">Admin Contact First Name:</label></td>
			<td><input type="text" id="contact_fname" name="contact_fname" /></td>
		</tr>
		<tr>
			<td><label for="name">Admin Contact Surname:</label></td>
			<td><input type="text" id="contact_sname" name="contact_sname" /></td>
		</tr>
		<tr>
			<td><label for="email">Admin Contact Email:</label></td>
			<td><input type="text" id="contact_email" name="contact_email" /></td>
		</tr>
		<tr>
			<td><label for="phone">Admin Contact Phone:</label></td>
			<td><input type="text" id="contact_phone" name="contact_phone" /></td>
		</tr>
		<tr>
			<td><label for="name">Admin Institution/Organisation:</label></td>
			<td><input type="text" id="contact_org" name="contact_org" /></td>
		</tr>
		<tr>
			<td><label for="name">Admin Address:</label></td>
			<td><input type="text" id="contact_address" name="contact_address" /></td>
		</tr>

		<tr>
			<td><label for="name">Country:</label></td>
			<td><?php include 'countryselect.html'; ?></td>
		</tr>

		<tr><td/><td>Same as above? <input type="checkbox" name="copycontact" onclick="CopyContact(this.form)"></td></tr>
		<tr>
			<td><label for="name">Data Custodian Name:</label></td>
			<td><input type="text" id="custodian_name" name="custodian_name" /></td>
		</tr>
		<tr>
			<td><label for="email">Data Custodian Email:</label></td>
			<td><input type="text" id="custodian_email" name="custodian_email" /></td>
		</tr>
		<tr>
			<td><label for="phone">Data Custodian Phone:</label></td>
			<td><input type="text" id="custodian_phone" name="custodian_phone" /></td>
		</tr>
		<tr>
			<td><label for="name">Name of data source:</label></td>
			<td><input type="text" id="data_name" name="data_name" /></td>
		</tr>
		<tr>
			<td><label for="name">Approx number of records:</label></td>
			<td><input type="text" id="data_size" name="data_size" /></td>
		</tr>
		<tr>
			<td><label for="message">Brief Description of dataset:</label></td>
			<td><textarea id="description" name="description" rows="5" cols="20"></textarea></td>
		</tr>
		<tr>
                        <td><label for="file">Select signed GLI MOU:</label></td>
                        <td><input type="file" id="moufile" name="moufile" /></td>
                </tr>

		<tr>
			<td>Security check (enter code): <img src="captcha.php"/></td>
			<td><input id="captcha" name="captcha" type="text"></td>
		</tr>


		<tr>
			<td></td>
			<td><button class="button button" type="submit" id="send">Signup!</button></td>
		</tr>
		

	</table>

	</form>

<div id="response"></div>

</div>

</body>
</html>
