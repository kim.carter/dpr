<?php
session_start();

// Clean up the input values
foreach($_POST as $key => $value) {
	if(ini_get('magic_quotes_gpc'))
		$_POST[$key] = stripslashes($_POST[$key]);
	$_POST[$key] = iconv("UTF-8", "ISO-8859-1//IGNORE", $_POST[$key]);

#	$_POST[$key] = iconv(mb_detect_encoding($_POST[$key], mb_detect_order(), true), "UTF-8//IGNORE", $_POST[$key]);	
#	$_POST[$key] = htmlspecialchars(strip_tags($_POST[$key]));

}

// Test input values for errors
$errors = array();

// Assign the input values to variables for easy reference
$con_name = $_POST["contact_sname"];
$con_email = $_POST["contact_email"];

if(strlen($con_name) < 2) {
        if(!$con_name) {
                $errors[] = "You must enter a contact name.";
        } else {
                $errors[] = "Name must be at least 2 characters.";
        }
}
if(!$con_email) {
        $errors[] = "You must enter a contact email.";
} else if(filter_var($con_email, FILTER_VALIDATE_EMAIL) === false) {
        $errors[] = "You must enter a valid email.";
}

if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&$_SESSION["code"]==$_POST["captcha"])
{
//echo "Correct Code Entered";
}
else
{
        $errors[] = "Please enter the correct security code.";
}

if ($errors)
{}
else
{

# write to temporary file on server

$key = MD5(microtime()); // eg 5959e4c551ee3a91d89449437b681ead
mkdir("/home/gli/requests/$key");


define("UPLOAD_DIR", "/home/gli/requests/$key/");
if (!empty($_FILES["moufile"])) {
    $myFile = $_FILES["moufile"];
 
    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        $errortext = "";
        foreach($errors as $error) {
                $errortext .= "<li>".$error."</li>";
        }
        die("<span class='failure'>The following errors occured:<ul>". $errortext ."<li>An error occurred uploading the MOU file</i></ul></span>");

    }
 
    // ensure a safe filename
    #$name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);
    $name = "MOU.pdf";
 
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name);
    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
 
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"],UPLOAD_DIR . $name);
    if (!$success) { 
	$errortext = "";
        foreach($errors as $error) {
                $errortext .= "<li>".$error."</li>";
        }
        die("<span class='failure'>The following errors occured:<ul>". $errortext ."<li>An error occurred uploading the MOU file</li></ul></span>");

 

      // echo "<p>Unable to save file.</p>";
       // exit;
    }
 
    // set proper permissions on the new file
    chmod(UPLOAD_DIR . $name, 0644);
}
} // end file pload

if($errors) {
	// Output errors and die with a failure message
	$errortext = "";
	foreach($errors as $error) {
		$errortext .= "<li>".$error."</li>";
	}
	die("<span class='failure'>The following errors occured:<ul>". $errortext ."</ul></span>");
}


# write to temporary file on server


$my_file = "/home/gli/requests/$key/details.txt";
$handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file); //implicitly creates file
fwrite($handle, "contact_fname\t".$_POST["contact_fname"]."\n");
fwrite($handle, "contact_sname\t".$_POST["contact_sname"]."\n");
fwrite($handle, "contact_email\t".$_POST["contact_email"]."\n");
fwrite($handle, "contact_phone\t".$_POST["contact_phone"]."\n");
fwrite($handle, "contact_org\t".$_POST["contact_org"]."\n");
fwrite($handle, "contact_address\t".$_POST["contact_address"]."\n");
fwrite($handle, "country\t".$_POST["country"]."\n");
fwrite($handle, "custodian_name\t".$_POST["custodian_name"]."\n");
fwrite($handle, "custodian_email\t".$_POST["custodian_email"]."\n");
fwrite($handle, "custodian_phone\t".$_POST["custodian_phone"]."\n");
fwrite($handle, "data_name\t".$_POST["data_name"]."\n");
fwrite($handle, "data_size\t".$_POST["data_size"]."\n");
fwrite($handle, $_POST["description"]."\n");

fclose($handle);


// Send the email to admin
$subject = "GLI Gas Transfer Project: New project request for ".$_POST["contact_fname"]." ".$_POST["contact_sname"];
$message1 = "<p>A new signup to the GLI gas transfer project has occurred.</p>";
$message1 .= "<p>Full details of submission are following, with uploaded PDF attached.</p>";
$message1 .= "<ul><li><b>Contact name:</b> ".$_POST["contact_fname"]." ".$_POST["contact_sname"]."\n";
$message1 .= "<li><b>Contact email:</b> ".$_POST["contact_email"]."\n";
$message1 .= "<li><b>Contact phone:</b> ".$_POST["contact_phone"]."\n";
$message1 .= "<li><b>Contact organisation:</b> ".$_POST["contact_org"]."\n";
$message1 .= "<li><b>Contact address:</b> ".$_POST["contact_address"]."\n";
$message1 .= "<li><b>Country:</b> ".$_POST["country"]."\n";
$message1 .= "</ul><ul>";
$message1 .= "<li><b>Custodian name:</b> ".$_POST["custodian_name"]."\n";
$message1 .= "<li><b>Custodian email:</b> ".$_POST["custodian_email"]."\n";
$message1 .= "<li><b>Custodian phone:</b> ".$_POST["custodian_phone"]."\n";
$message1 .= "</ul><ul>";
$message1 .= "<li><b>Data name:</b> ".$_POST["data_name"]."\n";
$message1 .= "<li><b>Data size:</b> ".$_POST["data_size"]."\n";
$message1 .= "<li><b>Description:</b> ".$_POST["description"]."\n";
$message1 .= "</ul>";



$message1 .= "</p>\n";
$message1 .= "<p>To activate this signup, please go to <a href=\"https://www.gligastransfer.org.au/cgi-bin/gli_signup.cgi?newid=$key\">https://www.gligastransfer.org.au/cgi-bin/gli_signup.cgi?newid=$key</a>.</p>\n\n";

$message2 = "A new signup to the GLI gas transfer project has occurred.\n\n";
$message2 .= "Full details of submission are following, with uploaded PDF attached.";

$message2 .= "Contact name: ".$_POST["contact_fname"]." ".$_POST["contact_sname"]."\n";
$message2 .= "Contact email: ".$_POST["contact_email"]."\n";
$message2 .= "Contact phone: ".$_POST["contact_phone"]."\n";
$message2 .= "Contact organisation: ".$_POST["contact_org"]."\n";
$message2 .= "Contact address: ".$_POST["contact_address"]."\n";
$message2 .= "Country: ".$_POST["country"]."\n";
$message2 .= "Custodian name: ".$_POST["custodian_name"]."\n";
$message2 .= "Custodian email: ".$_POST["custodian_email"]."\n";
$message2 .= "Custodian phone: ".$_POST["custodian_phone"]."\n";
$message2 .= "Data name: ".$_POST["data_name"]."\n";
$message2 .= "Data size: ".$_POST["data_size"]."\n";
$message2 .= "Description: ".$_POST["description"]."\n";


$message2 .= "To activate this signup, please go to https://www.gligastransfer.org.au/cgi-bin/gli_signup.cgi?newid=$key\">.\n\n";






require 'class.phpmailer.php';

$gliadmin2 = 'grahamh@ichr.uwa.edu.au';
$gliadmin3 = 'B.Thompson@alfred.org.au';
$gliadmin4 = 'Brian.Graham@sk.lung.ca';
$gliadmin1 = 'sanja.stanojevic@sickkids.ca';


$mail = new PHPMailer;

$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = '10.0.1.31';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication

$mail->From = 'gligastransfer@ichr.uwa.edu.au';
$mail->FromName = 'gligastransfer@ichr.uwa.edu.au';
$mail->AddAddress($gliadmin1);               // Name is optional
$mail->AddCC($gliadmin2);
$mail->AddCC($gliadmin3);
$mail->AddCC($gliadmin4);
$mail->AddCC('bioinformatics@ichr.uwa.edu.au');
#$mail->AddAddress($gliadmin2);

#$mail->AddCC('sanja.stanojevic@sickkids.ca');
#$mail->AddCC('B.Thompson@alfred.org.au');


$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->AddAttachment(UPLOAD_DIR . $name,'MOU.pdf');         // Add attachments
$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = $subject;
$mail->Body = $message1;
$mail->AltBody = $message2;


if(!$mail->Send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

#echo 'Message has been sent';


#send mail to person
$mail = new PHPMailer;

$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mailgate.uwa.edu.au';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication

$mail->From = 'gliigastransfer@ichr.uwa.edu.au';
$mail->FromName = 'gligastransfer@ichr.uwa.edu.au';
$mail->AddAddress($_POST["contact_email"], $_POST["contact_fname"]." ".$_POST["contact_sname"]);  // Add a recipient
//$mail->AddAddress('ellen@example.com');               // Name is optional
//$mail->AddReplyTo('info@example.com', 'Information');
//$mail->AddCC('kcarter@ichr.uwa.edu.au');
//$mail->AddBCC('bcc@example.com');

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
//$mail->AddAttachment(UPLOAD_DIR . $name,'MOU.pdf');         // Add attachments
//$mail->AddAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = 'GLI Gas Transfer Project: Confirmation of your signup request';
$mail->Body = "<p>Thankyou for your interest in contributing data to the GLI project. Your request has been sent to the GLI admin contacts for final checks (and a confirmation email sent to you), and if everything is okay, you will receive further instructions via your nominated email - ".$_POST["contact_email"].". Thanks!</p>";

//$mail->AltBody = $message2;


if(!$mail->Send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}


// Die with a success message
die("	<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />	<div id=\"wrap\"> <p>Thankyou for your interest in contributing data to the GLI project. Your request has been sent to the GLI admin contacts for final checks (and a confirmation email sent to you), and if everything is okay, you will receive further instructions via your nominated email - ".$_POST["contact_email"].". Thanks!</p> </div>");




// A function that checks to see if
// an email is valid
function validEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                 str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}

?>
