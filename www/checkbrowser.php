<?php
function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";
    $ub="";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'Linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'MacOS';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'Windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

// now try it
$ua=getBrowser();
$yourbrowser= "Your browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];

print_r("<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />");
print_r("<div id=\"wrap\">");

print_r("<b><center><h2>Browser Check</h2><p><h4>This is a browser compatability check for accessing the secure portal for the GLI Gas Transfer Project.</h4></p>");
#print_r($yourbrowser);

#CHROME - all platforms (and generally versions) is okay)
if (preg_match('/Chrom/',$ua['name']))
{
	print_r("<p>Your browser is Google Chrome, Version ".$ua['version']." on ".$ua['platform']." - everything should work okay.</p><p><img src=\"/images/thumbsup.gif\"</p>");
}
elseif (preg_match('/Fire/',$ua['name']))
{
	if ($ua['version'] < 3)
	{
		print_r("<p>Your browser is Mozilla Firefox, Version ".$ua['version']." on ".$ua['platform']." - this version is out of date, please upgrade by going to <a href=\"www.firefox.com\" target=\"_blank\">http://www.firefox.com</a>.</p></p><img src=\"/images/thumbsdown.gif\"</p>");
	}
	else
	{
		print_r("<p>Your browser is Mozilla Firefox, Version ".$ua['version']." on ".$ua['platform']." - everything should work okay.</p><p><img src=\"/images/thumbsup.gif\"</p>");
	}
}
elseif (preg_match('/Safari/',$ua['name'])) # Safari
{
	print_r("<p>Your browser is Apple Safari, Version ".$ua['version']." on ".$ua['platform'].". Unfortunately this browser does not properly support the security features used for the GLI site.  Can you please install Google Chrome (MacOS 10.6 or later) - <a href=\"http://www.google.com/intl/en/chrome/browser/\ target=\"_blank\">http://www.google.com/intl/en/chrome/browser/</a> or Mozilla Firefox - <a href=\"www.firefox.com\">http://www.firefox.com</a>.</p><p><img src=\"/images/thumbsdown.gif\"</p>");
}
elseif (preg_match('/Explorer/',$ua['name']))  #IE
{
	if ($ua['version'] < 10)
        {
		print_r("<p>Your browser is Microsoft Internet Explorer, Version ".$ua['version']." on ".$ua['platform'].". Unfortunately this browser does not properly support the security features used for the GLI site.  Can you please upgrade to Internet Explorer 10 (only if you are on Windows 7 above), or alternately please install Google Chrome - <a href=\"http://www.google.com/intl/en/chrome/browser/\ target=\"_blank\">http://www.google.com/intl/en/chrome/browser/</a> or Mozilla Firefox - <a href=\"www.firefox.com\">http://www.firefox.com</a>.</p><p><img src=\"/images/thumbsdown.gif\"</p>");
	}
	else
	{
		print_r("<p>Your browser is Microsoft Internet Explorer, Version ".$ua['version']." on ".$ua['platform']." - everything should work okay.</p><p><img src=\"/images/thumbsup.gif\"</p>");
	}
}
elseif (preg_match('/Opera/',$ua['name']))  #Opera
{
	print_r("<p>Your browser is Opera, Version ".$ua['version']." on ".$ua['platform']." - everything should work okay (or if not, please upgrade to the latest version).</p><p><img src=\"/images/thumbsup.gif\"</p>");

}
else
{
        print_r("<p>Your browser is ".$us['name'].", Version ".$ua['version']." on ".$ua['platform'].". This browser may not properly support the security features used for the GLI site.  Can you please install Google Chrome - <a href=\"http://www.google.com/intl/en/chrome/browser/\ target=\"_blank\">http://www.google.com/intl/en/chrome/browser/</a> or Mozilla Firefox - <a href=\"www.firefox.com\">http://www.firefox.com</a>.</p><p><img src=\"/images/thumbsdown.gif\"</p>");
}
?>



