insert into publicsite (title,body) values ("DPR test site",'<h1>About the DPR test site</h1><p>If you would like to join and contribute data to this collaboration, please read the following Memorandum of Understanding (MOU) for sites and researching contributing to this DPR - <a href="MOU.pdf">MOU.pdf</a>. This document describes the necessary operation details, ethical requirements, authorship and other information.');

insert into study (study,shortname,description,delstat) values ("DPR test study","DPR","This is a test study for demonstratrating DPR",0);

insert into users (username,password,time_zone,email,it,delstat,admin) values ("administrator","adminpass","Australia/Perth","bioinformatics@ichr.uwa.edu.au",1,0,1);


insert into projects (project,title,description,study,country,hash,locked) values ("Australia_administrator_1","DPR test project","This is a test data contributor project for DPR",1,"Australia","14c598500456cded4ec350a4d50d1a89",0);

insert into projects (project,title,description,study,country,hash,locked) values ("Australia_administrator_2","DPR test project2","This is a test data contributor project for DPR",1,"Australia","16c598500456cded4ec350a4d50d1a89",0);


insert into users_projects (user,project,user_level) values (1,1,2);
insert into users_projects (user,project,user_level) values (1,2,2);


# example continuous variable - type 2
insert into project_meta(variable,description,units,study,type,qorder,delstat) values ('con1','Example continuous variable - representing litres','L',1,2,1,0);
SET @lastid = LAST_INSERT_ID();
insert into project_meta_con (variable,min,max,prec) VALUES (@lastid,0,1,3);

# example categorical variable - type 1
insert into project_meta (variable,description,units,study,type,qorder,delstat) VALUES ("cat1","Example categorical variable - representing brand","",1,1,2,0);
SET @lastid = LAST_INSERT_ID();
insert into project_meta_cat (variable,cat,code) VALUES (@lastid,1,"Apple");
insert into project_meta_cat (variable,cat,code) VALUES (@lastid,2,"Google");
insert into project_meta_cat (variable,cat,code) VALUES (@lastid,3,"Microsoft");

# example string variable - type 0
insert into project_meta (variable,description,units,study,type,qorder,delstat) VALUES ("str1","Example string variable - representing year","",1,0,3,0);

# example file input variable - type 3
insert into project_meta (variable,description,units,study,type,qorder,delstat) VALUES ("file1","Example file input variable - representing pdf","",1,3,4,0);



########################################
#  define data dictionary of variables #
########################################
# person_id used as the id column in the ini file continuous/integer/string variable - type 0, mandatory
# we use string type to enable any type of unique ID to be used
insert into variables (variable,description,units,study,type,mand,delstat) VALUES ("person_id","Participant's ID","",1,0,1,0);

# example variable continuous/integer numerical variable - type 2, mandatory
insert into variables (variable,description,units,study,type,mand,delstat) VALUES ("age","Age at the time of testing","years",1,2,1,0);
SET @lastid = LAST_INSERT_ID();
insert into variables_con (variable,min,max,prec) VALUES (@lastid,0,120,3);

# example variable categorical numerical variable - type 1, mandatory
insert into variables (variable,description,units,study,type,mand,delstat) VALUES ("sex","Participant's sex","",1,1,1,0);
SET @lastid = LAST_INSERT_ID();
insert into variables_cat (variable,cat,code) VALUES (@lastid,1,"Male");
insert into variables_cat (variable,cat,code) VALUES (@lastid,2,"Female");

# example data variable - type 3, mandatory
insert into variables (variable,description,units,study,type,mand,delstat) VALUES ("dobirth","Date of birth","",1,3,1,0);
SET @lastid = LAST_INSERT_ID();
insert into variables_dat (variable,min,max) VALUES (@lastid,"","");

# example string variable - type 0, not mandatory
insert into variables (variable,description,units,study,type,mand,delstat) VALUES ("ethnicity","","",1,0,0,0);
