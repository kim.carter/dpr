CREATE DATABASE dpr DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
use dpr;

grant select on dpr.* to 'dpruser'@'127.0.0.1' identified by 'dprpass';
grant all privileges on dpr.* to 'dpradmin'@'127.0.0.1' identified by 'dprpass';

flush privileges;


CREATE TABLE `publicsite` (
  `p_auto` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT 'DPR',
  `body` mediumtext DEFAULT '',
   PRIMARY KEY (`p_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `study` (
  `st_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `study` varchar(50) NOT NULL,
  `shortname` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `delstat` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`st_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `projects` (
  `project` varchar(50) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '0',
  `p_auto` int(11) NOT NULL AUTO_INCREMENT,
  `study` int(11) unsigned NOT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `contact_phone` varchar(15) DEFAULT NULL,
  `contact_fname` varchar(50) DEFAULT NULL,
  `contact_sname` varchar(50) DEFAULT NULL,
  `custodian_name` varchar(50) DEFAULT NULL,
  `custodian_email` varchar(50) DEFAULT NULL,
  `custodian_phone` varchar(15) DEFAULT NULL,
  `contact_org` varchar(25) DEFAULT NULL,
  `contact_address` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `hash` varchar(50) DEFAULT NULL,
  `locked` tinyint(1) unsigned DEFAULT '0',
  FOREIGN KEY (`study`) REFERENCES `study` (`st_auto`) ON DELETE CASCADE,
  PRIMARY KEY (`p_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `u_auto` int(11) NOT NULL AUTO_INCREMENT,
  `time_zone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `it` tinyint(1) unsigned DEFAULT '0',
  `delstat` tinyint(1) unsigned DEFAULT '0',
  `admin` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`u_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_privs` (
  `pp_auto` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pp_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project_privs` VALUES (1,'Guest (view only)'),(2,'Data admin');

CREATE TABLE `users_projects` (
  `user` int(11) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `user_level` int(1) unsigned DEFAULT '1',
  `up_auto` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`up_auto`),
  FOREIGN KEY (`user`) REFERENCES `users` (`u_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`project`) REFERENCES `projects` (`p_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`user_level`) REFERENCES `project_privs` (`pp_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `load_data` (
  `ld_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `load_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  `locked` tinyint(1) unsigned DEFAULT '0',
  FOREIGN KEY (`project`) REFERENCES `projects` (`p_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`user`) REFERENCES `users` (`u_auto`) ON DELETE CASCADE,
  PRIMARY KEY (`ld_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta` (
  `pm_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `units` varchar(50) DEFAULT NULL,
  `study` int(11) unsigned NOT NULL,
  `type` int(11) unsigned NOT NULL DEFAULT '0',
  `qorder` tinyint(1) unsigned DEFAULT '0',
  `delstat` tinyint(1) unsigned NOT NULL DEFAULT '0',
  FOREIGN KEY (`study`) REFERENCES `study` (`st_auto`) ON DELETE CASCADE,
  PRIMARY KEY (`pm_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta_cat` (
  `pmcat_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned NOT NULL,
  `cat` int(11) unsigned NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pmcat_auto`),
  FOREIGN KEY (`variable`) REFERENCES `project_meta` (`pm_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta_con` (
  `pmcon_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned NOT NULL,
  `min` float DEFAULT NULL,
  `max` float DEFAULT NULL,
  `prec` smallint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pmcon_auto`),
  FOREIGN KEY (`variable`) REFERENCES `project_meta` (`pm_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta_dat` (
  `pmdat_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned NOT NULL,
  `min` date DEFAULT NULL,
  `max` date DEFAULT NULL,
  PRIMARY KEY (`pmdat_auto`),
  FOREIGN KEY (`variable`) REFERENCES `project_meta` (`pm_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta_data` (
  `pmd_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project` int(11) NOT NULL,
  `variable` int(11) unsigned NOT NULL DEFAULT '0',
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pmd_auto`),
  FOREIGN KEY (`variable`) REFERENCES `project_meta` (`pm_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`project`) REFERENCES `projects` (`p_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta_data_other` (
  `pmdo_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `project` int(11) NOT NULL,
  `variable` int(11) unsigned NOT NULL DEFAULT '0',
  `value` varchar(100) DEFAULT NULL,
  FOREIGN KEY (`variable`) REFERENCES `project_meta` (`pm_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`project`) REFERENCES `projects` (`p_auto`) ON DELETE CASCADE,
  PRIMARY KEY (`pmdo_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `project_meta_oth` (
  `pmoth_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned DEFAULT NULL,
  `oth` int(11) unsigned DEFAULT NULL,
  FOREIGN KEY (`variable`) REFERENCES `project_meta` (`pm_auto`) ON DELETE CASCADE,
  PRIMARY KEY (`pmoth_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `variables` (
  `v_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `units` varchar(50) DEFAULT NULL,
  `study` int(11) unsigned NOT NULL,
  `type` int(11) unsigned NOT NULL DEFAULT '0',
  `mand` tinyint(1) unsigned DEFAULT '0',
  `delstat` tinyint(1) unsigned NOT NULL DEFAULT '0',
  FOREIGN KEY (`study`) REFERENCES `study` (`st_auto`) ON DELETE CASCADE,
  PRIMARY KEY (`v_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `variables_cat` (
  `vcat_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned NOT NULL,
  `cat` int(11) unsigned NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vcat_auto`),
  FOREIGN KEY (`variable`) REFERENCES `variables` (`v_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `variables_con` (
  `vcon_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned NOT NULL,
  `min` float DEFAULT NULL,
  `max` float DEFAULT NULL,
  `prec` smallint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`vcon_auto`),
  FOREIGN KEY (`variable`) REFERENCES `variables` (`v_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `variables_dat` (
  `vdat_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `variable` int(11) unsigned NOT NULL,
  `min` date DEFAULT NULL,
  `max` date DEFAULT NULL,
  PRIMARY KEY (`vdat_auto`),
  FOREIGN KEY (`variable`) REFERENCES `variables` (`v_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `subjects` (
  `s_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(50) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `load_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`s_auto`),
  FOREIGN KEY (`project`) REFERENCES `projects` (`p_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`load_id`) REFERENCES `load_data` (`ld_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `data_table` (
  `dt_auto` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` int(11) unsigned NOT NULL DEFAULT '0',
  `variable` int(11) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dt_auto`),
  FOREIGN KEY (`subject`) REFERENCES `subjects` (`s_auto`) ON DELETE CASCADE,
  FOREIGN KEY (`variable`) REFERENCES `variables` (`v_auto`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
